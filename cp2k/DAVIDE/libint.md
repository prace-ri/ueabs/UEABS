```
module load gnu
export CC=gcc
export CXX=g++
CFLAGS="-O2 -ftree-vectorize -g -fno-omit-frame-pointer -mcpu=power8 -ffast-math"
CXXFLAGS=$CFLAGS
./configure --build=ppc64le-linux --prefix=/davide/home/userexternal/$USER/cp2k/libs/libint/1.1.4 --with-cc-optflags="${CFLAGS}" --with-cxx-optflags="${CXXFLAGS}"
make
make install
```
