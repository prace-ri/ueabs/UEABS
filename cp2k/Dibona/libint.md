```
module load gcc/8.2.0
CC=gcc  
CXX=g++
CFLAGS="-O2 -ftree-vectorize -g -fno-omit-frame-pointer -march=armv8.1-a -ffast-math"
CXXFLAGS=$CFLAGS
```

Downloaded new configure.guess to allow configure to recognise ARM platform, replacing bin/configure.guess:

<http://git.savannah.gnu.org/gitweb/?p=config.git;a=blob_plain;f=config.guess;hb=HEAD>

```
./configure --prefix=/dibona_home_nfs/$USER/libs/libint/1.1.4 --with-cc-optflags="$CFLAGS" --with-cxx-optflags="$CXXFLAGS"
make
make install
```
