```
module load openmpi3.1.2/gnu8
```

## SLmake.inc:
```
CDEFS         = -DAdd_
FC            = mpif90
CC            = mpicc 
NOOPT         = -O0
FCFLAGS       = -O3 -march=armv8.1-a -mtune=thunderx2t99 -mcpu=thunderx2t99
CCFLAGS       = -O3 -march=armv8.1-a -mtune=thunderx2t99 -mcpu=thunderx2t99
FCLOADER      = $(FC)
CCLOADER      = $(CC)
FCLOADFLAGS   = $(FCFLAGS)
CCLOADFLAGS   = $(CCFLAGS)
ARCH          = ar
ARCHFLAGS     = cr
RANLIB        = ranlib
SCALAPACKLIB  = libscalapack.a
BLASLIB       = /opt/arm/armpl-19.0.0_ThunderX2CN99_RHEL-7_arm-hpc-compiler_19.0_aarch64-linux/lib/libarmpl_lp64.a 
LAPACKLIB     = /opt/arm/armpl-19.0.0_ThunderX2CN99_RHEL-7_arm-hpc-compiler_19.0_aarch64-linux/lib/libarmpl_lp64.a
LIBS          = $(LAPACKLIB) $(BLASLIB)

```

```make -j 32```