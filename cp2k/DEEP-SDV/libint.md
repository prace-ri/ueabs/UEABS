```
CC=gcc  
CXX=g++
CFLAGS="-O2 -ftree-vectorize -g -fno-omit-frame-pointer -march=core-avx2 -ffast-math"
CXXFLAGS=$CFLAGS
./configure --prefix=/path/to/desired/install/location --with-cc-optflags="$CFLAGS" --with-cxx-optflags="$CXXFLAGS" 
make
make install
```
