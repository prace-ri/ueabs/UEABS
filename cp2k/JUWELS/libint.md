```
module load GCC/8.2.0
CC=gcc  
CXX=g++
CFLAGS="-O2 -ftree-vectorize -g -fno-omit-frame-pointer -march=skylake-avx512 -ffast-math"   
CXXFLAGS=$CFLAGS
./configure --prefix=/p/project/cprpb68/$USER/cp2k/libs/libint/1.1.4 --with-cc-optflags="$CFLAGS" --with-cxx-optflags="$CXXFLAGS" 
make -j 20 
make install
```