#!/bin/bash

if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls ../machines/
    echo ""
    echo "Example: ./prepare.sh jeanzay-gpu"
    exit 1
fi
machine_dir="../machines/$1"

cp $machine_dir/env_bench .
cp $machine_dir/batch_medium.slurm .

ln -s ../DeepGalaxy-master/output_bw_512.hdf5 .
