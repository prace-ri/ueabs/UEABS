Medium test case presentation
-----------------------------

This test case performs a training using 512X512 images, with 3 positions per image, as input.

Reference time on Jean-zay with 4 nodes, 16 MPI proces, 16 GPUs, 3 positions and 100 epochs: 

* For 100epochs: ~67ms/sample and 32min30s as time to solution
