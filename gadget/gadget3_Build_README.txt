1. Install FFTW-2,  available at http://www.fftw.org
2. Install GSL,  availavle at http://www.gnu.org/software/gsl
3. Install HDF5,  availavle at http://www.hdfgroup.org/HDF5/
4. Go to Gadget3/
5. Edit Makefile, set:
   CC
   CXX
   GSL_INCL
   GSL_LIBS
   FFTW_INCL
   FFTW_LIBS
   HDF5INCL
   HDF5LIB
6. make CONFIG=Config-Medium.sh
	