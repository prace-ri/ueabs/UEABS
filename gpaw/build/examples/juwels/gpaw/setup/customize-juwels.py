# Setup customisation for gpaw/cuda
import os

# compiler and linker
compiler = 'icc'
mpicompiler = 'mpicc'
mpilinker = 'mpicc'
extra_compile_args = ['-std=c99']

# libraries
libraries = ['z']

# use MKL
library_dirs += [os.environ['MKLROOT'] + '/lib/intel64/']
libraries = ['mkl_intel_lp64', 'mkl_sequential', 'mkl_core']
mpi_libraries += ['mkl_scalapack_lp64', 'mkl_blacs_intelmpi_lp64']

# libxc
library_dirs += [os.environ['LIBXCDIR'] + '/lib']
include_dirs += [os.environ['LIBXCDIR'] + '/include']
libraries += ['xc']

# GPAW defines
define_macros += [('GPAW_NO_UNDERSCORE_CBLACS', '1')]
define_macros += [('GPAW_NO_UNDERSCORE_CSCALAPACK', '1')]
define_macros += [("GPAW_ASYNC",1)]
define_macros += [("GPAW_MPI2",1)]

# ScaLAPACK
scalapack = True

# HDF5
hdf5 = False

