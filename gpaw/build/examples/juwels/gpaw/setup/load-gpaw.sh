#!/bin/bash
module load CUDA/9.2.88
module load Intel/2019.0.117-GCC-7.3.0
module load IntelMPI/2018.4.274
module load imkl/2019.0.117
source $HOME/lib/python-2019-01/load.sh 2016-06
export GPAW_SETUP_PATH=$HOME/lib/gpaw-setups-0.9.11271
export PATH=<BASE>/bin:$PATH
export PYTHONPATH=<BASE>/lib/python2.7/site-packages:$PYTHONPATH
