### GPAW installation script for JUWELS

# version numbers (modify if needed)
gpaw_version=1.1.0

# installation directory (modify!)
tgt=$HOME/lib/gpaw-${gpaw_version}

# setup build environment
module load CUDA/9.2.88
module load Intel/2019.0.117-GCC-7.3.0
module load IntelMPI/2018.4.274
module load imkl/2019.0.117
source $HOME/lib/python-2019-01/load.sh 2016-06
export GPAW_SETUP_PATH=$HOME/lib/gpaw-setups-0.9.11271
export CFLAGS=""

# gpaw
git clone https://gitlab.com/mlouhivu/gpaw.git gpaw-$gpaw_version
cd gpaw-$gpaw_version
git checkout $gpaw_version
patch gpaw/eigensolvers/rmm_diis.py ../setup/patch-rmmdiis.diff
cp ../setup/customize-juwels.py .
python setup.py install --customize=customize-juwels.py --prefix=$tgt 2>&1 | tee loki-inst
cd ..
sed -e "s|<BASE>|$tgt|g" -e "s|<PYTHONHOME>|$PYTHONHOME|" setup/load-gpaw.sh > $tgt/load.sh

# fix permissions
chmod -R g+rwX $tgt
chmod -R o+rX $tgt
