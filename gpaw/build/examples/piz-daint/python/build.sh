### Python installation script for Piz Daint
###   uses --prefix to set a custom installation directory

# version numbers (modify if needed)
python_version=2.7.13

# installation directory (modify!)
tgt=$SCRATCH/lib/python-2018-12-cuda

# setup build environment
module swap PrgEnv-cray PrgEnv-gnu
module unload xalt
module load daint-gpu
module load craype-accel-nvidia60
export CC=cc
export CFLAGS='-O3'
export CXX=CC
export CXXFLAGS='-O3'
export FC=ftn
export FFLAGS='-O3'
export LINKFORSHARED='-Wl,-export-dynamic -dynamic'
export MPI_LINKFORSHARED='-Wl,-export-dynamic -dynamic'

# python
git clone https://github.com/python/cpython.git python-$python_version
cd python-$python_version
git checkout v$python_version
./configure --prefix=$tgt --enable-shared --disable-ipv6 --enable-unicode=ucs4 2>&1 | tee loki-conf
make 2>&1 | tee loki-make
make install 2>&1 | tee loki-inst
cd ..
sed -e "s|<BASE>|$tgt|g" setup/load-python.sh > $tgt/load.sh

# install pip
source $tgt/load.sh
python -m ensurepip
pip install --upgrade pip

# fix permissions
chmod -R g+rwX $tgt
chmod -R o+rX $tgt
