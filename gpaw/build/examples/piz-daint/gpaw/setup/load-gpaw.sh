#!/bin/bash
module swap PrgEnv-cray PrgEnv-gnu
module unload xalt
module load daint-gpu
module load craype-accel-nvidia60
source <PYTHONHOME>/load.sh
export GPAW_SETUP_PATH=$SCRATCH/lib/gpaw-setups-0.9.11271
export PATH=<BASE>/bin:$PATH
export PYTHONPATH=<BASE>/lib/python2.7/site-packages:$PYTHONPATH
