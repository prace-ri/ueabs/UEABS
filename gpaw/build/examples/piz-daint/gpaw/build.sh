### GPAW installation script for Piz Daint

# version numbers (modify if needed)
gpaw_version=cuda

# installation directory (modify!)
tgt=$SCRATCH/lib/gpaw-${gpaw_version}

# setup build environment
module swap PrgEnv-cray PrgEnv-gnu
module unload xalt
module load daint-gpu
module load craype-accel-nvidia60
source $SCRATCH/lib/python-2018-12-cuda/load.sh 2016-06
export GPAW_SETUP_PATH=$SCRATCH/lib/gpaw-setups-0.9.11271
export CFLAGS=""

# gpaw
git clone https://gitlab.com/mlouhivu/gpaw.git gpaw-$gpaw_version
cd gpaw-$gpaw_version
git checkout $gpaw_version
patch gpaw/eigensolvers/rmm_diis.py ../setup/patch-rmmdiis.diff
ln -s ../setup/gcc.py
cd c/cuda
cp ../../../setup/make.inc .
make 2>&1 | tee loki-make
cd -
python setup.py install --customize=../setup/customize-piz-daint.py --prefix=$tgt 2>&1 | tee loki-inst
cd ..
sed -e "s|<BASE>|$tgt|g" -e "s|<PYTHONHOME>|$PYTHONHOME|" setup/load-gpaw.sh > $tgt/load.sh

# fix permissions
chmod -R g+rwX $tgt
chmod -R o+rX $tgt
