#!/bin/bash
#SBATCH -J 4x4
#SBATCH -p dvd_usr_prod
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=4
#SBATCH --time=00:30:00
#SBATCH --account=<PROJECT_ID>
#SBATCH --gres=gpu:tesla:4
#SBATCH --exclusive

cd $SLURM_SUBMIT_DIR

source $CINECA_SCRATCH/lib/gpaw-cuda/load.sh

srun gpaw-python input.py

