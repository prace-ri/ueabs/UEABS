#!/bin/bash -x
#SBATCH -J 4x48
#SBATCH --account=<PROJECT_ID>
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=48
#SBATCH --time=00:30:00
#SBATCH --partition=batch

source $HOME/lib/gpaw-1.1.0/load.sh

srun gpaw-python input.py

