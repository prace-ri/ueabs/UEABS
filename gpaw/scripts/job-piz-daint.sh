#!/bin/bash -l
#SBATCH -J 4x1
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=12
#SBATCH --time=00:30:00
#SBATCH --partition=normal
#SBATCH --constraint=gpu
#SBATCH --gres=gpu:1

export OMP_NUM_THREADS=1
export CRAY_CUDA_MPS=1
export MPICH_NO_GPU_DIRECT=1

source $SCRATCH/lib/gpaw-cuda/load.sh

srun gpaw-python input.py

