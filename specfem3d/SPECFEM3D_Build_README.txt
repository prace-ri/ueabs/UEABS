on CURIE :
flags.guess :
DEF_FFLAGS="-O3 -DFORCE_VECTORIZATION -check nobounds -xHost -ftz
-assume buffered_io -assume byterecl -align sequence -vec-report0 -std03
-diag-disable 6477 -implicitnone -warn truncated_source -warn
argument_checking -warn unused -warn declarations -warn alignments -warn
ignore_loc -warn usage  -mcmodel=medium -shared-intel"

configure command :
./configure MPIFC=mpif90 FC=ifort CC=icc CFLAGS="-mcmodel=medium
-shared-intel" CPP=cpp

in order to compile :
make clean
make all

