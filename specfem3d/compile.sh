#!/bin/bash

echo "==> Choose supercomputer :"
echo "     - occigen"
echo "     - marenostrum "
echo "     - marconi-knl "
echo "     - daint-gpu "
echo "     - daint-cpu-only "
echo "     - davide "
echo "     - juwels"
echo "     - irene-skl "
echo "     - irene-knl "
echo "     - dibona "
echo "     - frioul "
echo "     - deepsdv "
echo "     - hazelhen  "
read machine

source ./env/env_${machine}

Untar(){
    echo "==> Untar "
    #Clone the repository
    cd $install_dir
    git -c http.sslVerify=false clone  https://github.com/geodynamics/specfem3d_globe.git
    cd specfem3d_globe/
    # Checkout of 31 ocotbre 2017 version
    git checkout b1d6ba966496f269611eff8c2cf1f22bcdac2bd9

    #Copy sources
    #source_dir=$HOME/git/specfem3d_globe
    #echo "Copy source"
    #cp -r $source_dir $install_dir
    cd $ueabs_dir
}

Install(){
    echo "===>  Configure "
    answer=0
    echo "1. Test Case A is designed to run on Tier-1 sized systems (up to around 1,000 x86 cores, or equivalent)"
    echo "2. Test Case B is designed to run on Tier-0 sized systems (up to around 10,000 x86 cores, or equivalent)"
    echo "3. Test Case C is designed to run on 1 or 2 node(s)"
    while true
    do
        echo "===> Choose Test Case : (1, 2 or 3)"
        read answer
        if [[ $answer -eq 1 || $answer -eq 2 || $answer -eq 3 ]]; then break;fi
    done
    if [ $answer = "1" ]; then
        echo "===> Copy test case A"
        tar zxvf $HOME/tarballs/SPECFEM3D_TestCaseA.tar.gz
	export install_dir=$install_dir/TestCaseA
	mkdir -p $install_dir
	mv $install_dir/../specfem3d_globe  $install_dir/.
        cp SPECFEM3D_TestCaseA/* $install_dir/specfem3d_globe/DATA/.
        rm -rf SPECFEM3D_TestCaseA
    elif [ $answer = "2" ]; then
        echo "===> Copy test case B"
	export install_dir=$install_dir/TestCaseB
	mkdir -p $install_dir
	mv $install_dir/../specfem3d_globe  $install_dir/.
        tar zxvf $HOME/tarballs/SPECFEM3D_TestCaseB.tar.gz
        cp SPECFEM3D_TestCaseB/* $install_dir/specfem3d_globe/DATA/.
        rm -rf SPECFEM3D_TestCaseB
    else
	echo "===> Copy test case C"
        export install_dir=$install_dir/TestCaseC
        mkdir -p $install_dir
        mv $install_dir/../specfem3d_globe  $install_dir/.
        cp $ueabs_dir/test_cases/SPECFEM3D_TestCaseC/* $install_dir/specfem3d_globe/DATA/.

    fi
    cd $install_dir/specfem3d_globe
    if [ $machine = "daint-gpu" ] || [ $machine = "davide" ]; then
	echo "Configure for CPU+GPU system"
	### replace `use mpi` if needed ###
	#cd utils
 	#perl replace_use_mpi_with_include_mpif_dot_h.pl
 	#cd ..
	sed -i s/"GPU_MODE                        = .false."/"GPU_MODE                        = .true."/g  $install_dir/specfem3d_globe/DATA/Par_file
	if [ $machine = "daint-gpu" ];then
		export MPI_INC=$MPICH_DIR/include
		export CUDA_LIB="$CUDATOOLKIT_HOME/lib64"
		export CUDA_INC="$CUDATOOLKIT_HOME/include"
	fi
	./configure --build=ppc64 --with-cuda=cuda5
    else
	echo "Configure for CPU only system"
        ./configure  --enable-openmp
    fi
    echo $machine
    #if [ $machine = "occigen" ] || [ $machine = "marenostrum" ] || [ $machine = "marconi-knl" ];then
    #    echo "===> Edit Par_file (MemPerCor=50... )"
    #    sed -i s/"MEMORY_INSTALLED_PER_CORE_IN_GB = 2.5d0"/"MEMORY_INSTALLED_PER_CORE_IN_GB = 50d0"/g DATA/Par_file
    #fi
    echo "===> Build"
    make clean
    time -p make -j 12 all >& make.log &
}

Clean(){
	if [ ! -z "$install_dir" ] && [ -d $install_dir ];then
		echo "==> Clean installation directory"
		answer=0
		while true
                do
                    echo "===> Choose Test Case to delete : (1, 2, 3)"
                    echo "1 : $install_dir/TestCaseA"
                    echo "2 : $install_dir/TestCaseB"
                    echo "3 : $install_dir/TestCaseC"
                    read answer
                    if [[ $answer -eq 1 || $answer -eq 2 || $answer -eq 3 ]]; then break;fi
                done
                if [ $answer = "1" ]; then
                        rm -rf $install_dir/TestCaseA
                elif [ $answer = "2" ]; then
                        rm -rf $install_dir/TestCaseB
                else
                        rm -rf $install_dir/TestCaseC
                fi
	else
		echo "Error with install_dir variable"
		exit
	fi
}

Deploy(){
    echo "install_dir ="$install_dir
    if [ $machine = "occigen" ] || [ $machine = "marenostrum" ] || [ $machine = "marconi-knl" ] || [ $machine = "daint-cpu-only" ] || [ $machine = "daint-gpu" ]  || [ $machine = "davide" ] || [ $machine = "juwels" ] || [ $machine = "irene-skl" ] || [ $machine = "irene-knl" ] || [ $machine = "dibona" ] || [ $machine = "frioul" ]  || [ $machine = "deepsdv" ] || [ $machine = "hazelhen" ];then
        echo "==> Install on $machine :"
	mkdir -p $install_dir
	Clean
        export ueabs_dir=`pwd`
        Untar
        Install
    else
        echo "Wrong machine !"
        exit
    fi
}

Deploy
