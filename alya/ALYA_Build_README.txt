Alya builds the makefile from the compilation options defined in config.in. In order to build ALYA (Alya.x), please follow these steps:

   - Goto to directory: Executables/unix
   - Edit config.in (some default config.in files can be found in directory configure.in):
     - Select your own MPI wrappers and paths
     - Select size of integers. Default is 4 bytes, For 8 bytes, select -DI8
     - Choose your metis version, metis-4.0 or metis-5.1.0_i8 for 8-bytes integers
   - Configure Alya: ./configure -x nastin parall
   - Compile metis:  make metis4 or make metis5
   - Compile Alya:   make
