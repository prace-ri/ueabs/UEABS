#!/bin/bash
#module load Intel IntelMPI imkl

# the parastation MPI seems to work better than Intel MPI
module load intel-para/2018b-mt
# the next line is important because otherwise the QE configure does not
# recognise JUWELS as a Linux system
unset ARCH
CC=icc FC=ifort MPIF90=mpiifort ./configure --enable-openmp --with-scalapack=intel

