# Quantum Espresso onthe Juwels system (JSC)

## Notes

- before using ```configure``` perform the following command
```bash
unset ARCH
```
otherwise configure will not detect correctly taht Juwels is a Linux system

- On Juwels both Intel and Parastation MPI libraries are available. Parastation may be 
more stable
