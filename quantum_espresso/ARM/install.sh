#
# system supplied modules
module load autoload armcompiler/19.0--binary

# the following is the ARM performance library including BLAS, LAPACK + FFTW 
module load armpl-for-armcompiler/19.0.0--binary

# own-compiled OpenMPI 4.0.0 (for ARM compilers)
module use $HOME/modules
module load openmpi/openmpi-4.0.0_Arm

# QE configure
CC=mpicc FC=mpifort ./configure

# you need also to modify the make.inc since configure does not set it correctly
