#!/bin/bash
# script for DAVIDE 16 cores/node
# - 13 nodes, 4 tasks/node, 4 OMP threads/task
# Below <account> represents the budget
#SBATCH -N13
#SBATCH --gres=gpu:4
#SBATCH -A <account>
#SBATCH --tasks-per-node=4
#SBATCH -p dvd_usr_prod
#SBATCH -t 1:00:00

export OMP_NUM_THREADS=4
srun -v -np 52 ./pw.x -input ./Ta2O5.in -npool 26
