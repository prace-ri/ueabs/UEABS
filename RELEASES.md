# UEABS Releases

## Version 2.1 (PRACE-5IP, April 30, 2019)

 * Updated the benchmark suite to the status as used for the PRACE-5IP benchmarking deliverable D7.5 "Evaluation of Accelerated and Non-accelerated Benchmarks" (April 18, 2019)
 * Removed GENE

## Version 2.0 (PRACE-5IP MS31, May 31, 2018)

 * Reconstructed this versioned git repository from a "flat" web site representation
 * Merged back the (Version 1.2 based) Accelerator Benchmark Suite:
  + Added PFARM and SHOC
  + Added: README_ACC.md files (for the 11 accelerator benchmark applications)

## Version 1.3 (PRACE-4IP, May 31, 2017)

 * CP2K: updated build instructions and datasets
 * QCD: new version of code and dataset
 * ALYA: new datasets
 * Code_Saturne: new code version, build instructions, and datasets
 * Quantum Espresso: new build/run instructions

## Accelerator Benchmark Suite (PRACE-4IP, April 30, 2017)

This started as a fork / subset of UEABS Version 1.2:

 * Removed: Gadget, GENE, NEMO, QCD except kernel E
 * Added: PFARM and SHOC
 * Added: README_ACC.md files

## Version 1.2 (PRACE-4IP MS29, October 31, 2016)

 * GENE: new version of code and additional new dataset.
 * GPAW: new version of code and new dataset.
 * GROMACS: new version of code and updated dataset.
 * NAMD: new version of code and minor build and run instructions updates.
 * NEMO: new version of code and replaced dataset.

## Version 1.1 (PRACE-3IP, May 31, 2014)

 * ALYA: new version of code and new datasets.
 * Code_Saturne: additional large dataset, using tetrahedralelements.
 * CP2K: new build instructions.
 * GPAW: new dataset with reduced runtime.

## Version 1.0 (PRACE-2IP, October 31, 2013)
 * Initial Release
