# QCD
Matter consists of atoms, which in turn consist of nuclei and electrons. The nuclei consist of
neutrons and protons, which comprise quarks bound together by gluons.

The QCD benchmark benefits of several different implementations.
It can be found the different sub-directory `part_1`, `part_2` and `part_cpu`.
