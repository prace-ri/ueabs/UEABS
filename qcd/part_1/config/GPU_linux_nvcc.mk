#NVIDIA GPU configuration file

MPIDIR=/usr/local/packages/mpich2-1.5
OMPDIR=/usr/lib/gcc/x86_64-redhat-linux/4.4.4 #location of libgomp.a
GPUS_PER_NODE=1

NVARCH=sm_35	
CFLAGS = $(DEFINES) -O2 -DARCH=0 -I $(MPIDIR)/include 
LDFLAGS = -lm  -arch=$(NVARCH) -L./targetDP -ltarget -L$(MPIDIR)/lib -lmpich -lmpl -lm -L$(OMPDIR) -lgomp
CC=mpicc
TARGETCC=nvcc
TARGETCFLAGS=-x cu -arch=$(NVARCH) -I. -DCUDA -DVVL=1 -DSoA -DGPUSPN=$(GPUS_PER_NODE) -dc -c $(CFLAGS)

