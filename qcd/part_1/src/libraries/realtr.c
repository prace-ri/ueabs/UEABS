/******************  realtr.c  (in su3.a) *******************************
*									*
* double realtrace_su3_KE( su3_matrix *a,*b)				*
* return Re( Tr( A_adjoint*B )  					*
*/
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"

double realtrace_su3_KE( su3_matrix * a, su3_matrix * b )
{
    register int i, j;
    register double sum;
    for ( sum = 0.0, i = 0; i < 3; i++ )
	for ( j = 0; j < 3; j++ )
	    sum += a->ROWCOL( i, j ).real * b->ROWCOL( i, j ).real + a->ROWCOL( i, j ).imag * b->ROWCOL( i, j ).imag;
    return ( sum );
}
