/************************ generic_wilson_includes.h *********************/
/****************** wi_dyn_includes.h ***********************************/
/************************ generic_includes.h ****************************/


#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/time.h>
#include <sys/times.h>
#include <sys/utsname.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <math.h>
#include "complex.h"
#include "su3.h"
#include "dirs.h"
#include "macros.h"
#include "generic.h"
#include "generic_wilson.h"

//#ifndef INCLUDED_FROM_TARGETSOURCE

#include "comdefs.h"

#ifdef COMM_MPI
#include <mpi.h>
#endif

//#endif

#include "int32type.h"
#include <unistd.h>
#include <string.h>
#include "lattice.h"
#include "io_lat.h"
//#include <malloc.h>
#include <errno.h>
