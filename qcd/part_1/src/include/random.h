#ifndef _RANDOM_H
#define _RANDOM_H

/* Generic random number generator returning a uniformly distributed
   random value on [0,1] */
double myrand( );
void ranstart(  );
void ranend(  );

#endif /* _RANDOM_H */
