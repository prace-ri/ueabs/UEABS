/* machine specific options */
#ifndef _MACHINE_H
#define _MACHINE_H

#define GENERIC 0

/* GENERIC */ 
#if ARCH == GENERIC
#define COMM_MPI
#define COLORSPINOR(i,j)	c[i].d[j]
#define ROWCOL(i,j) 	e[i][j]
#define LINKDIST_32 4
#endif

#endif
