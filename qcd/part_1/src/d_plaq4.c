/************************** d_plaq4.c *******************************/
/* MIMD version 6 */
/* This version mallocs the temporary su3_matrix */

/* Double precision version of "plaquette4.c" including optional
   Schroedinger functional - UMH - 1/27/00 */

/* Measure the average plaquette of the space-space and
   space-time plaquettes */

#include "./include/includes.h"

void d_plaquette( double *ss_plaq, double *st_plaq )
{
    /* su3mat is scratch space of size su3_matrix */
    register int i, dir1, dir2;
    register site *s;
    register su3_matrix *m1, *m4;
    static su3_matrix mtmp __attribute__ ( ( aligned( 64 ) ) );
    su3_matrix *su3_gath;
    double ss_sum, st_sum;
    msg_tag *mtag0, *mtag1;
    ss_sum = st_sum = 0.0;

    MEMALIGN(su3_gath, su3_matrix, sites_on_node);

    for ( dir1 = YUP; dir1 <= TUP; dir1++ )
    {
	for ( dir2 = XUP; dir2 < dir1; dir2++ )
	{

	    mtag0 = start_gather_from_temp( &( gauge[dir2] ), sizeof( su3_matrix ), 
                    4 * sizeof( su3_matrix ), dir1, EVENANDODD, gen_pt[0] );
	    mtag1 = start_gather_from_temp( &( gauge[dir1] ), sizeof( su3_matrix ), 
                    4 * sizeof( su3_matrix ), dir2, EVENANDODD, gen_pt[1] );

	    FORALLSITES( i, s )
	    {
		m1 = &( gauge[4 * i + dir1] );
		m4 = &( gauge[4 * i + dir2] );
		mult_su3_an_KE( m4, m1, &su3_gath[i] );
	    }
	    wait_gather_KE( mtag0 );
	    wait_gather_KE( mtag1 );


	    FORALLSITES( i, s )
	    {
		mult_su3_nn_KE( &su3_gath[i], ( su3_matrix * ) ( gen_pt[0][i] ), &mtmp );
		if( dir1 == TUP )
		    st_sum += ( double ) realtrace_su3_KE( ( su3_matrix * ) ( gen_pt[1][i] ), &mtmp );

		else
		    ss_sum += ( double ) realtrace_su3_KE( ( su3_matrix * ) ( gen_pt[1][i] ), &mtmp );
		/* plaquette in usual definition */
		/*      if(dir1==TUP )st_sum += 1.0-1./3.*(double)
		   realtrace_su3_KE((su3_matrix *)(gen_pt[1][i]),&mtmp);
		   else          ss_sum += 1.0-1./3.*(double)
		   realtrace_su3_KE((su3_matrix *)(gen_pt[1][i]),&mtmp);
		 */
	    }

	    cleanup_gather( mtag0 );
	    cleanup_gather( mtag1 );
	}
    }
    g_doublesum_KE( &ss_sum );
    g_doublesum_KE( &st_sum );
    *ss_plaq = ss_sum / ( ( double ) ( 3 * nx * ny * nz * nt ) );
    *st_plaq = st_sum / ( ( double ) ( 3 * nx * ny * nz * nt ) );
    node0_fprintf( file_o1, "d_plaquette: %.15e\t%.15e\n", *ss_plaq, *st_plaq );

    FREE(su3_gath, su3_matrix, sites_on_node);
}				/* d_plaquette4 */
