/******** layout_bgl.c *********/
#include "includes.h"

int loc_size[4];		/* local dimensions in machine directions */
extern int totnodes[4];		/* number of nodes in machine directions */

void setup_layout_KE(  )
{
    node0_fprintf( file_o1, "setup_layout: 4d evenfirst\n" );

    if( ( nx % totnodes[XUP] ) || ( ny % totnodes[YUP] )
	|| ( nz % totnodes[ZUP] ) || ( nt % totnodes[TUP] ) )
    {
	node0_printf( "ERROR setup_layout: Can't lay out this lattice.\n" );
	exit( 1 );
    }


    loc_size[XUP] = nx / totnodes[XUP];
    loc_size[YUP] = ny / totnodes[YUP];
    loc_size[ZUP] = nz / totnodes[ZUP];
    loc_size[TUP] = nt / totnodes[TUP];
    sites_on_node = loc_size[XUP] * loc_size[YUP] * loc_size[ZUP] * loc_size[TUP];
    even_sites_on_node = odd_sites_on_node = sites_on_node / 2;
    node0_fprintf( file_o1, "setup_layout: local lattice size: %d x %d x %d x %d\n",
		   loc_size[XUP], loc_size[YUP], loc_size[ZUP], loc_size[TUP] );

    if( sites_on_node % 2 != 0 )
    {
	node0_printf( "ERROR steup_layout: we need EVEN sites on node\n" );
	exit( 1 );
    }

}

int node_number_KE( int x, int y, int z, int t )
{
    int coord[4];
    coord[XUP] = x / loc_size[XUP];
    coord[YUP] = y / loc_size[YUP];
    coord[ZUP] = z / loc_size[ZUP];
    coord[TUP] = t / loc_size[TUP];
    return coord[TUP]+totnodes[TUP]*coord[ZUP]+
        totnodes[TUP]*totnodes[ZUP]*coord[YUP]+
        totnodes[TUP]*totnodes[ZUP]*totnodes[YUP]*coord[XUP];
}
/* TODO: largest loc_size[dir] should be the fastest */
int node_index_KE( int x, int y, int z, int t )
{
    int coord[4], i;
    coord[XUP] = x % loc_size[XUP];
    coord[YUP] = y % loc_size[YUP];
    coord[ZUP] = z % loc_size[ZUP];
    coord[TUP] = t % loc_size[TUP];
    i = coord[TUP] + loc_size[TUP]*coord[ZUP]+
        loc_size[TUP]*loc_size[ZUP]*coord[YUP]+
        loc_size[TUP]*loc_size[ZUP]*loc_size[YUP]*coord[XUP];

    if( ( x + y + z + t ) % 2 == 0 )
    {				/* even site */
	return ( i / 2 );
    }
    else
    {
	return ( ( i + sites_on_node ) / 2 );
    }
}

int num_sites( int node )
{
    return ( sites_on_node );
}
