\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}Execution model}{4}{chapter.2}
\contentsline {chapter}{\numberline {3}Memory model}{6}{chapter.3}
\contentsline {section}{\numberline {3.1}Model overview}{6}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Host memory model}{6}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Target memory model}{7}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Implementation}{7}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}C}{7}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}CUDA}{7}{subsection.3.2.2}
\contentsline {chapter}{\numberline {4}Memory Management}{8}{chapter.4}
\contentsline {section}{\numberline {4.1}targetMalloc}{9}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Description}{9}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Syntax}{9}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Example}{9}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Implementation}{9}{subsection.4.1.4}
\contentsline {subsubsection}{C}{9}{section*.3}
\contentsline {subsubsection}{CUDA}{9}{section*.4}
\contentsline {section}{\numberline {4.2}targetCalloc}{10}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Description}{10}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Syntax}{10}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Example}{10}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Implementation}{10}{subsection.4.2.4}
\contentsline {subsubsection}{C}{10}{section*.5}
\contentsline {subsubsection}{CUDA}{10}{section*.6}
\contentsline {section}{\numberline {4.3}targetMallocUnified}{11}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Description}{11}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Syntax}{11}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Example}{11}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}Implementation}{11}{subsection.4.3.4}
\contentsline {subsubsection}{C}{11}{section*.7}
\contentsline {subsubsection}{CUDA}{11}{section*.8}
\contentsline {section}{\numberline {4.4}targetCallocUnified}{12}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Description}{12}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Syntax}{12}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Example}{12}{subsection.4.4.3}
\contentsline {subsection}{\numberline {4.4.4}Implementation}{12}{subsection.4.4.4}
\contentsline {subsubsection}{C}{12}{section*.9}
\contentsline {subsubsection}{CUDA}{12}{section*.10}
\contentsline {section}{\numberline {4.5}targetFree}{13}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Description}{13}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Syntax}{13}{subsection.4.5.2}
\contentsline {subsection}{\numberline {4.5.3}Example}{13}{subsection.4.5.3}
\contentsline {subsection}{\numberline {4.5.4}Implementation}{13}{subsection.4.5.4}
\contentsline {subsubsection}{C}{13}{section*.11}
\contentsline {subsubsection}{CUDA}{13}{section*.12}
\contentsline {section}{\numberline {4.6}copyToTarget}{14}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Description}{14}{subsection.4.6.1}
\contentsline {subsection}{\numberline {4.6.2}Syntax}{14}{subsection.4.6.2}
\contentsline {subsection}{\numberline {4.6.3}Example}{14}{subsection.4.6.3}
\contentsline {subsection}{\numberline {4.6.4}Implementation}{14}{subsection.4.6.4}
\contentsline {subsubsection}{C}{14}{section*.13}
\contentsline {subsubsection}{CUDA}{14}{section*.14}
\contentsline {section}{\numberline {4.7}copyFromTarget}{15}{section.4.7}
\contentsline {subsection}{\numberline {4.7.1}Description}{15}{subsection.4.7.1}
\contentsline {subsection}{\numberline {4.7.2}Syntax}{15}{subsection.4.7.2}
\contentsline {subsection}{\numberline {4.7.3}Example}{15}{subsection.4.7.3}
\contentsline {subsection}{\numberline {4.7.4}Implementation}{15}{subsection.4.7.4}
\contentsline {subsubsection}{C}{15}{section*.15}
\contentsline {subsubsection}{CUDA}{15}{section*.16}
\contentsline {section}{\numberline {4.8}copyDeepDoubleArrayToTarget}{16}{section.4.8}
\contentsline {subsection}{\numberline {4.8.1}Description}{16}{subsection.4.8.1}
\contentsline {subsection}{\numberline {4.8.2}Syntax}{16}{subsection.4.8.2}
\contentsline {subsection}{\numberline {4.8.3}Implementation}{16}{subsection.4.8.3}
\contentsline {subsubsection}{C}{16}{section*.17}
\contentsline {subsubsection}{CUDA}{16}{section*.18}
\contentsline {section}{\numberline {4.9}copyDeepDoubleArrayFromTarget}{17}{section.4.9}
\contentsline {subsection}{\numberline {4.9.1}Description}{17}{subsection.4.9.1}
\contentsline {subsection}{\numberline {4.9.2}Syntax}{17}{subsection.4.9.2}
\contentsline {subsection}{\numberline {4.9.3}Implementation}{17}{subsection.4.9.3}
\contentsline {subsubsection}{C}{17}{section*.19}
\contentsline {subsubsection}{CUDA}{17}{section*.20}
\contentsline {section}{\numberline {4.10}targetZero}{18}{section.4.10}
\contentsline {subsection}{\numberline {4.10.1}Description}{18}{subsection.4.10.1}
\contentsline {subsection}{\numberline {4.10.2}Syntax}{18}{subsection.4.10.2}
\contentsline {subsection}{\numberline {4.10.3}Implementation}{18}{subsection.4.10.3}
\contentsline {subsubsection}{C}{18}{section*.21}
\contentsline {subsubsection}{CUDA}{18}{section*.22}
\contentsline {section}{\numberline {4.11}targetSetConstant}{19}{section.4.11}
\contentsline {subsection}{\numberline {4.11.1}Description}{19}{subsection.4.11.1}
\contentsline {subsection}{\numberline {4.11.2}Syntax}{19}{subsection.4.11.2}
\contentsline {subsection}{\numberline {4.11.3}Implementation}{19}{subsection.4.11.3}
\contentsline {subsubsection}{C}{19}{section*.23}
\contentsline {subsubsection}{CUDA}{19}{section*.24}
\contentsline {section}{\numberline {4.12}targetConst}{20}{section.4.12}
\contentsline {subsection}{\numberline {4.12.1}Description}{20}{subsection.4.12.1}
\contentsline {subsection}{\numberline {4.12.2}Syntax}{20}{subsection.4.12.2}
\contentsline {subsection}{\numberline {4.12.3}Example}{20}{subsection.4.12.3}
\contentsline {subsection}{\numberline {4.12.4}Implementation}{20}{subsection.4.12.4}
\contentsline {subsubsection}{C}{20}{section*.25}
\contentsline {subsubsection}{CUDA}{20}{section*.26}
\contentsline {section}{\numberline {4.13}copyConstToTarget}{21}{section.4.13}
\contentsline {subsection}{\numberline {4.13.1}Description}{21}{subsection.4.13.1}
\contentsline {subsection}{\numberline {4.13.2}Syntax}{21}{subsection.4.13.2}
\contentsline {subsection}{\numberline {4.13.3}Example}{21}{subsection.4.13.3}
\contentsline {subsection}{\numberline {4.13.4}Implementation}{21}{subsection.4.13.4}
\contentsline {subsubsection}{C}{21}{section*.27}
\contentsline {subsubsection}{CUDA}{21}{section*.28}
\contentsline {section}{\numberline {4.14}copyConstFromTarget}{22}{section.4.14}
\contentsline {subsection}{\numberline {4.14.1}Description}{22}{subsection.4.14.1}
\contentsline {subsection}{\numberline {4.14.2}Syntax}{22}{subsection.4.14.2}
\contentsline {subsection}{\numberline {4.14.3}Example}{22}{subsection.4.14.3}
\contentsline {subsection}{\numberline {4.14.4}Implementation}{22}{subsection.4.14.4}
\contentsline {subsubsection}{C}{22}{section*.29}
\contentsline {subsubsection}{CUDA}{22}{section*.30}
\contentsline {section}{\numberline {4.15}targetConstAddress}{23}{section.4.15}
\contentsline {subsection}{\numberline {4.15.1}Description}{23}{subsection.4.15.1}
\contentsline {subsection}{\numberline {4.15.2}Syntax}{23}{subsection.4.15.2}
\contentsline {subsection}{\numberline {4.15.3}Implementation}{23}{subsection.4.15.3}
\contentsline {subsubsection}{C}{23}{section*.31}
\contentsline {subsubsection}{CUDA}{23}{section*.32}
\contentsline {section}{\numberline {4.16}targetInit3D}{24}{section.4.16}
\contentsline {subsection}{\numberline {4.16.1}Description}{24}{subsection.4.16.1}
\contentsline {subsection}{\numberline {4.16.2}Syntax}{24}{subsection.4.16.2}
\contentsline {section}{\numberline {4.17}targetFinalize3D}{25}{section.4.17}
\contentsline {subsection}{\numberline {4.17.1}Description}{25}{subsection.4.17.1}
\contentsline {subsection}{\numberline {4.17.2}Syntax}{25}{subsection.4.17.2}
\contentsline {section}{\numberline {4.18}copyToTargetPointerMap3D}{26}{section.4.18}
\contentsline {subsection}{\numberline {4.18.1}Description}{26}{subsection.4.18.1}
\contentsline {subsection}{\numberline {4.18.2}Syntax}{26}{subsection.4.18.2}
\contentsline {section}{\numberline {4.19}copyFromTargetPointerMap3D}{27}{section.4.19}
\contentsline {subsection}{\numberline {4.19.1}Description}{27}{subsection.4.19.1}
\contentsline {subsection}{\numberline {4.19.2}Syntax}{27}{subsection.4.19.2}
\contentsline {chapter}{\numberline {5}Data Parallel Execution}{28}{chapter.5}
\contentsline {section}{\numberline {5.1}targetEntry}{29}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Description}{29}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Syntax}{29}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Example}{29}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}Implementation}{29}{subsection.5.1.4}
\contentsline {subsubsection}{C}{29}{section*.33}
\contentsline {subsubsection}{CUDA}{29}{section*.34}
\contentsline {section}{\numberline {5.2}target}{30}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Description}{30}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Syntax}{30}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Example}{30}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Implementation}{30}{subsection.5.2.4}
\contentsline {subsubsection}{C}{30}{section*.35}
\contentsline {subsubsection}{CUDA}{30}{section*.36}
\contentsline {section}{\numberline {5.3}targetHost}{31}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Description}{31}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Syntax}{31}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Example}{31}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Implementation}{31}{subsection.5.3.4}
\contentsline {subsubsection}{C}{31}{section*.37}
\contentsline {subsubsection}{CUDA}{31}{section*.38}
\contentsline {section}{\numberline {5.4}targetLaunch}{32}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Description}{32}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}Syntax}{32}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}Example}{32}{subsection.5.4.3}
\contentsline {subsection}{\numberline {5.4.4}Implementation}{32}{subsection.5.4.4}
\contentsline {subsubsection}{C}{32}{section*.39}
\contentsline {subsubsection}{CUDA}{32}{section*.40}
\contentsline {section}{\numberline {5.5}targetSynchronize}{33}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Description}{33}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}Syntax}{33}{subsection.5.5.2}
\contentsline {subsection}{\numberline {5.5.3}Example}{33}{subsection.5.5.3}
\contentsline {subsection}{\numberline {5.5.4}Implementation}{33}{subsection.5.5.4}
\contentsline {subsubsection}{C}{33}{section*.41}
\contentsline {subsubsection}{CUDA}{33}{section*.42}
\contentsline {section}{\numberline {5.6}targetTLP}{34}{section.5.6}
\contentsline {subsection}{\numberline {5.6.1}Description}{34}{subsection.5.6.1}
\contentsline {subsection}{\numberline {5.6.2}Syntax}{34}{subsection.5.6.2}
\contentsline {subsection}{\numberline {5.6.3}Example}{34}{subsection.5.6.3}
\contentsline {subsection}{\numberline {5.6.4}Implementation}{34}{subsection.5.6.4}
\contentsline {subsubsection}{C}{34}{section*.43}
\contentsline {subsubsection}{CUDA}{34}{section*.44}
\contentsline {section}{\numberline {5.7}targetILP}{35}{section.5.7}
\contentsline {subsection}{\numberline {5.7.1}Description}{35}{subsection.5.7.1}
\contentsline {subsection}{\numberline {5.7.2}Syntax}{35}{subsection.5.7.2}
\contentsline {subsection}{\numberline {5.7.3}Example}{35}{subsection.5.7.3}
\contentsline {subsection}{\numberline {5.7.4}Implementation}{35}{subsection.5.7.4}
\contentsline {subsubsection}{C}{35}{section*.45}
\contentsline {subsubsection}{CUDA}{35}{section*.46}
\contentsline {section}{\numberline {5.8}targetCoords3D}{36}{section.5.8}
\contentsline {subsection}{\numberline {5.8.1}Description}{36}{subsection.5.8.1}
\contentsline {subsection}{\numberline {5.8.2}Syntax}{36}{subsection.5.8.2}
\contentsline {section}{\numberline {5.9}targetIndex3D}{37}{section.5.9}
\contentsline {subsection}{\numberline {5.9.1}Description}{37}{subsection.5.9.1}
\contentsline {subsection}{\numberline {5.9.2}Syntax}{37}{subsection.5.9.2}
\contentsline {chapter}{\numberline {6}Example}{38}{chapter.6}
