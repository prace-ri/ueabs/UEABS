##
##
##
##
##

time=$1
nodes=$2
n=$3
g=$4
omp=$5
perm=$6
src=$7
folder=$8
echo Creating submit-script in $folder

cp submit_job_part1.sh.template ${folder}/.
cd $folder
sed 's/#NODES#/'${nodes}'/g' submit_job_part1.sh.template > test
mv test submit_job_part1.temp
sed 's/#NTASK#/'${n}'/g' submit_job_part1.temp > test
mv test submit_job_part1.temp
sed 's/#TASKPERNODE#/'${g}'/g' submit_job_part1.temp > test
mv test submit_job_part1.temp
sed 's/#OMPTHREADS#/'${omp}'/g' submit_job_part1.temp > test
mv test submit_job_part1.temp
sed 's/#TIME#/'${time}'/g' submit_job_part1.temp > test
mv test submit_job_part1.temp
wrc=$(pwd)
echo $wrc
sed 's #WRC# '${wrc}' g' submit_job_part1.temp > test

mv test ${src}

if [ $perm -eq 1 ];then
	chmod +x $src
fi
rm submit_job_part1.temp
rm submit_job_part1.sh.template
cd ..
