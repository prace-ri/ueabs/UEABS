##prepare Kernel E
##
##
##
##

nx=$1
ny=$2
nz=$3
nt=$4
px=$5
py=$6
pz=$7
pt=$8
folder=$9
echo creating input file in $folder

sed 's/#NX#/'${nx}'/g' kernel_E.input_template > test
mv test kernel_E.input_tmp
sed 's/#NY#/'${ny}'/g' kernel_E.input_tmp > test
mv test kernel_E.input_tmp
sed 's/#NZ#/'${nz}'/g' kernel_E.input_tmp > test
mv test kernel_E.input_tmp
sed 's/#NT#/'${nt}'/g' kernel_E.input_tmp > test
mv test kernel_E.input_tmp
sed 's/#PX#/'${px}'/g' kernel_E.input_tmp > test
mv test kernel_E.input_tmp
sed 's/#PY#/'${py}'/g' kernel_E.input_tmp > test
mv test kernel_E.input_tmp
sed 's/#PZ#/'${pz}'/g' kernel_E.input_tmp > test
mv test kernel_E.input_tmp
sed 's/#PT#/'${pt}'/g' kernel_E.input_tmp > test
mv test $folder/kernel_E.input
