##
##  RUN - Part 1
##
##  Before starting this job-script replace "SUBMIT" with the submition-command of the local queing system.
##  Additional in the script submit_job the execution command has to be adjusted to the local machine.
##  
##
##  Script for Part 1 of the UEABS Benchmarksuite
##
#!/bin/bash

EXE=/ccc/cont005/home/unicy/finkenrj/run/part1/bench
## Set scaling-mode: Strong or Weak
sca_mode="Strong"
#sca_mode="OneNode"
#sca_mode="Weak"
mode="Analysis"
##mode="Run"

##	sbatch_on=1
exe_perm=1 ## use chmod to allow execution of submit_job_Nx_Gx.sh

## lattice size (size strong 1)
gx=64
gy=64
gz=64
gt=8
g=8        ##MPItaskperNODE
openmp=6   ##OMP
cpuptask=6 ## CPUPERTASK
## lattice size (size strong 2) - there is no other testcase yet
#gx=64
#gt=128
## lattice size (size weak 1)
#gx=48
#gt=24

## use smaller lattice size of weak scaling mode: like gx=24 gt=24
##

#gy=$gx
#gz=$gx

lt=$gt
lx=$gx
ly=$gy
lz=$gz

#for n in 1 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192 16384; do
#for n in 8; do
for n in 8 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576; do
#for n in 8;do
            px=$n
            py=1
            pz=1
            pt=1
            if [ $n -eq 16 ];then
	        py=2
                px=8
	    fi
	    if [ $n -eq 32 ];then
	        py=4
                px=8
	    fi
            if [ $n -eq 64 ];then
                pt=2
                py=4
                px=8
            fi
            if [ $n -eq 128 ];then
                pz=2
                py=8
                px=8
            fi
            if [ $n -eq 256 ];then
                pz=4
                py=8
                px=8
            fi
            if [ $n -eq 512 ];then
                pz=8
                py=8
                px=8
            fi
            if [ $n -eq 1024 ];then
                pz=8
	        py=8		
                px=8
                pt=2
            fi
            if [ $n -eq 2048 ];then
                pz=8
                py=8
                px=16
                pt=2
            fi
            if [ $n -eq 4096 ];then
                pz=8
                py=16
                px=16
                pt=2
            fi
            if [ $n -eq 8192 ];then
                pz=16
                py=16
                px=16
                pt=2
            fi
            if [ $n -eq 16384 ];then
                pz=16
                py=16
                px=16
                pt=4
            fi
            if [ $n -eq 32768 ];then
                px=16
                py=16
                pz=16
                pt=8
            fi 
            if [ $n -eq 65536 ];then
                px=16
                py=16
                pz=16
                pt=16
            fi
            if [ $n -eq 131072 ];then
                px=16
                py=16
                pz=16
                pt=32
            fi
            if [ $n -eq 262144 ];then
                px=16
                py=32
                pz=32
                pt=16
            fi
            if [ $n -eq 524288 ];then
                px=32
                py=32
                pz=32
                pt=16
            fi
            if [ $n -eq 1048576 ];then
                px=32
                py=32
                pz=32
                pt=32
            fi

            if [ $sca_mode = "Strong" ];then
                lt1=$((gt/pt))
                lx1=$((gx/px))
                ly1=$((gy/py))
                lz1=$((gz/pz))
            elif [ $sca_mode = "OneNode" ]; then
                lx1=$((gx*px))
                ly1=$((gy*py))
                lz1=$((gz*pz))
                lt1=$((gt*pt/g))
                n=$g
                lx=$((gx*px))
                ly=$((gy*py))
                lz=$((gz*pz))
                lt=$((gt*pt))
                px=1
                py=1
                pz=1
                pt=$g
            else
                lt1=$lt
                lx1=$lx
                ly1=$ly
                lz1=$lz

                lt=$((gt*pt))
                lx=$((gx*px))
                ly=$((gy*py))
                lz=$((gz*pz))
            fi
            node=$((n/g))
	    name=${sca_mode}_part1_${px}x${py}x${pz}x${pt}_${lx}x${ly}x${lz}x${lt}_${n}
            folder=N${node}_NtaskpN${g}_${lx}x${ly}x${lz}x${lt}
            if [ $mode != "Analysis" ];then
            	echo $name
	        mkdir $folder
         	submitscript=submit_job_part1_N${n}.sh
                  
		        ./prepare_submit_job_part1.sh '01:30:00' ${node} ${n} ${g} ${openmp} ${cpuptask} ${exe_perm} ${submitscript} ${folder} ${EXE} ${name}
                ./prepare_kernelE_input.sh ${lx} ${ly} ${lz} ${lt} ${px} ${py} ${pz} ${pt} $folder
                cd $folder
                echo sbatch $submitscript
         		ccc_msub ./$submitscript
                sleep 1
                cd ..
            ## Scaning the output and save the data in dat_nameif
      	    else   
                echo $name >> Part1_$mode.log
                less $folder/$name | grep "sec" >> Part1_$mode.log

            fi
    done

