#################
################# UEABS - QCD - BENCHMARKSUITE -- QUICK-USERGUIDE
#################

This is a very short summary of the general step, which has
to be performed, to run the UEABS QCD Benchmarksuite on a new 
machine. More information can be found in the documentation of
the UEABS-QCD BENCHMARKSUITE which is located in in the folder
./PABS/doc/*
or under the web-link

http://www.prace-ri.eu/UEABS/QCD/QCD_Build_README.txt
http://www.prace-ri.eu/UEABS/QCD/QCD_Run_README.txt

The suite works with Jube, which will handle the compilation,
the submission and the analysis of the Benchmarksuite. On a new
machine several xml-files has to be added or created. 
This guide will give a short and very quick overview about
the different steps.

The FIRST STEP on a new machine is to add information about the
system to the platform-folder located in:
./PABS/platform
Here, the new platform has to be added to the xml-file "platform.xml"
similar to the already xml-templates:

..
<platform name="NEW-PLATFORM">
       <params       
	    make            = "make"
            rm              = "rm -f"
            ar              = "ar"
            
            ..
            
	    module_cmd      = "module load"
       />
 </platform>

The SECOND STEP is to provide a dummy-submit script which has to
added to a new subdirectory given by:

./PABS/platform/"NEW-PLATFORM" 

In the THIRD STEP: Go to the home-directory of the UEABS-QCD-Benchmarksuite
located in:
./PABS/applications/QCD/
Note that the source-files of the kernels are located in "./PABS/applications/QCD/src".
Here, similar to STEP ONE the xml-files:

compile.xml, execute.xml and analyse.xml 

has to be edit, i.e. new xml-templates with the new platform-information
has to be added.

In the FOURTH STEP the runs will be setup by creating runs-scripts similar to
"prace-functional-NEW-PLATORM.xml" for a functional test 
and 
"prace-scaling-NEW-PLATORM.xml" for a scaling run.
Here, several limits of the different codes has to be taken into account, see for
this the section "Limitation" at the end of this quick-userguide.

In the FIFTH STEP the benchmark can be compiled and ran by using the command:

perl ../../bench/jube prace-functional-"NEW-PLATFORM".xml

in the directory:
"./PABS/applications/QCD/".
This will generate a folder "tmp" with subfolder in "./PABS/applications/QCD/"
where the source-file will be compiled and executed. If the compilation or the submission 
fails, more information can be found in the subdirectories of "tmp". In any cases 
after the generation of the folder "tmp", compilation and submition can be done,
in principle, without Jube.

In the LAST STEP, the scaling results can be analyzed, by using 
perl ../../bench/jube analyse.xml

LIMITATION:

The different kernels consists of lattice QCD production codes and have several limitations
in parallelization and lattice volume. Kernel A,C,D and E using a four dimensional
lattice while in case of kernel B a three dimensional lattice is used. All kernels
can be parallelized in all direction. The different lattice sizes and parallelization
has to be declared in the scripts: 'prace-functional-"NEW-PLATFORM".xml' or
'prace-scaling-NEW-PLATORM.xml'. The limitation for the different kernel are given by:

"pt * px * py * pz = task"

and additional for the Kernel A, D and E

" nt / pt  modulo 2  = 0 " and   " nt  => 4 "

and the same condition for the other pairs
"{nx,px}, {ny,py}, {nz,pz}". Moreover
the lattice extends nt, nx, ny and nx has to be even and larger
than 4.

#######
#######  Please see for further information the Readme-files
#######  which are provided under
#######
#######  http://www.prace-ri.eu/UEABS/QCD/QCD_Build_README.txt
#######  http://www.prace-ri.eu/UEABS/QCD/QCD_Run_README.txt
#######  or in
#######  ./PABS/doc/*
#######  
#######	 Jacob Finkenrath, 2017
#######