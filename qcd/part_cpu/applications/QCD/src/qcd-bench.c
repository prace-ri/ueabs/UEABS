#include "qcd-diag.h"

int main(int argc, char **argv)
{
  MPI_Init(&argc, &argv);
  
  jube_init();

#ifdef KA_ACTIVE

  jube_kernel_active[0] = 1;
  
  MPI_Barrier(MPI_COMM_WORLD);
  kernel_a();

#else

  jube_kernel_active[0] = 0;

#endif

#ifdef KB_ACTIVE

  jube_kernel_active[1] = 1;

  MPI_Barrier(MPI_COMM_WORLD);
  kernel_b();

#else

  jube_kernel_active[1] = 0;

#endif

#ifdef KC_ACTIVE

  jube_kernel_active[2] = 1;

  MPI_Barrier(MPI_COMM_WORLD);
  kernel_c();

#else

  jube_kernel_active[2] = 0;

#endif

#ifdef KD_ACTIVE

  jube_kernel_active[3] = 1;

  MPI_Barrier(MPI_COMM_WORLD);
  kernel_d();

#else

  jube_kernel_active[3] = 0;

#endif

#ifdef KE_ACTIVE

  jube_kernel_active[4] = 1;

  MPI_Barrier(MPI_COMM_WORLD);
  kernel_e();

#else

  jube_kernel_active[4] = 0;

#endif


  jube_end();

  MPI_Finalize();

  return 0;
}
