!===============================================================================
!
! BQCD -- Berlin Quantum ChromoDynamics program
!
! Author: Hinnerk Stueben <stueben@zib.de>
!
! Copyright (C) 1998-2002, Hinnerk Stueben, Zuse-Institut Berlin
!
!-------------------------------------------------------------------------------
!
! service-hitachi.F90 - calls to service routines on HITACHI
!
!-------------------------------------------------------------------------------
# include "defs.h"

!-------------------------------------------------------------------------------
subroutine abbruch()

  call fexit(1)
end

!-------------------------------------------------------------------------------
function rechner()  ! returns hostname

  character(len = 20) rechner

  call hostnm(rechner)
end

!-------------------------------------------------------------------------------
SECONDS function sekunden()

  implicit none

!!real(8) d
!!call xclock(d, 5)
!!sekunden = d

  real(8) dwalltime
  sekunden = dwalltime()
end

!-------------------------------------------------------------------------------
subroutine pxfgetarg(iarg, arg, larg, status)

  implicit none
  integer :: iarg, larg, status
  character(len = *) :: arg
  character(len(arg) + 1) :: a

  call getarg(iarg + 1, a)

  larg = len_trim(a)
  arg = a
  status = 0
end

!-------------------------------------------------------------------------------
integer function ipxfargc()
  ipxfargc = iargc() - 1
end

!-------------------------------------------------------------------------------
logical function is_big_endian()
  is_big_endian = .true.
end

!===============================================================================
