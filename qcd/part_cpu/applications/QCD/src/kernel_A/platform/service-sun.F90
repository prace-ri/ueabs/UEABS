!===============================================================================
!
! BQCD -- Berlin Quantum ChromoDynamics program
!
! Author: Hinnerk Stueben <stueben@zib.de>
!
! Copyright (C) 1998-2002, Hinnerk Stueben, Zuse-Institut Berlin
!
!-------------------------------------------------------------------------------
!
! service-sun.F90 - calls to service routines on SUN
!
!-------------------------------------------------------------------------------
# include "defs.h"

!-------------------------------------------------------------------------------
subroutine abbruch()

  call exit(1)
end

!-------------------------------------------------------------------------------
function rechner()  ! returns hostname

  character(len = 20) rechner

  i = hostnm(rechner)
end

!-------------------------------------------------------------------------------
SECONDS function sekunden()

  real time(2)

  call etime(time)
  sekunden = time(1)
end

!-------------------------------------------------------------------------------
subroutine pxfgetarg(iarg, arg, larg, status)

  implicit none
  integer :: iarg, larg, status
  character(len = *) :: arg
  character(len(arg) + 1) :: a

  call getarg(iarg, a)

  larg = len_trim(a)
  arg = a
  status = 0
end

!-------------------------------------------------------------------------------
integer function ipxfargc()
  ipxfargc = iargc()
end

!-------------------------------------------------------------------------------
logical function is_big_endian()
  is_big_endian = .false.
end

!===============================================================================
