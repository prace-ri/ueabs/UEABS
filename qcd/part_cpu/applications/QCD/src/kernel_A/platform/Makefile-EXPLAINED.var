#===============================================================================
#
# BQCD -- Berlin Quantum ChromoDynamics programme
#
# Author: Hinnerk Stueben <stueben@zib.de>
#
# Copyright (C) 1998-2006, Hinnerk Stueben, Zuse-Institut Berlin
#
#-------------------------------------------------------------------------------
#
# Makefile-EXPLAINED.var  
#
#-------------------------------------------------------------------------------

SHELL     = /bin/ksh

MODULES_FLAG = -I$(MODULES_DIR)  # how to find "modules"
                                 # MODULES_DIR is set in every Makefile

FPP       = cpp -C -P    # Fortran preprocessor / ANSI C preprocessor
F90       = f90          # Fortran90 compiler
CC        = cc           # ANSI C compiler
AR        = ar           # ar command
RANLIB    = echo         # "ranlib" if necessary

MYFLAGS   = -DTIMING     # or <empty> to switch time measurement off
                         # must always be set for benchmarking
            -DD3_BUFFER_VOL=24*24*12*12 
                         # -sample for lattice='24 24 24 48',processes='1 1 2 4'
                         # - largest possible size of a local lattice boundary
                         # - determines the size of *static* arrays
                         # - only needed in libd3.a
                         # - should be set to 1 when using a different LIBD.
            -D_OPENMP    # has to be explicitly defined for OpenMP
 
FFLAGS    = -O3          # Fortran90 compiler flags
            $(MODULES_FLAG)
CFLAGS    = -O3          # C compiles flags
ARFLAGS   = rv           # ar flags 

LDFLAGS   =              # loader flags (the loader is: ${F90})
SYSLIBS   =              # system libraries (e.g. for BLAS)

FAST_MAKE = gmake -j 8   # parallel make

CKSUM_O   = cksum.o      # do not change
RANDOM_O  = ran.o ranf.o # or "ran.o" on Crays    
UUU_O     = uuu_f90.o    # or "uuu_fwd.o uuu_bwd.o uuu_bwd_m.o" if C is
                         # faster than Fortran90 (was a small effect on T3E)

LIBD      = libd.a       # Multiplication with Wilson hopping term:
                         #  libd.a:  Cray T3E version (MPI or shmem)
                         #  libd2.a: Hitachi SR8000 version (MPI)
                         #  libd3.a: Hitachi SR8000 version (MPI+OpenMP)
                         #  libd21.a: version for high scalability

LIBCOMM   = lib_mpi.a    # or "lib_single_pe.a" or "lib_shmempi.a"

LIBCLOVER = libclover.a  # do not change

#===============================================================================
