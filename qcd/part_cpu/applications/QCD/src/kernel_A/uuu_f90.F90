!===============================================================================
!
! BQCD -- Berlin Quantum ChromoDynamics program
!
! Author: Hinnerk Stueben <stueben@zib.de>
!
! Copyright (C) 1998-2001, Hinnerk Stueben, Zuse Institute Berlin
!
!-------------------------------------------------------------------------------
!
! uuu_f90.F90 - Fortran loops for (U * U * U)
!
!-------------------------------------------------------------------------------
# include "defs.h"

!-------------------------------------------------------------------------------
subroutine uuu_bwd(r, a, b, c)  ! adds backward staple:
                                ! r = r + a^\dagger b^\dagger c
  implicit none
  SU3 :: r, a, b, c
  integer :: i, j, k, m

  do i = 1, NCOL
     do j = 1, NCOL
        do k = 1, NCOL
           do m = 1, NCOL
              r(i,j) = r(i,j) + conjg(a(k,i)) * conjg(b(m,k)) * c(m,j)
           enddo
        enddo
     enddo
  enddo

end

!-------------------------------------------------------------------------------
subroutine uuu_bwd_m(r, a, b, c)  ! subtracts backward staple:
                                  ! r = r - a^\dagger b^\dagger c
  implicit none
  SU3 :: r, a, b, c
  integer :: i, j, k, m

  do i = 1, NCOL
     do j = 1, NCOL
        do k = 1, NCOL
           do m = 1, NCOL
              r(i,j) = r(i,j) - conjg(a(k,i)) * conjg(b(m,k)) * c(m,j)
           enddo
        enddo
     enddo
  enddo

end

!-------------------------------------------------------------------------------
subroutine uuu_fwd(r, a, b, c)  ! adds forward staple:
                                ! r = r + a b^\dagger c^\dagger
  implicit none
  SU3 :: r, a, b, c
  integer :: i, j, k, m

  do i = 1, NCOL
     do j = 1, NCOL
        do k = 1, NCOL
           do m = 1, NCOL
              r(i,j) = r(i,j) + a(i,k) * conjg(b(m,k)) * conjg(c(j,m))
           enddo
        enddo
     enddo
  enddo

end

!===============================================================================
