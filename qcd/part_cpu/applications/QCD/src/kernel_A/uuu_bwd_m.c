/*
!===============================================================================
!
! BQCD -- Berlin Quantum ChromoDynamics program
!
! Author: Hinnerk Stueben <stueben@zib.de>
!
! Copyright (C) 1998-2002, Hinnerk Stueben, Zuse-Institut Berlin
!
!-------------------------------------------------------------------------------
!
! uuu_bwd_m.c - subtracts backward staple:  r = r - a^\dagger b^\dagger c
!
!-------------------------------------------------------------------------------
*/

#include "types.h"

#ifdef NamesToLower_
# define UUU_BWD_M uuu_bwd_m_
#endif

#ifdef NamesToLower
# define UUU_BWD_M uuu_bwd_m
#endif

void UUU_BWD_M(r, a, b, c)
COMPLEX8 *r, *a, *b, *c;
{
    register COMPLEX8 q__1, q__2;
    register COMPLEX8 t1, t2, t3, x1, x2, x3;

    /* Parameter adjustments */
    c -= 4;
    b -= 4;
    a -= 4;
    r -= 4;

    /* Function Body */
    q__1.r = -a[4].r * b[4].r + a[4].i * b[4].i,
    q__1.i = -a[4].r * b[4].i - a[4].i * b[4].r;
    t1.r = q__1.r,
    t1.i = q__1.i;
    q__1.r = -a[4].r * b[5].r + a[4].i * b[5].i,
    q__1.i = -a[4].r * b[5].i - a[4].i * b[5].r;
    t2.r = q__1.r,
    t2.i = q__1.i;
    q__1.r = -a[4].r * b[6].r + a[4].i * b[6].i,
    q__1.i = -a[4].r * b[6].i - a[4].i * b[6].r;
    t3.r = q__1.r,
    t3.i = q__1.i;
    q__2.r = -a[5].r * b[7].r + a[5].i * b[7].i,
    q__2.i = -a[5].r * b[7].i - a[5].i * b[7].r;
    q__1.r = t1.r + q__2.r,
    q__1.i = t1.i + q__2.i;
    t1.r = q__1.r,
    t1.i = q__1.i;
    q__2.r = -a[5].r * b[8].r + a[5].i * b[8].i,
    q__2.i = -a[5].r * b[8].i - a[5].i * b[8].r;
    q__1.r = t2.r + q__2.r,
    q__1.i = t2.i + q__2.i;
    t2.r = q__1.r,
    t2.i = q__1.i;
    q__2.r = -a[5].r * b[9].r + a[5].i * b[9].i,
    q__2.i = -a[5].r * b[9].i - a[5].i * b[9].r;
    q__1.r = t3.r + q__2.r,
    q__1.i = t3.i + q__2.i;
    t3.r = q__1.r,
    t3.i = q__1.i;
    q__2.r = -a[6].r * b[10].r + a[6].i * b[10].i,
    q__2.i = -a[6].r * b[10].i - a[6].i * b[10].r;
    q__1.r = t1.r + q__2.r,
    q__1.i = t1.i + q__2.i;
    t1.r = q__1.r,
    t1.i = -q__1.i;
    q__2.r = -a[6].r * b[11].r + a[6].i * b[11].i,
    q__2.i = -a[6].r * b[11].i - a[6].i * b[11].r;
    q__1.r = t2.r + q__2.r,
    q__1.i = t2.i + q__2.i;
    t2.r = q__1.r,
    t2.i = -q__1.i;
    q__2.r = -a[6].r * b[12].r + a[6].i * b[12].i,
    q__2.i = -a[6].r * b[12].i - a[6].i * b[12].r;
    q__1.r = t3.r + q__2.r,
    q__1.i = t3.i + q__2.i;
    t3.r = q__1.r,
    t3.i = -q__1.i;

    q__2.r = t1.r * c[4].r - t1.i * c[4].i,
    q__2.i = t1.r * c[4].i + t1.i * c[4].r;
    q__1.r = r[4].r + q__2.r,
    q__1.i = r[4].i + q__2.i;
    x1.r = q__1.r,
    x1.i = q__1.i;
    q__2.r = t1.r * c[7].r - t1.i * c[7].i,
    q__2.i = t1.r * c[7].i + t1.i * c[7].r;
    q__1.r = r[7].r + q__2.r,
    q__1.i = r[7].i + q__2.i;
    x2.r = q__1.r,
    x2.i = q__1.i;
    q__2.r = t1.r * c[10].r - t1.i * c[10].i,
    q__2.i = t1.r * c[10].i + t1.i * c[10].r;
    q__1.r = r[10].r + q__2.r,
    q__1.i = r[10].i + q__2.i;
    x3.r = q__1.r,
    x3.i = q__1.i;
    q__2.r = t2.r * c[5].r - t2.i * c[5].i,
    q__2.i = t2.r * c[5].i + t2.i * c[5].r;
    q__1.r = x1.r + q__2.r,
    q__1.i = x1.i + q__2.i;
    x1.r = q__1.r,
    x1.i = q__1.i;
    q__2.r = t2.r * c[8].r - t2.i * c[8].i,
    q__2.i = t2.r * c[8].i + t2.i * c[8].r;
    q__1.r = x2.r + q__2.r,
    q__1.i = x2.i + q__2.i;
    x2.r = q__1.r,
    x2.i = q__1.i;
    q__2.r = t2.r * c[11].r - t2.i * c[11].i,
    q__2.i = t2.r * c[11].i + t2.i * c[11].r;
    q__1.r = x3.r + q__2.r,
    q__1.i = x3.i + q__2.i;
    x3.r = q__1.r,
    x3.i = q__1.i;
    q__2.r = t3.r * c[6].r - t3.i * c[6].i,
    q__2.i = t3.r * c[6].i + t3.i * c[6].r;
    q__1.r = x1.r + q__2.r,
    q__1.i = x1.i + q__2.i;
    r[4].r = q__1.r,
    r[4].i = q__1.i;
    q__2.r = t3.r * c[9].r - t3.i * c[9].i,
    q__2.i = t3.r * c[9].i + t3.i * c[9].r;
    q__1.r = x2.r + q__2.r,
    q__1.i = x2.i + q__2.i;
    r[7].r = q__1.r,
    r[7].i = q__1.i;
    q__2.r = t3.r * c[12].r - t3.i * c[12].i,
    q__2.i = t3.r * c[12].i + t3.i * c[12].r;
    q__1.r = x3.r + q__2.r,
    q__1.i = x3.i + q__2.i;
    r[10].r = q__1.r,
    r[10].i = q__1.i;

    q__1.r = -a[7].r * b[4].r + a[7].i * b[4].i,
    q__1.i = -a[7].r * b[4].i - a[7].i * b[4].r;
    t1.r = q__1.r,
    t1.i = q__1.i;
    q__1.r = -a[7].r * b[5].r + a[7].i * b[5].i,
    q__1.i = -a[7].r * b[5].i - a[7].i * b[5].r;
    t2.r = q__1.r,
    t2.i = q__1.i;
    q__1.r = -a[7].r * b[6].r + a[7].i * b[6].i,
    q__1.i = -a[7].r * b[6].i - a[7].i * b[6].r;
    t3.r = q__1.r,
    t3.i = q__1.i;
    q__2.r = -a[8].r * b[7].r + a[8].i * b[7].i,
    q__2.i = -a[8].r * b[7].i - a[8].i * b[7].r;
    q__1.r = t1.r + q__2.r,
    q__1.i = t1.i + q__2.i;
    t1.r = q__1.r,
    t1.i = q__1.i;
    q__2.r = -a[8].r * b[8].r + a[8].i * b[8].i,
    q__2.i = -a[8].r * b[8].i - a[8].i * b[8].r;
    q__1.r = t2.r + q__2.r,
    q__1.i = t2.i + q__2.i;
    t2.r = q__1.r,
    t2.i = q__1.i;
    q__2.r = -a[8].r * b[9].r + a[8].i * b[9].i,
    q__2.i = -a[8].r * b[9].i - a[8].i * b[9].r;
    q__1.r = t3.r + q__2.r,
    q__1.i = t3.i + q__2.i;
    t3.r = q__1.r,
    t3.i = q__1.i;
    q__2.r = -a[9].r * b[10].r + a[9].i * b[10].i,
    q__2.i = -a[9].r * b[10].i - a[9].i * b[10].r;
    q__1.r = t1.r + q__2.r,
    q__1.i = t1.i + q__2.i;
    t1.r = q__1.r,
    t1.i = -q__1.i;
    q__2.r = -a[9].r * b[11].r + a[9].i * b[11].i,
    q__2.i = -a[9].r * b[11].i - a[9].i * b[11].r;
    q__1.r = t2.r + q__2.r,
    q__1.i = t2.i + q__2.i;
    t2.r = q__1.r,
    t2.i = -q__1.i;
    q__2.r = -a[9].r * b[12].r + a[9].i * b[12].i,
    q__2.i = -a[9].r * b[12].i - a[9].i * b[12].r;
    q__1.r = t3.r + q__2.r,
    q__1.i = t3.i + q__2.i;
    t3.r = q__1.r,
    t3.i = -q__1.i;

    q__2.r = t1.r * c[4].r - t1.i * c[4].i,
    q__2.i = t1.r * c[4].i + t1.i * c[4].r;
    q__1.r = r[5].r + q__2.r,
    q__1.i = r[5].i + q__2.i;
    x1.r = q__1.r,
    x1.i = q__1.i;
    q__2.r = t1.r * c[7].r - t1.i * c[7].i,
    q__2.i = t1.r * c[7].i + t1.i * c[7].r;
    q__1.r = r[8].r + q__2.r,
    q__1.i = r[8].i + q__2.i;
    x2.r = q__1.r,
    x2.i = q__1.i;
    q__2.r = t1.r * c[10].r - t1.i * c[10].i,
    q__2.i = t1.r * c[10].i + t1.i * c[10].r;
    q__1.r = r[11].r + q__2.r,
    q__1.i = r[11].i + q__2.i;
    x3.r = q__1.r,
    x3.i = q__1.i;
    q__2.r = t2.r * c[5].r - t2.i * c[5].i,
    q__2.i = t2.r * c[5].i + t2.i * c[5].r;
    q__1.r = x1.r + q__2.r,
    q__1.i = x1.i + q__2.i;
    x1.r = q__1.r,
    x1.i = q__1.i;
    q__2.r = t2.r * c[8].r - t2.i * c[8].i,
    q__2.i = t2.r * c[8].i + t2.i * c[8].r;
    q__1.r = x2.r + q__2.r,
    q__1.i = x2.i + q__2.i;
    x2.r = q__1.r,
    x2.i = q__1.i;
    q__2.r = t2.r * c[11].r - t2.i * c[11].i,
    q__2.i = t2.r * c[11].i + t2.i * c[11].r;
    q__1.r = x3.r + q__2.r,
    q__1.i = x3.i + q__2.i;
    x3.r = q__1.r,
    x3.i = q__1.i;
    q__2.r = t3.r * c[6].r - t3.i * c[6].i,
    q__2.i = t3.r * c[6].i + t3.i * c[6].r;
    q__1.r = x1.r + q__2.r,
    q__1.i = x1.i + q__2.i;
    r[5].r = q__1.r,
    r[5].i = q__1.i;
    q__2.r = t3.r * c[9].r - t3.i * c[9].i,
    q__2.i = t3.r * c[9].i + t3.i * c[9].r;
    q__1.r = x2.r + q__2.r,
    q__1.i = x2.i + q__2.i;
    r[8].r = q__1.r,
    r[8].i = q__1.i;
    q__2.r = t3.r * c[12].r - t3.i * c[12].i,
    q__2.i = t3.r * c[12].i + t3.i * c[12].r;
    q__1.r = x3.r + q__2.r,
    q__1.i = x3.i + q__2.i;
    r[11].r = q__1.r,
    r[11].i = q__1.i;

    q__1.r = -a[10].r * b[4].r + a[10].i * b[4].i,
    q__1.i = -a[10].r * b[4].i - a[10].i * b[4].r;
    t1.r = q__1.r,
    t1.i = q__1.i;
    q__1.r = -a[10].r * b[5].r + a[10].i * b[5].i,
    q__1.i = -a[10].r * b[5].i - a[10].i * b[5].r;
    t2.r = q__1.r,
    t2.i = q__1.i;
    q__1.r = -a[10].r * b[6].r + a[10].i * b[6].i,
    q__1.i = -a[10].r * b[6].i - a[10].i * b[6].r;
    t3.r = q__1.r,
    t3.i = q__1.i;
    q__2.r = -a[11].r * b[7].r + a[11].i * b[7].i,
    q__2.i = -a[11].r * b[7].i - a[11].i * b[7].r;
    q__1.r = t1.r + q__2.r,
    q__1.i = t1.i + q__2.i;
    t1.r = q__1.r,
    t1.i = q__1.i;
    q__2.r = -a[11].r * b[8].r + a[11].i * b[8].i,
    q__2.i = -a[11].r * b[8].i - a[11].i * b[8].r;
    q__1.r = t2.r + q__2.r,
    q__1.i = t2.i + q__2.i;
    t2.r = q__1.r,
    t2.i = q__1.i;
    q__2.r = -a[11].r * b[9].r + a[11].i * b[9].i,
    q__2.i = -a[11].r * b[9].i - a[11].i * b[9].r;
    q__1.r = t3.r + q__2.r,
    q__1.i = t3.i + q__2.i;
    t3.r = q__1.r,
    t3.i = q__1.i;
    q__2.r = -a[12].r * b[10].r + a[12].i * b[10].i,
    q__2.i = -a[12].r * b[10].i - a[12].i * b[10].r;
    q__1.r = t1.r + q__2.r,
    q__1.i = t1.i + q__2.i;
    t1.r = q__1.r,
    t1.i = -q__1.i;
    q__2.r = -a[12].r * b[11].r + a[12].i * b[11].i,
    q__2.i = -a[12].r * b[11].i - a[12].i * b[11].r;
    q__1.r = t2.r + q__2.r,
    q__1.i = t2.i + q__2.i;
    t2.r = q__1.r,
    t2.i = -q__1.i;
    q__2.r = -a[12].r * b[12].r + a[12].i * b[12].i,
    q__2.i = -a[12].r * b[12].i - a[12].i * b[12].r;
    q__1.r = t3.r + q__2.r,
    q__1.i = t3.i + q__2.i;
    t3.r = q__1.r,
    t3.i = -q__1.i;

    q__2.r = t1.r * c[4].r - t1.i * c[4].i,
    q__2.i = t1.r * c[4].i + t1.i * c[4].r;
    q__1.r = r[6].r + q__2.r,
    q__1.i = r[6].i + q__2.i;
    x1.r = q__1.r,
    x1.i = q__1.i;
    q__2.r = t1.r * c[7].r - t1.i * c[7].i,
    q__2.i = t1.r * c[7].i + t1.i * c[7].r;
    q__1.r = r[9].r + q__2.r,
    q__1.i = r[9].i + q__2.i;
    x2.r = q__1.r,
    x2.i = q__1.i;
    q__2.r = t1.r * c[10].r - t1.i * c[10].i,
    q__2.i = t1.r * c[10].i + t1.i * c[10].r;
    q__1.r = r[12].r + q__2.r,
    q__1.i = r[12].i + q__2.i;
    x3.r = q__1.r,
    x3.i = q__1.i;
    q__2.r = t2.r * c[5].r - t2.i * c[5].i,
    q__2.i = t2.r * c[5].i + t2.i * c[5].r;
    q__1.r = x1.r + q__2.r,
    q__1.i = x1.i + q__2.i;
    x1.r = q__1.r,
    x1.i = q__1.i;
    q__2.r = t2.r * c[8].r - t2.i * c[8].i,
    q__2.i = t2.r * c[8].i + t2.i * c[8].r;
    q__1.r = x2.r + q__2.r,
    q__1.i = x2.i + q__2.i;
    x2.r = q__1.r,
    x2.i = q__1.i;
    q__2.r = t2.r * c[11].r - t2.i * c[11].i,
    q__2.i = t2.r * c[11].i + t2.i * c[11].r;
    q__1.r = x3.r + q__2.r,
    q__1.i = x3.i + q__2.i;
    x3.r = q__1.r,
    x3.i = q__1.i;
    q__2.r = t3.r * c[6].r - t3.i * c[6].i,
    q__2.i = t3.r * c[6].i + t3.i * c[6].r;
    q__1.r = x1.r + q__2.r,
    q__1.i = x1.i + q__2.i;
    r[6].r = q__1.r,
    r[6].i = q__1.i;
    q__2.r = t3.r * c[9].r - t3.i * c[9].i,
    q__2.i = t3.r * c[9].i + t3.i * c[9].r;
    q__1.r = x2.r + q__2.r,
    q__1.i = x2.i + q__2.i;
    r[9].r = q__1.r,
    r[9].i = q__1.i;
    q__2.r = t3.r * c[12].r - t3.i * c[12].i,
    q__2.i = t3.r * c[12].i + t3.i * c[12].r;
    q__1.r = x3.r + q__2.r,
    q__1.i = x3.i + q__2.i;
    r[12].r = q__1.r,
    r[12].i = q__1.i;

    return;
}
