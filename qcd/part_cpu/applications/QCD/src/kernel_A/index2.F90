!===============================================================================
!
! BQCD -- Berlin Quantum ChromoDynamics programme
!
! Author: Hinnerk Stueben <stueben@zib.de>
!
! Copyright (C) 2006, Hinnerk Stueben, Zuse-Institut Berlin
!
!-------------------------------------------------------------------------------
!
! index2.F90 - more functions for index calculations
!              these functions use modules
!
!-------------------------------------------------------------------------------
# include "defs.h"

!-------------------------------------------------------------------------------
subroutine local2global(home, local, global)

  use module_lattice
  implicit none

  integer, intent(in)                 :: home   ! home process of "local"
  integer, intent(in), dimension(DIM) :: local  ! local coordinates 
  integer, intent(out),dimension(DIM) :: global ! global coordinates

  integer, external                   :: i_global, i_periodic
  integer                             :: coord_home(DIM), i
  

  call unlex(home, DIM, coord_home, npe)

  do i = 1, DIM
     global(i) = i_global(local(i), N(i), coord_home(i))
     global(i) = i_periodic(global(i), L(i))
  enddo

end

!-------------------------------------------------------------------------------
integer function e_o(local)  !// returns EVEN or ODD (0 or 1)

  use module_function_decl
  implicit none

  integer, intent(in), dimension(DIM) :: local  ! local coordinates
  integer, dimension(DIM)             :: global ! global coordinates
  integer, external                   :: i_e_o

  call local2global(my_pe(), local, global)

  e_o = i_e_o(DIM, global)
end

!===============================================================================
