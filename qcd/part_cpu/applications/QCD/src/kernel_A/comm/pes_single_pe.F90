!===============================================================================
!
! BQCD -- Berlin Quantum ChromoDynamics program
!
! Author: Hinnerk Stueben <stueben@zib.de>
!
! Copyright (C) 1998-2001, Hinnerk Stueben, Zuse Institute Berlin
!
!-------------------------------------------------------------------------------
!
! pes_single_pe.F90 - dummy routines for shmem functions
!
!-------------------------------------------------------------------------------
integer function my_pe()
  my_pe = 0
end

!-------------------------------------------------------------------------------
integer function num_pes()
  num_pes = 1
end

!-------------------------------------------------------------------------------
subroutine barrier()
  return
end

!===============================================================================
