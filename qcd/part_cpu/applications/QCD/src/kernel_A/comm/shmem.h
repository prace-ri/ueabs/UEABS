#ifdef ALTIX
# define barrier shmem_barrier_all
# define shmem_broadcast shmem_broadcast8
# define shmem_get shmem_get8
# define shmem_put shmem_put8
# define shpalloc(addr, length, errcode, abort) shpalloc(addr, 2 * (length), errcode, abort)
#endif
