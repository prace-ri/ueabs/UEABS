!===============================================================================
!
! BQCD -- Berlin Quantum ChromoDynamics program
!
! Author: Hinnerk Stueben <stueben@zib.de>
!
! Copyright (C) 2005, Hinnerk Stueben, Zuse-Institut Berlin
!
!-------------------------------------------------------------------------------
!
! comm_shmem.F90 - routines for shmem versions
!
!-------------------------------------------------------------------------------
# include "defs.h"

!-------------------------------------------------------------------------------
subroutine comm_init()
#ifdef ALTIX
  call start_pes(0)
#endif
  return
end

!-------------------------------------------------------------------------------
subroutine comm_finalize()
  return
end

!-------------------------------------------------------------------------------
COMM_METHOD function comm_method()

#ifdef _OPENMP
  comm_method = "shmem + OpenMP"
#else
  comm_method = "shmem"
#endif
end

!-------------------------------------------------------------------------------
integer function get_d3_buffer_vol()
  get_d3_buffer_vol = 0
end

!===============================================================================
