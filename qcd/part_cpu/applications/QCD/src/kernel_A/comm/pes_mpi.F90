!===============================================================================
!
! BQCD -- Berlin Quantum ChromoDynamics program
!
! Author: Hinnerk Stueben <stueben@zib.de>
!
! Copyright (C) 1998-2001, Hinnerk Stueben, Zuse Institute Berlin
!
!-------------------------------------------------------------------------------
!
! pes_mpi.F90 - MPI version of shmem functions
!
!-------------------------------------------------------------------------------
integer function my_pe()

  implicit none
  include 'mpif.h'
  integer  ierror

  call mpi_comm_rank(MPI_COMM_WORLD, my_pe, ierror)
end

!-------------------------------------------------------------------------------
integer function num_pes()

  implicit none
  include 'mpif.h'
  integer  ierror

  call mpi_comm_size(MPI_COMM_WORLD, num_pes, ierror)
end

!===============================================================================
