!===============================================================================
!
! BQCD -- Berlin Quantum ChromoDynamics programme
!
! Author: Hinnerk Stueben <stueben@zib.de>
!
! Copyright (C) 1998-2006, Hinnerk Stueben, Zuse-Institut Berlin
!
!-------------------------------------------------------------------------------
!
! dotprod.F90 - dot product for parallel computers
!
!-------------------------------------------------------------------------------
# include "defs.h"

!-------------------------------------------------------------------------------
REAL function dotprod(a, b, n)
 
  implicit none
  integer  i, n
  REAL     a(n), b(n), s, global_sum
  
  s = ZERO
  !$omp parallel do reduction(+: s)
  do i = 1, n
     s = s + a(i) * b(i)
  enddo

  dotprod = global_sum(s)
 
end

!===============================================================================
