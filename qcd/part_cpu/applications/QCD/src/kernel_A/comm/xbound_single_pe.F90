!===============================================================================
!
! BQCD -- Berlin Quantum ChromoDynamics programme
!
! Author: Hinnerk Stueben <stueben@zib.de>
!
! Copyright (C) 1998-2006, Hinnerk Stueben, Zuse-Institut Berlin
!
!-------------------------------------------------------------------------------
!
! xbound_single_pe.F90 - dummy routines for boundary exchange
!
!-------------------------------------------------------------------------------
# include "defs.h"

!-------------------------------------------------------------------------------
subroutine init_xbound()
  return
end

!-------------------------------------------------------------------------------
subroutine xbound_g(u, eo, mu)

  use module_vol
  implicit none
  integer :: eo, mu
  GAUGE_FIELD :: u

  return
end

!-------------------------------------------------------------------------------
subroutine xbound_g_field(u)

  use module_vol
  implicit none
  GAUGE_FIELD :: u

  return
end

!-------------------------------------------------------------------------------
subroutine xbound_sc_field(array)

  use module_vol
  implicit none
  SPINCOL_FIELD :: array

  return
end

!-------------------------------------------------------------------------------
subroutine xbound_sc2_field(array)

  use module_vol
  implicit none
  SC2_FIELD :: array

  return
end

!-------------------------------------------------------------------------------
subroutine xbound_sc2_field_i(array)

  use module_vol
  implicit none
  SC2_FIELD :: array

  return
end

!===============================================================================
