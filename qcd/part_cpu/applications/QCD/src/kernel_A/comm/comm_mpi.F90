!===============================================================================
!
! BQCD -- Berlin Quantum ChromoDynamics program
!
! Author: Hinnerk Stueben <stueben@zib.de>
!
! Copyright (C) 1998-2005, Hinnerk Stueben, Zuse-Institut Berlin
!
!-------------------------------------------------------------------------------
!
! comm_mpi.F90 - wrapper for MPI routines
!
!-------------------------------------------------------------------------------
# include "defs.h"

!-------------------------------------------------------------------------------
subroutine comm_init()

  implicit none
  include 'mpif.h'
  integer  ierror

!  call mpi_init(ierror)
end

!-------------------------------------------------------------------------------
subroutine comm_finalize()

  implicit none
  include 'mpif.h'
  integer  ierror

!  call mpi_finalize(ierror)
end

!-------------------------------------------------------------------------------
COMM_METHOD function comm_method()

#ifdef _OPENMP
  comm_method = "MPI + OpenMP"
#else
  comm_method = "MPI"
#endif
end

!===============================================================================
