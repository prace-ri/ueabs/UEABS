typedef int     INTSTD;
typedef short   INT4;
typedef long    INT8;

typedef double  REALSTD;
typedef float   REAL4;
typedef double  REAL8;

typedef struct { REAL4 r, i; }  COMPLEX4;
typedef struct { REAL8 r, i; }  COMPLEX8;
