!===============================================================================
!
! BQCD -- Berlin Quantum ChromoDynamics program
!
! Author: Hinnerk Stueben <stueben@zib.de>
!
! Copyright (C) 1998-2001, Hinnerk Stueben, Zuse Institute Berlin
!
!-------------------------------------------------------------------------------
!
! clover_uuuu.F90 - multiplications of four SU(3) matrices
!
!-------------------------------------------------------------------------------
!
!    --<--     --<--
!   |     |   |     |
!   v  2  ^   v  1  ^
!   |     |   |     |
!    -->--     -->--
!           x       
!    --<--     --<--
!   |     |   |     |
!   v  3  ^   v  4  ^
!   |     |   |     |
!    -->--     -->--
!
!   uuuu1:  uuuu += u1 u2 u3+ u4+    ! + = dagger
!   uuuu2:  uuuu += u1 u2+ u3+ u4
!   uuuu3:  uuuu += u1+ u2+ u3 u4
!   uuuu4:  uuuu += u1+ u2 u3 u4+
!
!-------------------------------------------------------------------------------
# include "defs.h"

!-------------------------------------------------------------------------------
subroutine clover_uuuu1(uuuu, u1, u2, u3, u4)

  implicit none
  SU3 :: uuuu, u1, u2, u3, u4
  integer :: i, j, k, l, m

  do i = 1, NCOL
   do m = 1, NCOL
    do j = 1, NCOL
     do k = 1, NCOL
      do l = 1, NCOL
       uuuu(i,m)= uuuu(i,m)+ u1(i,j) * u2(j,k) * conjg(u3(l,k)) * conjg(u4(m,l))
      enddo
     enddo
    enddo
   enddo
  enddo
end

!-------------------------------------------------------------------------------
subroutine clover_uuuu2(uuuu, u1, u2, u3, u4)

  implicit none
  SU3 :: uuuu, u1, u2, u3, u4
  integer :: i, j, k, l, m

  do i = 1, NCOL
   do m = 1, NCOL
    do j = 1, NCOL
     do k = 1, NCOL
      do l = 1, NCOL
       uuuu(i,m)= uuuu(i,m)+ u1(i,j) * conjg(u2(k,j)) * conjg(u3(l,k)) * u4(l,m)
      enddo
     enddo
    enddo
   enddo
  enddo
end



!-------------------------------------------------------------------------------
subroutine clover_uuuu3(uuuu, u1, u2, u3, u4)

  implicit none
  SU3 :: uuuu, u1, u2, u3, u4
  integer :: i, j, k, l, m

  do i = 1, NCOL
   do m = 1, NCOL
    do j = 1, NCOL
     do k = 1, NCOL
      do l = 1, NCOL
       uuuu(i,m)= uuuu(i,m)+ conjg(u1(j,i)) * conjg(u2(k,j)) * u3(k,l) * u4(l,m)
      enddo
     enddo
    enddo
   enddo
  enddo
end

!-------------------------------------------------------------------------------
subroutine clover_uuuu4(uuuu, u1, u2, u3, u4)

  implicit none
  SU3 :: uuuu, u1, u2, u3, u4
  integer :: i, j, k, l, m

  do i = 1, NCOL
   do m = 1, NCOL
    do j = 1, NCOL
     do k = 1, NCOL
      do l = 1, NCOL
       uuuu(i,m)= uuuu(i,m)+ conjg(u1(j,i)) * u2(j,k) * u3(k,l) * conjg(u4(m,l))
      enddo
     enddo
    enddo
   enddo
  enddo
end

!===============================================================================
