!===============================================================================
!
! BQCD -- Berlin Quantum ChromoDynamics program
!
! Author: Hinnerk Stueben <stueben@zib.de>
!
! Copyright (C) 1998-2003, Hinnerk Stueben, Zuse-Institut Berlin
!
!-------------------------------------------------------------------------------
!
! swap.F90 - swap routines for various data types
!
!-------------------------------------------------------------------------------
# include "defs.h"

!-------------------------------------------------------------------------------
subroutine swap_p_g_field(u, v)

  implicit none
  P_GAUGE_FIELD :: u, v, tmp

  tmp => u
  u => v
  v => tmp

end

!-------------------------------------------------------------------------------
subroutine swap_p_sc_field(a, b)

  implicit none
  P_SPINCOL_FIELD :: a, b, tmp

  tmp => a
  a => b
  b => tmp

end

!-------------------------------------------------------------------------------
subroutine swap_p_clover_field_a(x, y)

  use typedef_clover
  implicit none
  P_CLOVER_FIELD_A :: x, y, tmp

  tmp => x
  x => y
  y => tmp

end

!-------------------------------------------------------------------------------
subroutine swap_p_clover_field_b(x, y)

  use typedef_clover
  implicit none
  P_CLOVER_FIELD_B :: x, y, tmp

  tmp => x
  x => y
  y => tmp

end

!-------------------------------------------------------------------------------
subroutine swap_real(x, y)

  implicit none
  REAL :: x, y, tmp
  
  tmp = x
  x = y
  y = tmp

end

!-------------------------------------------------------------------------------
subroutine swap_integer(x, y)

  implicit none
  integer :: x, y, tmp
  
  tmp = x
  x = y
  y = tmp

end

!===============================================================================
