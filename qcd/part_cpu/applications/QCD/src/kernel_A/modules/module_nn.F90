!===============================================================================
!
! BQCD -- Berlin Quantum ChromoDynamics program
!
! Author: Hinnerk Stueben <stueben@zib.de>
!
! Copyright (C) 2003, Hinnerk Stueben, Zuse-Institut Berlin
!
!-------------------------------------------------------------------------------
!
! module_nn.F90  -  pointer to nearest neighbour list 
!                   nn(volh_tot, EVEN:ODD, DIM, FWD:BWD)
!
!-------------------------------------------------------------------------------
# include "defs.h"

!-------------------------------------------------------------------------------
module module_nn

  INTEGER, dimension(:, :, :, :), pointer, save :: nn 

end
!===============================================================================
