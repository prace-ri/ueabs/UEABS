!===============================================================================
!
! BQCD -- Berlin Quantum ChromoDynamics programme
!
! Author: Hinnerk Stueben <stueben@zib.de>
!
! Copyright (C) 2003-2006, Hinnerk Stueben, Zuse-Institut Berlin
!
!-------------------------------------------------------------------------------
!
! module_bqcd.F90
!
!-------------------------------------------------------------------------------
module module_bqcd

  character(len = *), parameter :: prog_name = "bqcd"
  character(len = *), parameter :: prog_version = "benchmark2"
  integer, parameter :: input_version = 4
  integer, parameter :: conf_info_version = 3

end
!===============================================================================
