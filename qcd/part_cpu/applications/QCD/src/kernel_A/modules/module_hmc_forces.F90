!===============================================================================
!
! BQCD -- Berlin Quantum ChromoDynamics programme
!
! Author: Hinnerk Stueben <stueben@zib.de>
!
! Copyright (C) 2006, Hinnerk Stueben, Zuse-Institut Berlin
!
!-------------------------------------------------------------------------------
!
! module_hmc_forces.F90
!
!-------------------------------------------------------------------------------
# include "defs.h"

!-------------------------------------------------------------------------------
module module_hmc_forces

  P_GENERATOR_FIELD, save :: p_old
  
  integer, parameter :: n_force = 4

  integer, parameter :: i_sg = 1
  integer, parameter :: i_sd = 2
  integer, parameter :: i_sf1 = 3
  integer, parameter :: i_sf2 = 4

  REAL, save :: f_count(n_force)
  REAL, save :: f_avg(n_force)
  REAL, save :: f_max(n_force)
end

!===============================================================================
