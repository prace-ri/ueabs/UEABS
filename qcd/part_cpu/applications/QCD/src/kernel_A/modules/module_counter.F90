!===============================================================================
!
! BQCD -- Berlin Quantum ChromoDynamics program
!
! Author: Hinnerk Stueben <stueben@zib.de>
!
! Copyright (C) 2003, Hinnerk Stueben, Zuse-Institut Berlin
!
!-------------------------------------------------------------------------------
!
! module_counter.F90
!
!-------------------------------------------------------------------------------
# include "defs.h"

!-------------------------------------------------------------------------------
module module_counter

  type type_counter
     sequence
     integer :: run
     integer :: job
     integer :: traj    ! overall trajectory counter
     integer :: j_traj  ! job trajectory counter
  end type type_counter

  type(type_counter), save :: counter

end
!===============================================================================
