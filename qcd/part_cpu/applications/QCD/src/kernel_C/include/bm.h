/*******************************************************************************
*
* File bm.h
*
* Copyright (C) 2008 Bjorn Leder
*
* This software is distributed under the terms of the GNU General Public
* License (GPL)
*
*******************************************************************************/

#ifndef BM_H
#define BM_H

#define NMR 4
#define NCY 5
#define NS  16

#ifndef CG_ITER_C
extern void time_cg_iter(FILE *flog, double *wdt);
#endif

#ifndef CG_ITER_DBLE_C
extern void time_cg_iter_dble(FILE *flog, double *wdt);
#endif

#ifndef TIME_MSAP_C
extern void time_msap(FILE *flog, double *wdt);
#endif

#ifndef TIME_AWHAT_C
extern void time_Awhat(FILE *flog, double *wdt, int* nb);
#endif

#endif
