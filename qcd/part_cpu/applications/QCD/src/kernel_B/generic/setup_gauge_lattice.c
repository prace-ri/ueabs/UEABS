/*****************************************************************
 * set up the gauge lattice 
 */

#include LATDEF

void hotlat();
void coldlat();
void load_node0();

void load_config(int lattice_mode)
{

  switch (lattice_mode) {
  COLD:   
    coldlat(); 
    break;
  HOT:    
    hotlat();  
    break;
  LOAD:   
    load_node0();
    break;
  }
}

/* Load the lattice through node 0 and communicate
 */

void load_node0()
{
  int i;
  double t;
  FILE * fil;


  /* fix the name of the lattice to be loaded */
  if (this_node == 0) {
    i = ((fil = fopen(start_lattice_name,"r")) != NULL);
    if (i) {
      printf(" - Reading the configuration\n");
    } else {
      char line[200];
      sprintf(line,"No configuration file '%s'",start_lattice_name);
      halt(line);
    }
  }
  /* broadcast_int(&stat);
   */

  t = cputime();
  restore_binary(fil);
  reunitarize_all(U);
  if(this_node==0){
    printf("Time for loading lattice = %le seconds\n",cputime()-t);
    fclose(fil);
  }
}


/**********************************************************/


void coldlat()  
{
  /* sets link matrices to unit matrices */
  register int i,j,k,dir;

  forallsites(i){
    foralldir(dir) {
      for(j=0; j<Ncol; j++)  {
	for(k=0; k<Ncol; k++)  {
	  if (j != k) U[dir][i].e[j][k] = cmplx(0.0,0.0);
	  else U[dir][i].e[j][k] = cmplx(1.0,0.0);
	}
      }
    }
  }
  if(this_node==0)printf(" -- COLD lattice loaded\n");
}


/**********************************************************/


void hotlat()  
{
  /* sets link matrices to random su3 */
  register int i,dir;

  forallsites(i){
    foralldir(dir) random_su3P( &(U[dir][i]), 16 ); /* 16 hits only */
  }
  if(this_node==0)printf(" -- HOT lattice loaded\n");
}

