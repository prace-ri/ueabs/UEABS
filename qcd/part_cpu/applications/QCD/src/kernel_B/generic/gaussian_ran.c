/*****************  gaussian_ran.c *************************************
 *
 *  double gaussian_ran()
 *  Gaussian distributed random number
 *  Probability distribution exp( -x*x ), so < x^2 > = 1/2
 *  This requires a random number generator named "dran()", returning
 *  a float uniformly distributed between zero and one.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "mersenne.h"
#define dran() mersenne()

double gaussian_ran()
{
  static int iset=0;
  static double gset;
  register double fac,r,v1,v2;
  
  if  (iset) {
    iset = 0;
    return(gset);
  }

  do {
    v1 = 2.0*dran() - 1.0;
    v2 = 2.0*dran() - 1.0;
    r  = v1*v1 + v2*v2;
  } while (r >= 1.0);
  fac  = sqrt( -log(r)/r );
  gset = v1*fac;
  iset = 1;
  return(v2*fac);
}
