/******************************************************************
 * here some MPI typical "bulk update" subroutines
 */

#include "comdefs.h"
#include "generic.h"

/* Is the link inside node?  Thus, OK if the link is not _along_ any
 * of the bottom slabs of the node.  For example, reject 
 * x-links where y,z,.. coordinate == min on the node
 */

int inside_node(int i, int dir)
{
  register int d,s;

  s = 1;
  foralldir( d ) if ( d != dir && coordinate(i,d) == node.xmin[d] ) s = 0;
  return( s );
}

