/* smooth_field_su3adjoint -- does smearing on su3 adjoint scalar field
 * Kari Rummukainen 2002
 */

#include LATDEF

#define FIELD adjoint_matrix
#define smooth_FIELD smooth_field_su3adjoint

#define mult_MATRIX_FIELD(a,b,c) mult_su3_ahiggs( &(a), &(b), &(c) )
#define mult_adj_MATRIX_FIELD(a,b,c) mult_adj_su3_ahiggs( &(a), &(b), &(c) )
#define add_FIELD(a,b,c) add_adjmat( &(a), &(b), &(c) )
#define scalar_mult_FIELD(a,b,c) adj_scalar_mul( &(a), b, &(c) )

#include "smooth_field_generic.c"
