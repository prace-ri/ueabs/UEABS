/*********************** reunitarize.c ***************************/
/* MIMD version 3 */

/* reunitarize the link matrices */
#include LATDEF

/* canopy qcdlib code - stolen, of course */
#define fixsu3(matrix) \
{ \
    bj0r = (*matrix).e[0][0].real; \
    bj0i = (*matrix).e[0][0].imag; \
    bj1r = (*matrix).e[0][1].real; \
    bj1i = (*matrix).e[0][1].imag; \
    bj2r = (*matrix).e[0][2].real; \
    bj2i = (*matrix).e[0][2].imag; \
    ar = (*matrix).e[1][2].real; \
    ai = (*matrix).e[1][2].imag; \
    tr = bj1r*ar - bj1i*ai; \
    ti = bj1r*ai + bj1i*ar; \
    ar = (*matrix).e[1][1].real; \
    ai = (*matrix).e[1][1].imag; \
    tr = tr - bj2r*ar + bj2i*ai; \
    ti = ti - bj2r*ai - bj2i*ar; \
    (*matrix).e[2][0].real = tr; \
    (*matrix).e[2][0].imag = -ti; \
    ar = (*matrix).e[1][0].real; \
    ai = (*matrix).e[1][0].imag; \
    tr = bj2r*ar - bj2i*ai; \
    ti = bj2r*ai + bj2i*ar; \
    ar = (*matrix).e[1][2].real; \
    ai = (*matrix).e[1][2].imag; \
    tr = tr - bj0r*ar + bj0i*ai; \
    ti = ti - bj0r*ai - bj0i*ar; \
    (*matrix).e[2][1].real = tr; \
    (*matrix).e[2][1].imag = -ti; \
    ar = (*matrix).e[1][1].real; \
    ai = (*matrix).e[1][1].imag; \
    tr = bj0r*ar - bj0i*ai; \
    ti = bj0r*ai + bj0i*ar; \
    ar = (*matrix).e[1][0].real; \
    ai = (*matrix).e[1][0].imag; \
    tr = tr - bj1r*ar + bj1i*ai; \
    ti = ti - bj1r*ai - bj1i*ar; \
    (*matrix).e[2][2].real = tr; \
    (*matrix).e[2][2].imag = -ti; \
 }

/* #pragma inline ( reunit_su3 )  */

void reunit_su3(su3_matrix *c) 
{
  register float bj0r, bj0i, bj1r, bj1i, bj2r, bj2i;
  register float ar, ai, tr, ti;

  /* first normalize row 0 */
  ar = (*c).e[0][0].real * (*c).e[0][0].real +    /* sum of squares of row */
    (*c).e[0][0].imag * (*c).e[0][0].imag +
    (*c).e[0][1].real * (*c).e[0][1].real +
    (*c).e[0][1].imag * (*c).e[0][1].imag +
    (*c).e[0][2].real * (*c).e[0][2].real +
    (*c).e[0][2].imag * (*c).e[0][2].imag;
  
  ar = 1.0 / sqrt( (double)ar);	       /* used to normalize row */
  (*c).e[0][0].real *= ar;
  (*c).e[0][0].imag *= ar;
  (*c).e[0][1].real *= ar;
  (*c).e[0][1].imag *= ar;
  (*c).e[0][2].real *= ar;
  (*c).e[0][2].imag *= ar;

  /* now make row 1 orthogonal to row 0 */
  ar = (*c).e[0][0].real * (*c).e[1][0].real +     /* real part of 0 dot 1 */
    (*c).e[0][0].imag * (*c).e[1][0].imag +
    (*c).e[0][1].real * (*c).e[1][1].real +
    (*c).e[0][1].imag * (*c).e[1][1].imag +
    (*c).e[0][2].real * (*c).e[1][2].real +
    (*c).e[0][2].imag * (*c).e[1][2].imag;
  ai = (*c).e[0][0].real * (*c).e[1][0].imag -     /* imag part of 0 dot 1 */
    (*c).e[0][0].imag * (*c).e[1][0].real +
    (*c).e[0][1].real * (*c).e[1][1].imag -
    (*c).e[0][1].imag * (*c).e[1][1].real +
    (*c).e[0][2].real * (*c).e[1][2].imag -
    (*c).e[0][2].imag * (*c).e[1][2].real;

					        /* row 2 -= a * row1 */
  (*c).e[1][0].real -= ar*(*c).e[0][0].real - ai*(*c).e[0][0].imag;
  (*c).e[1][0].imag -= ar*(*c).e[0][0].imag + ai*(*c).e[0][0].real;
  (*c).e[1][1].real -= ar*(*c).e[0][1].real - ai*(*c).e[0][1].imag;
  (*c).e[1][1].imag -= ar*(*c).e[0][1].imag + ai*(*c).e[0][1].real;
  (*c).e[1][2].real -= ar*(*c).e[0][2].real - ai*(*c).e[0][2].imag;
  (*c).e[1][2].imag -= ar*(*c).e[0][2].imag + ai*(*c).e[0][2].real;
     
  /* now normalize row 1 */
  ar = (*c).e[1][0].real * (*c).e[1][0].real +    /* sum of squares of row */
    (*c).e[1][0].imag * (*c).e[1][0].imag +
    (*c).e[1][1].real * (*c).e[1][1].real +
    (*c).e[1][1].imag * (*c).e[1][1].imag +
    (*c).e[1][2].real * (*c).e[1][2].real +
    (*c).e[1][2].imag * (*c).e[1][2].imag;
  
  ar = 1.0 / sqrt( (double)ar);	       /* used to normalize row */
  (*c).e[1][0].real *= ar;
  (*c).e[1][0].imag *= ar;
  (*c).e[1][1].real *= ar;
  (*c).e[1][1].imag *= ar;
  (*c).e[1][2].real *= ar;
  (*c).e[1][2].imag *= ar;

  fixsu3(c); /* reconstruct row 2 */

} /* reunit_su3 */


void reunitarize(su3_matrix *link[NDIM]) {
  int i,dir;

  foralldir(dir) forallsites(i) reunit_su3( &link[dir][i] );

}  /*reunitarize() */
