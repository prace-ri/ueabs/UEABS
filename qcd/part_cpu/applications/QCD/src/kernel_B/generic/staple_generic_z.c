/****** generic_staples.c  -- compute the staple ******************/

void staple_MATRIX(MATRIX *link[NDIM], MATRIX *staple_p, int site, int dir1)
{
  register int i,dir2,odir,start;
  MATRIX tmat1,tmat2,staple;

  /* Loop over other directions, computing force from plaquettes in
     the dir1,dir2 plane */
  
  start = 1;
  foralldir(dir2) if (dir2 != dir1) {
  
    odir = opp_dir(dir2);

    /* multiply  link[dir2]^* link[dir1] link[dir2] at direction -dir2 */
    i = nb(odir,site);
    mult_MATRIX_an( link[dir2][i], link[dir1][i], tmat1 );
    if (start) {
      mult_MATRIX_nn( tmat1, link[dir2][nb(dir1,i)], staple );
    } else {
      mult_MATRIX_nn( tmat1, link[dir2][nb(dir1,i)], tmat2 );
      add_MATRIX( staple, tmat2, staple );
    }
    start = 0;

    /* and now the upper staple */
    mult_MATRIX_nn( link[dir2][site], link[dir1][nb(dir2,site)], tmat1 );
    mult_MATRIX_na( tmat1, link[dir2][nb(dir1,site)], tmat2 );
    add_MATRIX( staple, tmat2, staple );
  }
  /* and return the staple */
  *staple_p = staple;
}

