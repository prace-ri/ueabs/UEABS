
#ifdef TIMERS

typedef struct {
  double total, start, initial; /* cumulated, work, and initial timeval */
  int count;
} timer_type;


double timer_start( timer_type * );
double timer_end( timer_type * );
void timer_reset( timer_type * t );
void timer_report( timer_type * );

#else

#define timer_start(a)
#define timer_end(a)
#define timer_reset(a)
#define timer_report(a)

#endif
