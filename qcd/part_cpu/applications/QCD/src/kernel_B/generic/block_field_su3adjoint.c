/* block_su3_adjoint -- does blocking on su3 adjoint scalar field
 * Kari Rummukainen 2002
 */

#include LATDEF

#define FIELD adjoint_matrix
#define block_FIELD block_field_su3adjoint

#include "block_field_generic.c"
