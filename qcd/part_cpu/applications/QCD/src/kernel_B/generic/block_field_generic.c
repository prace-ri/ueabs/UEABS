/*********************************************************
 * Generic field blocking routine for SU(2)
 * Kari Rummukainen 1998-2002
 */

FIELD * block_FIELD(FIELD *f, int newlev[NDIM], int free_old)
{
  /*   Block the field(s)
   */
  int dir,i,j,off,x[NDIM],step[NDIM],oldlev[NDIM];
  node_struct oldnode;
  FIELD *th;

  /* first, catch the current node */
  oldnode = node;

  /* calculate the relative change */
  foralldir(dir) step[dir] = newlev[dir] - 
    (oldlev[dir] = current_blocking_level[dir]);

  /* set new block */
  set_blocking_level( newlev );

  th = new_latfield( FIELD );

  /* and loop over */
  off = 0;
  forallsites(i) {
    foralldir(dir) x[dir] = (coordinate(i,dir) << step[dir]);

    /* index to the corresponding site */
    th[i] = f[ node_index( x, &oldnode ) ];
    
  }

  set_blocking_level( oldlev );

  if (free_old) free_latfield( f );

  return( th );
}

