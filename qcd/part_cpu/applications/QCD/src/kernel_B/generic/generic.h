/**************************************************************************
 * Header file where to define headers for generic functions
 * (not for communications, which are defined in comdefs.h)
 */

/********************  Mersenne random numbers ***/

#include "mersenne.h"

/**
#define MERSENNE_N 624

extern int mersenne_i;
extern double mersenne_array[MERSENNE_N];

#define mersenne() ( mersenne_i > 0 ? mersenne_array[--mersenne_i] : \
                     mersenne_generate(&mersenne_i) )

void seed_mersenne(long);
double mersenne_generate(int *);

**/

#define dran() mersenne()


/********************* General protos ***/

void initial_setup();
void initialize_prn(long seed);
void *halt(char *);
double gaussian_ran();
void restore_binary(FILE * f);
void save_binary(FILE * f);

/*********************** Some defines ****/

#define smaller(a,b) ((a)<(b)? (a) : (b))
#define greater(a,b) ((a)>(b)? (a) : (b))
#define sqr(x) ((x)*(x))
#define printf0 if (this_node != 0) { } else printf

/********************** Parameter_io.c ***/

double get_d(FILE *f,char *s,int bcast);
int get_i(FILE *f,char *s,int bcast);
int get_s(FILE *f,char *s,char *target,int bcast);
int get_item(FILE *f,char *s,char *items[],int n_items, int bcast);
void print_d(FILE *f,char *s,double val);
void print_i(FILE *f,char *s,int val);
void print_s(FILE *f,char *s,char *val);

/************************* TIMING STUFF **/

double added_cpu_time();
double cputime();
void timecheck(int iter, int maxiter, int status);
void inittimecheck(void);
int setup_timelimit(time_t t,int argc,char *argv);
void resettime(void);
void inittime(void);

#define addtime(t) t += added_cpu_time()

/************************ Multicanonical headers **/

int setmulti();
double multi_weight();
int mc_acceptance(int parity,double rt);
void set_mc_update(int parity);
void writemuca();

EXTERN int is_multicanonical, is_mucacalc;

/**********************/

