/* smooth_field_su2adjoint -- does smearing on su3 adjoint scalar field
 * Kari Rummukainen 2002
 */

#include LATDEF

#define FIELD adjoint
#define smooth_FIELD smooth_field_su2adjoint

#define mult_MATRIX_FIELD(a,b,c) mult_su2_nadj( a, b, c )
#define mult_adj_MATRIX_FIELD(a,b,c) mult_su2_aadj( a, b, c )
#define add_FIELD(a,b,c) add_adjoint( a, b, c )
#define scalar_mult_FIELD(a,b,c) adj_scalar_mul( a, b, c )

#include "smooth_field_generic.c"
