/*  Definitions which convert generic MATRIX definitions to su3
 */
#define MATRIX complex
#define mult_MATRIX_nn(a,b,c) c_mul_nn( a, b, c )
#define mult_MATRIX_na(a,b,c) c_mul_ni( a, b, c )
#define mult_MATRIX_an(a,b,c) c_mul_in( a, b, c )
#define mult_MATRIX_aa(a,b,c) c_mul_ii( a, b, c )
#define add_MATRIX(a,b,c)     c_add( a, b, c )
#define scalar_mul_MATRIX(a,s,b) c_scalar_mul( a, s, b )

void smooth_field_complex(complex *link[NDIM], complex *cphi, int d[NDIM],
			  double c_mul_0, double c_mul_1);

complex *block_field_complex(complex *f, int newlev[NDIM], int free_old);

void block_link_complex( complex *oldl[NDIM], complex *newl[NDIM],
			 int newlev[NDIM], int free_old );

void smooth_link_complex( complex *link[NDIM],  int d1[NDIM], int d2[NDIM],
			  double c_mul_0, double c_mul_1);

