/******** setup_basic.c *********/
/* MIMD code version 3 */

/* Here are basic setup routines, which do not depend on the
 * program
 */

#include LATDEF

/* SETUP ROUTINES */


void initial_setup()
{

  /* First, adjust malloc so that glibc free() does not 
   * release space to the system
   */

#ifdef __GNUC__
#include "malloc.h"
  mallopt( M_MMAP_MAX, 0 );  /* don't use mmap */
  /* HACK: don't release memory by calling sbrk */
  mallopt( M_TRIM_THRESHOLD, -1 );
#endif

  /* Machine initialization first */
  initialize_machine();
  g_sync();

  /* set the timing up */
  inittime();    

  /* basic static node variables */
  this_node = mynode();
  number_of_nodes = numnodes();

#ifdef __GNUC__
  printf0(" GNU c-library performance:\n using sbrk instead of mmap; not returning memory\n");
#endif

}


/**************************************************
 * random number generators
 */

void initialize_prn(long seed)
{
  int node;
  
  node = mynode();

  if (seed == 0) {
    if (this_node == 0) {
      seed = time(NULL);
      seed = seed^(seed<<26)^(seed<<9);
      printf(" + Random seed from time %ld\n",seed);
    }
    broadcast_field(&seed,sizeof(long));
  }
  seed += 1121*node;
  seed = seed ^ ((532*node)<<18);

  seed_mersenne(seed);

}

