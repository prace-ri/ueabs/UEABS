/* smooth_field_complex -- does smearing on complex scalar field
 * Kari Rummukainen 2002
 */

#include LATDEF

#define FIELD complex
#define smooth_FIELD smooth_field_complex

#define mult_MATRIX_FIELD(a,b,c) c_mul_nn( a, b, c )
#define mult_adj_MATRIX_FIELD(a,b,c) c_mul_in( a, b, c )
#define add_FIELD(a,b,c) c_add( a, b, c )
#define scalar_mult_FIELD(a,b,c) c_scalar_mul( a, b, c )

#include "smooth_field_generic.c"
