/****** smooth_link_complex.c  -- compute the blocked link ******************/

/* MIMD version 3 */

#include LATDEF

#define smooth_link_MATRIX smooth_link_complex

#define zero_MATRIX(a) c_zero( a )

#include "smooth_link_generic.c"
