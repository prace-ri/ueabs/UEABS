/****** staples_su3.c  -- compute the staple ******************/

/* MIMD version 3 */

#include LATDEF
#include "generic_su2.h"

#define staples_MATRIX staples_su2
#include "staples_generic.c"
