/****** block_link_su2.c  -- compute the blocked link ******************/

/* MIMD version 3 */

#include LATDEF
#include "generic_su2.h"

#define block_link_MATRIX block_link_su2
#include "block_link_generic.c"
