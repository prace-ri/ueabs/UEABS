/*  Definitions which convert generic MATRIX definitions to su3
 */

#define MATRIX su2_matrix

#define mult_MATRIX_nn(a,b,c) mult_su2_nn( (a), (b), (c) )
#define mult_MATRIX_na(a,b,c) mult_su2_na( (a), (b), (c) )
#define mult_MATRIX_an(a,b,c) mult_su2_an( (a), (b), (c) )
#define mult_MATRIX_aa(a,b,c) mult_su2_aa( (a), (b), (c) )
#define add_MATRIX(a,b,c)     add_su2_matrix( (a), (b), (c) )
#define scalar_mul_MATRIX(a,s,b) su2_scalar_mul( a, s, b )

#define prefetch_MATRIX( a )  prefetch_matrix( a )

void smooth_field_su2adjoint(su2_matrix *link[NDIM], adjoint *cphi, int d[NDIM],
			     double c_mul_0, double c_mul_1);

adjoint *block_field_su2adjoint(adjoint *f, int newlev[NDIM], int free_old);

void block_link_su2( su2_matrix  *oldl[NDIM], su2_matrix *newl[NDIM],
		     int newlev[NDIM], int free_old );

void smooth_link_su2( su2_matrix *link[NDIM],  int d1[NDIM], int d2[NDIM],
		      double c_mul_0, double c_mul_1);

