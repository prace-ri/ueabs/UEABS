/****** smooth_link_su3.c  -- compute the blocked link ******************/

/* MIMD version 3 */

#include LATDEF

#define smooth_link_MATRIX smooth_link_su3

#define zero_MATRIX(a) scalar_mult_su3_matrix( &(a), 0.0, &(a) )

#include "smooth_link_generic.c"
