#ifdef TIMERS

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include "comdefs.h"
#include "timers.h"

double timer_start( timer_type * t )
{
  struct timeval resource;
  
  if (this_node == 0) {
    gettimeofday(&resource,NULL);
    t->start =    resource.tv_sec + 1.0e-6*resource.tv_usec;
  
    /* t->start = clock()*1.0/(CLOCKS_PER_SEC); */
    return(t->start);
  } else return(0.0);
}

double timer_end( timer_type * t )
{
  double e;
  struct timeval resource;

  if (this_node == 0) {
    gettimeofday(&resource,NULL);
    e = resource.tv_sec + 1.0e-6*resource.tv_usec;
    /* e = clock()*1.0/(CLOCKS_PER_SEC); */

    t->total += (e - t->start);
    t->count++;
    return(e);
  } else return(0.0);
}

void timer_report( timer_type * t )
{
  struct timeval resource;

  if (this_node == 0) {
    gettimeofday(&resource,NULL);
    /* time used during the counter activity */
    t->initial = resource.tv_sec + 1.0e-6*resource.tv_usec - t->initial;
    if (t->count) 
      printf("  total %g sec, %d calls, %g usec/call, fraction %.2g of time\n",
	     t->total, t->count, 1e6 * t->total/t->count, t->total/t->initial );
    else
      printf("  no timed calls made\n");
  }
}

void timer_reset( timer_type * t ) {
  struct timeval resource;

  t->total = t->count = t->start = 0;
  if (this_node == 0) {
    gettimeofday(&resource,NULL);
    t->initial = resource.tv_sec + 1.0e-6*resource.tv_usec;
  }
}

#endif  /* timers */
