/****** smooth_link_su2.c  -- compute the blocked link ******************/

/* MIMD version 3 */

#include LATDEF

#define smooth_link_MATRIX smooth_link_su2

#define zero_MATRIX(a) su2_zero( a )

#include "smooth_link_generic.c"
