/*  Definitions which convert generic MATRIX definitions to su3
 */


#define MATRIX su3_matrix

#define mult_MATRIX_nn(a,b,c) mult_su3_nn( &(a), &(b), &(c) )
#define mult_MATRIX_na(a,b,c) mult_su3_na( &(a), &(b), &(c) )
#define mult_MATRIX_an(a,b,c) mult_su3_an( &(a), &(b), &(c) )
#define mult_MATRIX_aa(a,b,c) mult_su3_aa( &(a), &(b), &(c) )
#define add_MATRIX(a,b,c)     add_su3_matrix( &(a), &(b), &(c) )
#define scalar_mul_MATRIX(a,s,b) scalar_mult_su3_matrix( &(a), s, &(b) )

#define prefetch_MATRIX( a )  prefetch_matrix( a )

void smooth_field_su3adjoint(su3_matrix *link[NDIM], adjoint_matrix *cphi, int d[NDIM],
			     double c_mul_0, double c_mul_1);

adjoint_matrix *block_field_su3adjoint(adjoint_matrix *f, int newlev[NDIM], int free_old);

void block_link_su3( su3_matrix  *oldl[NDIM], su3_matrix *newl[NDIM],
		     int newlev[NDIM], int free_old );

void smooth_link_su3( su3_matrix *link[NDIM],  int d1[NDIM], int d2[NDIM],
		      double c_mul_0, double c_mul_1);

void reunitarize( su3_matrix *U[NDIM]);

#ifdef SSE_INLINE
#include "../sse/inline_sse.h"
#endif
