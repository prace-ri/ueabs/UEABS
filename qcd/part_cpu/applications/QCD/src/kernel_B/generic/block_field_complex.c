/* block_field_complex -- does blocking on u1 higgs scalar field
 * Kari Rummukainen 2002
 */

#include LATDEF

#define FIELD complex
#define block_FIELD block_field_complex

#include "block_field_generic.c"
