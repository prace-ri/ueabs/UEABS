/************************ generic_clover.h ******************************
*									*
*  Macros and declarations for generic_clover routines                  *
*  This header is for codes that call generic_clover routines           *
*  MIMD version 5 							*
*									*
*/


int clover_invert(      /* Return value is number of iterations taken */
    field_offset src,   /* type wilson_vector (source already created)*/
    field_offset dest,  /* type wilson_vector (answer and initial guess) */
    field_offset tmp,   /* type wilson_vector (workspace used only for bi-cg)*/
    field_offset sav,   /* type wilson_vector (for saving source) */
    int MinCG,          /* minimum number of iterations per restart */
    int MaxCG,          /* maximum number of iterations per restart */
    int nrestart,       /* maximum restarts */
    radix RsdCG,        /* desired residual - 
			   normalized as sqrt(r*r)/sqrt(src_e*src_e */
    radix *size_r,      /* resulting residual */
    int start_flag,     /* 0: use a zero initial guess; 1: use dest */
    radix Kappa,        /* hopping */
    radix Clov_c,       /* Perturbative clover coeff */
    radix U0,           /* Tadpole correction to Clov_c */
    field_offset f_mn   /* size of su3_matrix (workspace) */
    );

int clover_invert_lean( /* Return value is number of iterations taken */
    field_offset src,   /* type wilson_vector (where source is to be created)*/
    field_offset dest,  /* type wilson_vector (answer and initial guess) */
    field_offset tmp,   /* type wilson_vector (workspace used only for bi-cg)*/
    void (*source_func)(field_offset src, 
			wilson_quark_source *wqs),  /* source function */
    wilson_quark_source *wqs, /* source parameters */
    int MinCG,          /* minimum number of iterations per restart */
    int MaxCG,          /* maximum number of iterations per restart */
    int nrestart,       /* maximum restarts */
    radix RsdCG,        /* desired residual - 
			   normalized as sqrt(r*r)/sqrt(src_e*src_e */
    radix *size_r,      /* resulting residual */
    int start_flag,     /* 0: use a zero initial guess; 1: use dest */
    radix Kappa,        /* hopping */
    radix Clov_c,       /* Perturbative clover coeff */
    radix U0,           /* Tadpole correction to Clov_c */
    field_offset f_mn   /* size of su3_matrix (workspace) */
    );

int cgilu_cl(           /* Return value is number of iterations taken */
    field_offset src,   /* type wilson_vector (source vector - OVERWRITTEN!)*/
    field_offset dest,  /* type wilson_vector (answer and initial guess )*/
    int MinCG,          /* minimum number of iterations per restart */
    int MaxCG,          /* maximum number of iterations */
    radix RsdCG,        /* desired residual - 
			   normalized as sqrt(r*r)/sqrt(src_e*src_e */
    radix *size_r,      /* resulting residual */
    int flag,           /* 0: use a zero initial guess; 1: use dest */
    radix Kappa,        /* hopping */
    radix Clov_c,       /* Perturbative clover coeff */
    radix U0,           /* Tadpole correction to Clov_c */
    field_offset f_mn   /* Scratch space of size su3_matrix */
    );
int bicgilu_cl(           /* Return value is number of iterations taken */
    field_offset src,   /* type wilson_vector (source vector - OVERWRITTEN!)*/
    field_offset dest,  /* type wilson_vector (answer and initial guess )*/
    int MinCG,          /* minimum number of iterations per restart */
    int MaxCG,          /* maximum number of iterations */
    radix RsdCG,        /* desired residual - 
			   normalized as sqrt(r*r)/sqrt(src_e*src_e */
    radix *size_r,      /* resulting residual */
    int flag,           /* 0: use a zero initial guess; 1: use dest */
    radix Kappa,        /* hopping */
    field_offset rv,    /* Scratch space of size wilson_vector */
    radix Clov_c,       /* Perturbative clover coeff */
    radix U0,           /* Tadpole correction to Clov_c */
    field_offset f_mn   /* Scratch space of size su3_matrix */
    );
void f_mu_nu(field_offset f_mn,int mu,int nu);
void make_clov(radix Clov_c,field_offset f_mn);
void make_clovinv();

void mult_ldu(
  field_offset src,   /* type wilson_vector RECAST AS wilson_block_vector */
  field_offset dest,  /* type wilson_vector RECAST AS wilson_block_vector */
  field_offset triang,/* type triangular */
  field_offset diag,  /* type diagonal */
  int parity );

