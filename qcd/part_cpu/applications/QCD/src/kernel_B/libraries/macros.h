/* macros for "field offset" and "field pointer", used when fields
  are arguments to subroutines */
/* Usage:  fo = F_OFFSET( field ), where "field" is the name of a field
  in lattice.
     address = F_PT( &site , fo ), where &site is the address of the
  site and fo is a field_offset.  Usually, the result will have to be
  cast to a pointer to the appropriate type. (It is naturally a char *).
*/
typedef int field_offset;
#define F_OFFSET(a) \
  ((field_offset)(((char *)&(lattice[0]. a ))-((char *)&(lattice[0])) ))
#define F_PT( site , fo )  ((char *)( site ) + (fo)) 

/* macros to loop over sites of a given parity.
   Usage:  
	int i;
	site *s;
	FOREVENSITES(i,s){
	    commands, where s is a pointer to the current site and i is
	    the index of the site on the node
	}
*/
#ifdef EVENFIRST
#define FOREVENSITES(i,s) \
    for(i=0,s=lattice;i<even_sites_on_node;i++,s++)
#define FORODDSITES(i,s) \
    for(i=even_sites_on_node,s= &(lattice[i]);i<sites_on_node;i++,s++)
#define FORSOMEPARITY(i,s,choice) \
    for( i=((choice)==ODD ? even_sites_on_node : 0 ),  \
    s= &(lattice[i]); \
    i< ( (choice)==EVEN ? even_sites_on_node : sites_on_node); \
    i++,s++)
#else
#define FOREVENSITES(i,s) \
    for(i=0,s=lattice;i<sites_on_node;i++,s++)if(s->parity==EVEN)
#define FORODDSITES(i,s) \
    for(i=0,s=lattice;i<sites_on_node;i++,s++)if(s->parity==ODD)
#define FORSOMEPARITY(i,s,choice) \
    for(i=0,s=lattice;i<sites_on_node;i++,s++)if( (s->parity & (choice)) != 0)
#endif	/* end ifdef EVENFIRST */
#define FORALLSITES(i,s) \
    for(i=0,s=lattice;i<sites_on_node;i++,s++)


/* printf on node zero only */
#define node0_printf if(this_node==0)printf

#define ON 1
#define OFF 0
