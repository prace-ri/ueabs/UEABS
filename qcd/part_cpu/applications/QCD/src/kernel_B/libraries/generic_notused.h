/************************ generic.h *************************************
*									*
*  Macros and declarations for miscellaneous generic routines           *
*  This header is for codes that call generic routines                  *
*  MIMD version 5 							*
*									*
*/

/* Other generic directory declarations are elsewhere:

   For com_*.c, see comdefs.h
   For io_ansi.c, io_nonansi.c, io_piofs.c, io_paragon.c see io_lat.h
   For io_wb.c, see io_wb.h
*/

/* bsd sum */
#ifndef _type32
#define _type32
#ifdef SHORT32
typedef unsigned short type32;
#else
typedef unsigned int type32;
#endif
#endif
type32 bsd_sum (char *data,type32 total_bytes);

/* check_unitarity.c */
void check_unitarity( void );

/* Routines in layout_*.c */
void setup_layout( void );
int node_number(int x,int y,int z,int t);
int node_index(int x,int y,int z,int t);
int num_sites(int node);

/* ploop?.c */
complex ploop( void );

/* d_plaq?.c */
void d_plaquette(double *ss_plaq,double *st_plaq);

/* plaquette_generic.c */
void plaquette_generic(radix *ss_plaq,radix *st_plaq,field_offset su3_mat);

/* plaquette4.c */
void plaquette(radix *ss_plaq,radix *st_plaq);

/* ploop_staple.c */
complex ploop_staple(radix alpha_fuzz);

/* ranstuff.c */
void initialize_prn(double_prn *prn_pt, int seed, int index);
radix myrand(double_prn *prn_pt);

/* ranmom.c */
void ranmom();

/* restrict_fourier.c */
void setup_restrict_fourier( int *key, int *restrict);
void restrict_fourier( 
     field_offset src,	 /* src is field to be transformed */
     field_offset space, /* space is working space, same size as src */
     field_offset space2,/* space2 is working space, same size as src */
                         /* space2 is needed only for non power of 2 */
     int size,		 /* Size of field in bytes.  The field must
			    consist of size/sizeof(complex) consecutive
			    complex numbers.  For example, an su3_vector
			    is 3 complex numbers. */
     int isign);	 /* 1 for x -> k, -1 for k -> x */

/* gaugefix.c */
void gaugefix(int gauge_dir,radix relax_boost,int max_gauge_iter,
	      radix gauge_fix_tol, field_offset diffmat, field_offset sumvec,
	      int nvector, field_offset vector_offset[], int vector_parity[],
	      int nantiherm, field_offset antiherm_offset[], 
	      int antiherm_parity[] );

/* reunitarize.c */
void reunitarize( void );

