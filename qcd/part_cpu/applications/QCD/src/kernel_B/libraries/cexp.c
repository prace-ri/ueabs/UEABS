/* Subroutines for operations on complex numbers */
/* complex exponential */
#include <math.h>
#include "complex.h"

complex cexp( complex *a ){
    complex c;
    radix mag;
    mag = (radix)exp( (double)(*a).real );
    c.real = mag*(radix)cos( (double)(*a).imag );
    c.imag = mag*(radix)sin( (double)(*a).imag );
    return(c);
}
