/* Subroutines for operations on complex numbers */
/* complex logarithm */
#include <math.h>
#include "complex.h"

complex clog( complex *a ){
    complex c;
    c.real = 0.5*(radix)log((double)((*a).real*(*a).real+(*a).imag*(*a).imag));
    c.imag = (radix)atan2( (double)(*a).imag, (double)(*a).real );
    return(c);
}
