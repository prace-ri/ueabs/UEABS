/*****************  wp_shrink8.c  (in su3.a) ****************************
*									*
* Shrink a wilson vector in eight directions, producing eight		*
*  half_wilson_vectors.							*
* void wp_shrink_8dir(a,b,sign)						*
* wilson_vector *a; half_wilson_vector *b; 				*
* int sign;								*
* B1 <- (1 +- gamma_x)A,, projection					*
*  argument "sign" is sign of gamma matrix.				*
*  See wp_shrink.c for definitions of gamma matrices and eigenvectors.	*
*/
#include "complex.h"
#include "su3.h"
/* Directions, and a macro to give the opposite direction */
/*  These must go from 0 to 7 because they will be used to index an
    array. */
/* Also define NDIRS = number of directions */
#define XUP 0
#define YUP 1
#define ZUP 2
#define TUP 3
#define TDOWN 4
#define ZDOWN 5
#define YDOWN 6
#define XDOWN 7

#define OPP_DIR(dir)	(7-(dir))	/* Opposite direction */
#define NDIRS 8				/* number of directions */

void wp_shrink_8dir( wilson_vector *a, half_wilson_vector *b, int sign) {
    wp_shrink( a,&(b[XUP]),XUP,sign);
    wp_shrink( a,&(b[YUP]),YUP,sign);
    wp_shrink( a,&(b[ZUP]),ZUP,sign);
    wp_shrink( a,&(b[TUP]),TUP,sign);
    wp_shrink( a,&(b[XDOWN]),XDOWN,sign);
    wp_shrink( a,&(b[YDOWN]),YDOWN,sign);
    wp_shrink( a,&(b[ZDOWN]),ZDOWN,sign);
    wp_shrink( a,&(b[TDOWN]),TDOWN,sign);
}
