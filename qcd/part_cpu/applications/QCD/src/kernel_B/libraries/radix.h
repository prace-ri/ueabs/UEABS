#ifndef RADIX
#define RADIX
/* this file just defines radix */

#define RADIX_F         /* define symbol so that know radix is in use
			 * and is float 
			 * another option: RADIX_D
			 */

typedef float radix;    /* basic type */

#endif
