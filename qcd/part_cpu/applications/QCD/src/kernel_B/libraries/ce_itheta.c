/* Subroutines for operations on complex numbers */
/* exp( i*theta ) */
#include <math.h>
#include "complex.h"

complex ce_itheta( radix theta ){
    complex c;
    c.real = (radix)cos( (double)theta );
    c.imag = (radix)sin( (double)theta );
    /* there must be a more efficient way */
    return( c );
}
