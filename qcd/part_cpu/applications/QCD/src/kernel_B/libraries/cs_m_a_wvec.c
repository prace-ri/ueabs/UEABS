/********************  	cs_m_a_wvec.c  (in su3.a) ********************
*
*void c_scalar_mult_add_wvec(wilson_vector *src1, wilson_vector *src2,
	complex *s, wilson_vector *dest)
*  Multiply a Wilson vector by a complex scalar and add to another vector
* dest  <-  src1 + s*src2
*/
#include "complex.h"
#include "su3.h"

void c_scalar_mult_add_wvec(wilson_vector *src1,wilson_vector *src2,complex
	*phase, wilson_vector *dest) {

#ifndef NATIVEDOUBLE
register int i,j;
complex t;
    for(i=0;i<4;i++){
           for(j=0;j<3;j++){
		t = cmul(&src2->d[i].c[j],phase);
                dest->d[i].c[j] = cadd(&src1->d[i].c[j],&t);
           }
    }

#else
register int i,j;
register double sr,si,br,bi,cr,ci;

    sr = (*phase).real; si = (*phase).imag;

    for(i=0;i<4;i++){
	for(j=0;j<3;j++){
	    br=src2->d[i].c[j].real; bi=src2->d[i].c[j].imag;

	    cr = sr*br - si*bi;
	    ci = sr*bi + si*br;

	    dest->d[i].c[j].real = src1->d[i].c[j].real + cr;
	    dest->d[i].c[j].imag = src1->d[i].c[j].imag + ci;
	}
    }
#endif
}
