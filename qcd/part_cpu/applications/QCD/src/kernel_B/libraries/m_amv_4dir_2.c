/*****************  m_amv_4dir_2.c  (in su3.a) *****************************
*									*
*  void mult_adj_su3_mat_vec_4dir_2( su3_matrix *mat,			*
*  su3_vector *src, su3_vector *dest )					*
*  Multiply an su3_vector by an array of four adjoint su3_matrices,	*
*  result in an array of four su3_vectors.				*
*  dest[i]  <-  A_adjoint[i] * src					*
*/
#include "complex.h"
#include "su3.h"

void mult_adj_su3_mat_vec_4dir_2( su3_matrix *mat, su3_vector *src,
    su3_vector *xdest, su3_vector *ydest, su3_vector *zdest,
    su3_vector *tdest ) {
    mult_adj_su3_mat_vec( mat+0, src, xdest );
    mult_adj_su3_mat_vec( mat+1, src, ydest );
    mult_adj_su3_mat_vec( mat+2, src, zdest );
    mult_adj_su3_mat_vec( mat+3, src, tdest );
}
