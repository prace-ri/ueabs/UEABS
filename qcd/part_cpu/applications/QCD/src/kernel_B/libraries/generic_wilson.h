/************************ generic_wilson.h ******************************
*									*
*  Macros and declarations for generic_wilson routines                  *
*  This header is for codes that call generic_wilson routines           *
*  MIMD version 5 							*
*									*
*/

/* For various inversion routines.  Not used sytematically yet. CD */
enum guess_params { START_ZERO_GUESS = 0 ,  START_NONZERO_GUESS } ;  

int wilson_invert(      /* Return value is number of iterations taken */
    field_offset src,   /* type wilson_vector (source already created)*/
    field_offset dest,  /* type wilson_vector (answer and initial guess) */
    field_offset tmp,   /* type wilson_vector (workspace used only for bi-cg)*/
    field_offset sav,   /* type wilson_vector (for saving source) */
    int MinCG,          /* minimum number of iterations per restart */
    int MaxCG,          /* maximum number of iterations per restart */
    int nrestart,       /* maximum restarts */
    radix RsdCG,        /* desired residual - 
			   normalized as sqrt(r*r)/sqrt(src_e*src_e */
    radix *size_r,      /* resulting residual */
    int start_flag,     /* 0: use a zero initial guess; 1: use dest */
    radix Kappa         /* hopping */
    );

int wilson_invert_lean( /* Return value is number of iterations taken */
    field_offset src,   /* type wilson_vector (where source is to be created)*/
    field_offset dest,  /* type wilson_vector (answer and initial guess) */
    field_offset tmp,   /* type wilson_vector (workspace used only for bi-cg)*/
    void (*source_func)(field_offset src, 
			wilson_quark_source *wqs),  /* source function */
    wilson_quark_source *wqs, /* source parameters */
    int MinCG,          /* minimum number of iterations per restart */
    int MaxCG,          /* maximum number of iterations per restart */
    int nrestart,       /* maximum restarts */
    radix RsdCG,        /* desired residual - 
			   normalized as sqrt(r*r)/sqrt(src_e*src_e */
    radix *size_r,      /* resulting residual */
    int start_flag,     /* 0: use a zero initial guess; 1: use dest */
    radix Kappa         /* hopping */
    );


int congrad(int niter,radix rsqmin,radix *final_rsq_ptr);

void copy_site_wilson_vector(field_offset src, field_offset dest);

int cgilu_w(           /* Return value is number of iterations taken */
    field_offset src,   /* type wilson_vector (source vector - OVERWRITTEN!)*/
    field_offset dest,  /* type wilson_vector (answer and initial guess )*/
    int MinCG,          /* minimum number of iterations */
    int MaxCG,          /* maximum number of iterations */
    radix RsdCG,        /* desired residual - 
			   normalized as sqrt(r*r)/sqrt(src_e*src_e */
    radix *size_r,      /* resulting residual */
    int flag,           /* 0: use a zero initial guess; 1: use dest */
    radix Kappa         /* hopping */
    );
int bicgilu_w(           /* Return value is number of iterations taken */
    field_offset src,   /* type wilson_vector (source vector - OVERWRITTEN!)*/
    field_offset dest,  /* type wilson_vector (answer and initial guess )*/
    int MinCG,          /* minimum number of iterations */
    int MaxCG,          /* maximum number of iterations */
    radix RsdCG,        /* desired residual - 
			   normalized as sqrt(r*r)/sqrt(src_e*src_e */
    radix *size_r,      /* resulting residual */
    int flag,           /* 0: use a zero initial guess; 1: use dest */
    radix Kappa,        /* hopping */
    field_offset rv     /* Scratch space of size wilson_vector */
    );
int mrilu_w_or(field_offset src,field_offset dest,int MinMR,int MaxMR,radix RsdMR,
	       radix *size_r,int flag,radix Kappa);

/* For quark source routines */
/* The Weyl representation types are included for w_source_h */
enum source_type { 
  POINT = 1, GAUSSIAN, CUTOFF_GAUSSIAN,
  POINT_WEYL, CUTOFF_GAUSSIAN_WEYL } ;
void w_source(field_offset src,wilson_quark_source *wqs);
void w_source_h(field_offset src,wilson_quark_source *wqs);
radix *make_template(radix gamma, int cutoff);
void w_sink(field_offset snk,wilson_quark_source *wqs);
int ask_quark_source( int prompt, int *type, char *descrp );

void bj_to_weyl( wilson_vector *src, wilson_vector *dest);
void dslash(field_offset src,field_offset dest,
	    int isign,int parity);
void dslash_special(field_offset src,field_offset dest,
		    int isign,int parity,msg_tag **tag,int is_started);
void w_meson(field_offset src1,field_offset src2,complex *prop[10]);
void w_baryon(field_offset src1,field_offset src2,field_offset src3,
	      complex *prop[4]);
void w_baryon_hl(field_offset src1,field_offset src2,
		 field_offset src3, complex *prop[6]);
