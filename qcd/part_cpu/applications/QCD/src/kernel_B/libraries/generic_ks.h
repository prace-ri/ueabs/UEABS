/************************ generic_ks.h **********************************
*									*
*  Macros and declarations for generic_ks routines                      *
*  This header is for codes that call generic_ks routines               *
*  MIMD version 5 							*
*									*
*/

int congrad( int niter, radix rsqmin, int parity, radix *rsq );
void copy_latvec(field_offset src, field_offset dest, int parity);
void dslash( field_offset src, field_offset dest, int parity );
void dslash_special( field_offset src, field_offset dest,
    int parity, msg_tag **tag, int start );
void clear_latvec(field_offset v,int parity);

void scalar_mult_latvec(field_offset src, radix scalar,
			field_offset dest, int parity);
void scalar_mult_add_latvec(field_offset src1, field_offset src2,
			    radix scalar, field_offset dest, int parity);
void grsource(int parity);
void checkmul();
int spectrum();
void make_lattice();
void phaseset();
void rephase( int flag );

void prefetch_vector( su3_vector * );
void prefetch_matrix( su3_matrix * );
void V_sma_and_rdot( su3_vector * ttt_pt, su3_vector * cg_p_pt, radix x,
   double * pkp_pt, int nsites, int stride );
void V_sma2_and_mag( su3_vector * xxx_pt, su3_vector * cg_p_pt,
    su3_vector * resid_pt, su3_vector * ttt_pt, radix a,
    double * rsq_pt, int nsites, int stride );
void V_sma_vec( su3_vector * src1, su3_vector * src2,
    radix scalar, su3_vector * dest, int nsites, int stride );
void V_mult_adj_su3_mat_vec_4dir( su3_matrix * lpt,
    su3_vector * srcpt, su3_vector * destpt, int nsites, int stride );
void V_mult_su3_mat_vec_sum_4dir( su3_matrix * lpt,
    su3_vector ** xpt, su3_vector ** ypt, su3_vector ** zpt, su3_vector ** tpt,
    su3_vector * destpt, int nsites, int stride );


