/************************ generic_form.h *************************************
*									*
*  Macros and declarations for miscellaneous generic routines           *
*  This header is for codes that call generic_form routines             *
*  MIMD version 5 							*
*									*
*/

void c_scale_wilson_vector(wilson_vector *m , complex scale);
void copy_site_wilson_vector(field_offset src, field_offset dest) ;
void flip_source_re(field_offset quark_prop);
int load_momentum_from_disk(int mom_in[][3], char filename[], int max_mom);
void load_scalar_smear(radix *data, int dim, char filename[]);
void load_smearing(field_offset where_smear, char filename[80]);
void mult_gamma(int phase, gamma_matrix *g1, gamma_matrix *g2, gamma_matrix *g3);
void make_gammas(gamma_matrix *gamma);
void mult_sw_by_gamma_l(spin_wilson_vector * src,
			spin_wilson_vector * dest, int dir);
void mult_sw_by_gamma_r(spin_wilson_vector * src,
			spin_wilson_vector * dest, int dir);
void meson_cont_mom(complex prop[],
		    field_offset src1,field_offset src2,
		    int base_pt, int q_stride, int op_stride,
		    gamma_corr gamma_table[], int no_gamma_corr);
void load_wilson_source(field_offset src, field_offset dest,int color,int spin);

void load_wvec(wilson_vector *dest, complex *z, int spin, int colour) ;




