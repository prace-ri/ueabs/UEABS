/********************  vol_s_m_a_wvec.c  (in su3.a) ********************
*
*void scalar_mult_add_wvec(wilson_vector *src1, wilson_vector *src2,
	                   radix s, wilson_vector *dest)
*  Multiply a Wilson vector by a scalar and add to another vector
*  dest  <-  src1 + s*src2
*/

#include "lattice.h"

void scalar_mult_sum_wvec_V(wilson_vector s1[], wilson_vector s2[], radix ss,
			    wilson_vector d[]) 
{
  int i;
  wilson_vector *dest;
  register wilson_vector src1,src2;

  for_active_sites(i) {
    src1 = s1[i];
    src2 = s2[i];
    dest = &d[i]

    dest->d[0].c[0].real = src1.d[0].c[0].real + ss*src2.d[0].c[0].real;
    dest->d[0].c[0].imag = src1.d[0].c[0].imag + ss*src2.d[0].c[0].imag;
    dest->d[0].c[1].real = src1.d[0].c[1].real + ss*src2.d[0].c[1].real;
    dest->d[0].c[1].imag = src1.d[0].c[1].imag + ss*src2.d[0].c[1].imag;
    dest->d[0].c[2].real = src1.d[0].c[2].real + ss*src2.d[0].c[2].real;
    dest->d[0].c[2].imag = src1.d[0].c[2].imag + ss*src2.d[0].c[2].imag;
  
    dest->d[1].c[0].real = src1.d[1].c[0].real + ss*src2.d[1].c[0].real;
    dest->d[1].c[0].imag = src1.d[1].c[0].imag + ss*src2.d[1].c[0].imag;
    dest->d[1].c[1].real = src1.d[1].c[1].real + ss*src2.d[1].c[1].real;
    dest->d[1].c[1].imag = src1.d[1].c[1].imag + ss*src2.d[1].c[1].imag;
    dest->d[1].c[2].real = src1.d[1].c[2].real + ss*src2.d[1].c[2].real;
    dest->d[1].c[2].imag = src1.d[1].c[2].imag + ss*src2.d[1].c[2].imag;
  
    dest->d[2].c[0].real = src1.d[2].c[0].real + ss*src2.d[2].c[0].real;
    dest->d[2].c[0].imag = src1.d[2].c[0].imag + ss*src2.d[2].c[0].imag;
    dest->d[2].c[1].real = src1.d[2].c[1].real + ss*src2.d[2].c[1].real;
    dest->d[2].c[1].imag = src1.d[2].c[1].imag + ss*src2.d[2].c[1].imag;
    dest->d[2].c[2].real = src1.d[2].c[2].real + ss*src2.d[2].c[2].real;
    dest->d[2].c[2].imag = src1.d[2].c[2].imag + ss*src2.d[2].c[2].imag;
  
    dest->d[3].c[0].real = src1.d[3].c[0].real + ss*src2.d[3].c[0].real;
    dest->d[3].c[0].imag = src1.d[3].c[0].imag + ss*src2.d[3].c[0].imag;
    dest->d[3].c[1].real = src1.d[3].c[1].real + ss*src2.d[3].c[1].real;
    dest->d[3].c[1].imag = src1.d[3].c[1].imag + ss*src2.d[3].c[1].imag;
    dest->d[3].c[2].real = src1.d[3].c[2].real + ss*src2.d[3].c[2].real;
    dest->d[3].c[2].imag = src1.d[3].c[2].imag + ss*src2.d[3].c[2].imag;
  }
}
