/****************************************************************
 * MIMD SU3 
 *                                                              *
 *     Measure the polyakov loop
 *                                                              *
 ***************************************************************/

#include "lattice.h"    /* global variables for lattice fields */

complex measure_ploop(su3_matrix *U[NDIM], int dir)
{
  su3_matrix ploop,tm;
  int i,j,loc,nt;
  complex sum,ct;
  
  /** THIS SIMPLE VERSION WORKS ONLY IF THE dir-DIRECTION
   *  FITS COMPLETELY WITHIN ONE NODE.  THUS,
   */

  sum = cmplx(0.0,0.0);

  if (node.nodesize[dir] != lattice.size[dir])
    halt(" PLOOP:: lattice size error!");
  
  nt = lattice.size[dir];

  /* Now, multiply all dir-links */
  forallsites(i) if (coordinate(i,dir) == 0) {
    ploop = U[dir][i];
    loc = i;
    for (j=1; j<nt; j++) {
      loc = nb(dir,loc);
      mult_su3_nn( &ploop, &U[dir][loc], &tm );
      ploop = tm;
    }
    ct = trace_su3( &ploop );
    CSUM( sum, ct );
  }

  CDIVREAL( sum, (((radix)lattice.volume) / lattice.size[dir]), sum );

  return( sum );
}
