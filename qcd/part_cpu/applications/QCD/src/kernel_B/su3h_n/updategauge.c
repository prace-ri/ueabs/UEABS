/************************** updategauge.c *******************************/
/* Microcanonical overrelaxation by doing successive SU(2) gauge hits */
/* MIMD version 3 */
/* T. DeGrand March 1991 */
/* Heavily modified K.R. 97 & 2002 */

#include "lattice.h"


void updategauge(int isrelax)
{
  /* Do overrelaxation by SU(2) subgroups */
  int i,parity;
  static int dir = XUP;
  su3_matrix *a_uncmpr, *staple;
  msg_tag *tag;

  ++dir; if (!is_up_dir(dir)) dir = XUP;  /* rotate directions */
  
#ifdef HIGGS
  a_uncmpr = tmp_latfield( su3_matrix );
#endif
  staple   = tmp_latfield( su3_matrix );

  forbothparities(parity) {

#ifdef HIGGS
    /* collect adjoint field from up */
    tag = start_get( ahiggs, dir, parity );

    /* uncompress the adjoint, it is needed, to ->a_uncmp */
    forparity(i,parity) {
      prefetch_adjoint(ahiggs+2+i);
      uncompress_adjmat(&(ahiggs[i]),&(a_uncmpr[i]));
    }
#endif    

    /* compute the gauge force */
    staples_su3(U, staple, dir, parity);  /* goest to ->staple */

#ifdef HIGGS
    wait_get(tag);  /* wait for ahiggs from up */
    if (isrelax) relax(dir, parity, U, staple, a_uncmpr);
    else         monte(dir, parity, U, staple, a_uncmpr);
#else
    if (isrelax) relax(dir, parity, U, staple );
    else         monte(dir, parity, U, staple );
#endif

  }
  
#ifdef HIGGS
  free_tmp( a_uncmpr );
#endif
  free_tmp( staple );
}

