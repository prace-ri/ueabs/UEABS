/********************* HBHiggs.c ************************/

#include "lattice.h"

/* #include "gaussian_ran.c"
 */	

double Goverrelax(int parity, adjoint_matrix *ahiggs, adjoint_matrix *adjstaple)
{
  int i,j,nhit;
  double rsum,sm,r2,r2o;
  adjoint_matrix m;

  nhit = 0;
  rsum = 0.0;
  
  sm = betaA/beta2;

  forparity(i,parity) {

    prefetch_adjoint(&ahiggs[i+1]);
    prefetch_adjoint(&adjstaple[i+1]);

    /* perform a Gaussian overrelax for Higgs: Since
     *    Act = -bA A.S + b2 A^2 + b4 A^4, we can do        
     *    Act = b2 (A - bA/(2b2) S)^2 + b4 A^4
     * Thus, reflect A using the gaussian potential:
     *    (A'-bA/2b2 S) = -(A-bA/2b2 S) =>
     *    A' = bA/b2 S - A
     * Accept/reject with the change in the A^4-term
     */
    
    for (r2o=j=0; j<8; j++) r2o += sqr( ahiggs[i].l[j] );

    for (r2=j=0; j<8; j++) {
      m.l[j] = sm * adjstaple[i].l[j] - ahiggs[i].l[j];
      r2 += sqr( m.l[j] );
    }
    /* acc/rej with A^4 */
    if ( exp( beta4*(r2o*r2o - r2*r2) ) >= dran() ) {
      ahiggs[i] = m;
      nhit++;
      rsum += r2;
    } else rsum += r2o;
  }
 
  nhitog++;
  if (parity == EVEN) ahitog += 1.0*nhit/node.evensites;
  else ahitog += 1.0*nhit/node.oddsites;

  return(rsum);
}
 
