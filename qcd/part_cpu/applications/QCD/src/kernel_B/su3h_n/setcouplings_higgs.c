/******** setcouplings_higgs.c *********/
/* MIMD code version 3 */

#include "lattice.h"

/* Each node has a params structure for passing simulation parameters */

#define Sigma 3.1759115
/* #define improve_y */



void setcouplings()
{
  if (this_node == 0) 
    printf(" Input couplings: betag %.8g  x %.8g  y %.8g\n",betag,p_x,p_y);

  /* Improved x: from eq. (2.8) in JHEP11 (1998) 011
   */

#ifdef improve_y
  p_y *= 1 + (2.574608+0.72985*p_x) / betag;
#endif

#define improve_x
#ifdef improve_x
  p_x = p_x + ( 0.328432 - 0.835282 * p_x + 1.167759 * sqr(p_x) ) / betag;
  if (this_node == 0) 
    printf(" USING IMPROVED x\n");
#else
  if (this_node == 0)
    printf(" USING NON-IMPROVED x\n");
#endif

  /* and calculate the lattice couplings: 
   * NOTE: This normalizes A0  -> sqrt(2) A0, compared to paper
   * thus, now h^a h^a = 2 Tr h^2 = Tr A_cont
   * AND (h^a h^a)^2 = (2 Tr h^2)^2 = (Tr A_cont)^2
   *
   * Now Tr h^3 = 2^(-3/2) Tr A_cont^3 (note also extra 3 in the measurement)
   */

  betaA = 12/betag;
  beta4 = p_x * 1.5 * sqr(betaA)/betag;
  beta2 = 3*betaA * (1 + 6*p_y/sqr(betag) 
		     - (6 + 10*p_x)*Sigma/(4*pi*betag)
		     - 6/(16*sqr(pi*betag)) * 
		     ((60*p_x - 20*sqr(p_x))*(log(betag) + 0.08849)
		      + 34.768*p_x + 36.130));

  betay = 3*betaA * 6/sqr(betag);

  if (this_node == 0) {
    printf(" Non-improved couplings:   betag %.8g  x %.8g  y %.8g\n",betag,p_x,p_y);
    printf(" Other lattice couplings:  betaA %.8g  beta2 %.8g  beta4 %.8g\n",
	   betaA,beta2,beta4);

    printf(" OUTPUT NORMALIZATION: #5 = Tr A0^2, #7 = (Tr A0^2)^2\n");

  }
}

