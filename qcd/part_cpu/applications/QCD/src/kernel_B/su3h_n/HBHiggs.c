/********************* HBHiggs.c ************************/

#include "lattice.h"

/* #include "gaussian_ran.c"
 */	

double HBHiggs(int parity, adjoint_matrix *ahiggs, adjoint_matrix *adjstaple)
{
  int i,j,ntry;
  double rsum,w,sm,r2,r4o;

  ntry = 0;
  rsum = 0.0;
  
  w  = 1.0/sqrt(beta2);
  sm = betaA/(2.0*beta2);

  forparity(i,parity) {

    prefetch_adjoint(&ahiggs[i+1]);
    prefetch_adjoint(&adjstaple[i+1]);

    /* perform a heat bath update for Higgs: Since
     *    Act = -bA A.adjStaple + b2 A^2 + b4 A^4, we can do        
     *    Act = b2 (A - bA/(2b2) adjStaple)^2 + b4 A^4
     * Thus, pull A from a gaussian distribution
     *    A = bA/2b2 S + 1/sqrt(b2) gaussian_ran()
     * and acc/rej with the b4-term
     */
    
    /* for (r4o=j=0; j<8; j++) r4o += sqr( st->ahiggs.l[j] );
       r4o *= r4o; */

    do {
      ++ntry;
      for (r2=j=0; j<8; j++) {
	ahiggs[i].l[j] = sm * adjstaple[i].l[j] + w * gaussian_ran();
	r2 += sqr( ahiggs[i].l[j] );
      }
      /* acc/rej with A^4 */
    } while ( exp( -beta4*r2*r2 ) < dran() );  /* loop until ok */
    
    rsum += r2;
  }
 
  nhithb++;
  if (parity == EVEN) ahithb += 1.0*node.evensites/ntry;
  else ahithb += 1.0*node.oddsites/ntry;

  return(rsum);
}
 
