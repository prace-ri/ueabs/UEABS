/******** setup.c *********/
/* MIMD code version 3 */

#include "lattice.h"

typedef adjoint_matrix multi_type;         /* defines type of MC field */
#define multi_field ahiggs         /* defines MC field */
#define multi_order(i) adj_sqr( &ahiggs[i] )   /* defines MC order param. */

#include "../generic/multican_generic.c"

