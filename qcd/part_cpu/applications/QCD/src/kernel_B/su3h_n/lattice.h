/****************************** lattice.h ********************************/

/* include file for SU3-adjoint Higgs program, version 2
   This file defines global scalars and the fields in the lattice. */

/* #define check */
  
#ifdef CONTROL
#define EXTERN 
#else
#define EXTERN extern
#endif

#define PI 3.14159265358979323846
#define pi PI
#define pi2 (PI*2.0)

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "complex.h"
#include "su3.h"
#include "comdefs.h"
#include "generic.h"
#include "generic_su3.h"

#ifndef check
#define check_action(a) /* nothing */
#endif

#define MAX_BOP 5   /* max number of blockings */

/* The following are global scalars */
EXTERN	long seed;		/* random number seed */
EXTERN  int mc_steps,n_measurement,n_save;
EXTERN  int n_iteration,n_thermal,iteration;
EXTERN	double betag;
#ifdef HIGGS
EXTERN  double p_x,p_y,betaA,beta4,beta2,betay;
EXTERN  int n_correlation,w_correlation;
#endif

EXTERN  double wvalue; /*for multicanonical */
EXTERN  double timeu,timea,timerest;
EXTERN  double ahitu,ahitua,ahithb,ahitax,ahitmc,ahitog; /* hit*/
EXTERN  int    nhitu,nhitua,nhithb,nhitax,nhitmc,nhitog;
EXTERN  int    meas_sync,corr_sync;

#ifdef HIGGS
/* correlation function globals */
EXTERN  int    corrlen,n_corr;
EXTERN  int    n_bop,n_blocking,b_level[MAX_BOP];

/* correlation function pointers */
#define N_CORR 8
EXTERN  float *c_array;
EXTERN  float *cr2[MAX_BOP],*cr3[MAX_BOP],*ch0[MAX_BOP],*ch1[MAX_BOP];
EXTERN  float *cH0[MAX_BOP],*cH1[MAX_BOP],*cp0[MAX_BOP],*cp1[MAX_BOP];

#define b_const_a1      0.2
#define b_const_a2     (0.25*(1.0-b_const_a1))
#define b_const_g1      0.334
#define b_const_g2     (0.5*(1.0-b_const_g1))

#endif

/*****************************************************************
 * Field variables
 */

EXTERN su3_matrix *U[NDIM];
#ifdef HIGGS
EXTERN adjoint_matrix *ahiggs;
#endif

/*****************************************************************/

#define confname    "config"

/* PABS replace status by kernel_B.input.status */
#define statname    "kernel_B.input.status"

#define measurename "measure"
#define corrname    "correl"
#define wlname      "wloop"

/* PABS replace beta by kernel_B.input.beta */
#define betaname    "kernel_B.input.beta"

#define weightname  "weight"

/* PABS replace parameters by kernel_B.input.parameters */
#define paramname   "kernel_B.input.parameters"

#ifndef T3E
#define prefetch_adjoint(x)  /* nothing */
#define prefetch_matrix(x)   /* nothing */
#endif

void reunitarize(su3_matrix *link[NDIM]);
int setup(void);
void load_config(int status);
void updatehiggs(int isover);
void measure(); void writemeas(); void hcorr(); void writecorr();
void setfiles(int restart);
void dumpall(int status,int * maxiters);
void updategauge(int isrelax);
void relax(int dir, int parity, su3_matrix *link[NDIM], su3_matrix *staple
#ifdef HIGGS
	   , su3_matrix *ac
#endif
	   );
void monte(int dir, int parity, su3_matrix *link[NDIM], su3_matrix *staple
#ifdef HIGGS
	   , su3_matrix *ac
#endif
	   );
void staples_su3(su3_matrix *link[NDIM], su3_matrix *staple, int dir1,int parity);
double Xoverrelax(int parity, adjoint_matrix *ahiggs, adjoint_matrix *astaple);
double HBHiggs(int parity, adjoint_matrix *ahiggs, adjoint_matrix *astaple);
double act_gauge_adj(su3_matrix *a, su3_matrix *u,adjoint_matrix *b);

complex measure_ploop(su3_matrix *link[NDIM], int dir);

void staple1(int i, int dir1, MATRIX *link[NDIM], MATRIX *staple) ;



