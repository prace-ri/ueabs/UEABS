
/*
 * void make_adjointmat( su3_matrix *m3, adjointmat *ah3)   
 * takes the hermitian and traceless part of su3_matrix 
 * in terms of generators					
 */
#include "complex.h"
#include "su3.h"

#define sqrt3 1.7320508075688772

void 
compress_adjmat(m3,a3)
     su3_matrix *m3;
     adjoint_matrix *a3;
{
  a3->l[0]  = m3->e[0][0].real - m3->e[1][1].real;
  a3->l[1]  = (1.0/sqrt3)*(m3->e[0][0].real + 
			   m3->e[1][1].real - 2.0*m3->e[2][2].real);

  a3->l[2]  = m3->e[0][1].real + m3->e[1][0].real;
  a3->l[3]  = m3->e[0][1].imag - m3->e[1][0].imag;
  a3->l[4]  = m3->e[0][2].real + m3->e[2][0].real;
  a3->l[5]  = m3->e[0][2].imag - m3->e[2][0].imag;
  a3->l[6]  = m3->e[1][2].real + m3->e[2][1].real;
  a3->l[7]  = m3->e[1][2].imag - m3->e[2][1].imag;
}/* make_adjmat */


/*
 * void uncompress_adjointmat( su3_matrix *m3, adjointmat *ah3)	 
 * takes the adjoint matrix and throws it in SU(3)-matrix        
 */

void 
uncompress_adjmat(a3,m3)
     su3_matrix *m3;
     adjoint_matrix *a3;
{
  radix t;

  t = a3->l[1]*(1.0/sqrt3);

  m3->e[0][0].real = 0.5*(a3->l[0]  +  t);
  m3->e[0][0].imag = 0.0;
  m3->e[1][1].real = 0.5*(-a3->l[0] +  t);
  m3->e[1][1].imag = 0.0;
  m3->e[2][2].real = -t;
  m3->e[2][2].imag = 0.0;

  m3->e[0][1].real = m3->e[1][0].real = 0.5*a3->l[2];
  m3->e[0][1].imag =  0.5*a3->l[3];
  m3->e[1][0].imag = -0.5*a3->l[3];

  m3->e[0][2].real = m3->e[2][0].real = 0.5*a3->l[4];
  m3->e[0][2].imag =  0.5*a3->l[5];
  m3->e[2][0].imag = -0.5*a3->l[5];

  m3->e[1][2].real = m3->e[2][1].real = 0.5*a3->l[6];
  m3->e[1][2].imag =  0.5*a3->l[7];
  m3->e[2][1].imag = -0.5*a3->l[7];
}/* uncmp_adjointmat */


/* void make_adjointmat( su3_matrix *m3, adjointmat *ah3) 
 * takes the hermitian and traceless part of su3_matrix
 * in terms of generators
 */

void
make_adjointmat(m3,a3)
     su3_matrix *m3;
     adjoint_matrix *a3;
{
  compress_adjmat(m3,a3);
}


/******************************************************
 *
 *   adjoint arithmetics
 *
 *****************************************************/

void
add_adjmat(a,b,t)
     adjoint_matrix *a,*b,*t;
{
  int i;
  for (i=0; i<8; i++) t->l[i] = a->l[i] + b->l[i];
}


void
adj_scalar_mul(a,s,t)
     adjoint_matrix *a,*t;
     double s;
{
  int i;
  for (i=0; i<8; i++) t->l[i] = (s) * a->l[i];
}


void
adj_scalar_mul_add(a,s,t)
     adjoint_matrix *a,*t;
     double s;
{
  int i;
  for (i=0; i<8; i++) t->l[i] += (s) * a->l[i];
}

radix
adj_sqr(adjoint_matrix *a)
{
  int i;
  radix f;
  for (f=i=0; i<8; i++) f += a->l[i] * a->l[i];
  return(f);
}

radix
adj_dot(adjoint_matrix *a,adjoint_matrix *b)
{
  int i;
  radix f;
  for (f=i=0; i<8; i++) f += a->l[i] * b->l[i];
  return(f);
}


void 
mult_su3_ahiggs( su3_matrix *m, adjoint_matrix *a, adjoint_matrix *r )
{
  su3_matrix tmat1,tmat2;

  uncompress_adjmat( a, &tmat1 );
  mult_su3_nn( m, &tmat1, &tmat2 );
  mult_su3_na( &tmat2, m, &tmat1 );
  compress_adjmat( &tmat1, r );
}

void 
mult_adj_su3_ahiggs( su3_matrix *m, adjoint_matrix *a, adjoint_matrix *r )
{
  su3_matrix tmat1,tmat2;

  uncompress_adjmat( a, &tmat1 );
  mult_su3_na( m, &tmat1, &tmat2 );
  mult_su3_nn( &tmat2, m, &tmat1 );
  compress_adjmat( &tmat1, r );
}




void
mult_su3_by_I(su3_matrix *a, su3_matrix *b)
{
  int i,j;

  for(i=0;i<3;i++)for(j=0;j<3;j++) CMUL_I(a->e[i][j],b->e[i][j]);
}
