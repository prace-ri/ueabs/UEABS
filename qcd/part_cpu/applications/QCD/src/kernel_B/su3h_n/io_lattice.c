/*********************** io_lattice.c *************************/
/* This reads and writes a (binary) lattice
 *
 * THIS IS AN ENCAPSULATING FILE TO 
 * ../generic/io_lattice_generic.c 
 * HERE WE HAVE TO DEFINE
 *
 * typedef struct { } allfields;
 *
 * copy_fields(int site, allfields *s)    copy from all latfields to s.(whatever)
 * set_fields(allfields *s, int site)     copy s.(stuff) to lattice fields
 *
 * #include "../generic/io_lattice_generic.c"
 */ 

#include "lattice.h"

typedef struct {
  su3_matrix link[NDIM];
#ifdef HIGGS
  adjoint_matrix ahiggs;
#endif
} allfields;

void set_fields( allfields *s, int i )
{
  int dir;
  
  foralldir(dir) U[dir][i] = s->link[dir];
#ifdef HIGGS
  ahiggs[i] = s->ahiggs;
#endif
}

void copy_fields( int i, allfields *s )
{
  int dir;
  
  foralldir(dir) s->link[dir] = U[dir][i];
#ifdef HIGGS
  s->ahiggs = ahiggs[i];
#endif
}

#include "../generic/io_lattice_generic.c"

