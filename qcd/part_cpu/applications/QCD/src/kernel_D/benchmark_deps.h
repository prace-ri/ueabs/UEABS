/*
*  DUMMY-HEADER
*
*  to avoid cross dependencies
*  which we dont need for benchmarking
* 
*
*
*/
#ifndef _BENCHMARK_DEPS_H
#define _BENCHMARK_DEPS_H
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* struct conaining all neccessary information to perform the preconditioning */
typedef struct spinorPrecWS_{
  /* spinor containing projectors belonging to all eigenvalues with positive imaginary part */
  /* spinor containing projectors belonging to all eigenvalues with positive imaginary part */
  spinor **spinor_up;

  spinor* spinorMemBuff;


  /* array containing eigenvalues */
  _Complex double *evs;

  /* sinus and cosinus lookup table */
  double *c_table;
  double *s_table;

//  tm_operator m_op;

  _Complex double averageLambda;

  /* correction function parameters */
  unsigned int useCorrectionFunc;
  double ai[4];

  double precExpo[3];

} spinorPrecWS;

/* ??? */
void spinorPrecondition(spinor *spinor_out,const spinor* spinor_in,spinorPrecWS* ws,int tt,int ll,const _Complex double alpha,unsigned int dagger,unsigned int autofft);



#endif