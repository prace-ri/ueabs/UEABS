#ifndef _GENERIC_H
#define _GENERIC_H
/************************ generic.h *************************************
*									*
*  Macros and declarations for miscellaneous generic routines           *
*  This header is for codes that call generic routines                  *
*  MIMD version 6 							*
*									*
*/

/* Other generic directory declarations are elsewhere:

   For com_*.c, see comdefs.h
   For io_lat4.c io_ansi.c, io_nonansi.c, io_piofs.c, io_romio.c see io_lat.h
   For io_wb3.c, see io_wb.h
 */

#include "../include/int32type.h"
#include "../include/complex.h"
#include "../include/macros.h"
#include "../include/random.h"


/* ax_gauge.c */
void ax_gauge(  );
void w_loop1( int nsmear );
void w_loop2( int nsmear );

/* check_unitarity.c */
double check_unitarity( void );

/* d_plaq?.c */
void d_plaquette( double *ss_plaq, double *st_plaq );

/* gaugefix.c and gaugefix2.c */
void gaugefix( int gauge_dir, double relax_boost, int max_gauge_iter, double gauge_fix_tol );

/* gauge_stuff.c */
void dsdu_qhb_subl( int dir, int subl );

/* layout_*.c */
void setup_layout_KE( void );
int node_number_KE( int x, int y, int z, int t );
int node_index_KE( int x, int y, int z, int t );
int num_sites( int node );

/* make_lattice.c */
void make_lattice(  );
int site_mu( int i, int mu );
int taxi_dist( int j );

/* make_global_fields.c */
void make_global_fields(  );

/* plaquette4.c */
void plaquette( double *ss_plaq, double *st_plaq );

/* ploop?.c */
complex ploop( void );
complex ploop_dir( int dir );

/* ploop_staple.c */
complex ploop_staple( double alpha_fuzz );

/* project_su3_hit.c */
void project_su3( su3_matrix * w,	/* input initial guess. output resulting
					   SU(3) matrix */
		  su3_matrix * q,	/* starting 3 x 3 complex matrix */
		  int Nhit,	/* number of SU(2) hits. 0 for no projection */
		  double tol	/* tolerance for SU(3) projection.
				   If nonzero, treat Nhit as a maximum
				   number of hits.  If zero, treat Nhit
				   as a prescribed number of hits. */
     );

/* rand_gauge.c */
void rand_gauge( field_offset G );

/* ranmom.c */
void ranmom( void );

/* ranstuff.c */
double myrand(  );

/* reunitarize2.c */
void reunitarize( void );
int reunit_su3( su3_matrix * c );

/* smearing.c */
void ape_smearing( double smear_fac );

/* hyp_smearing.c */
void malloc_hyp(  );
void free_hyp(  );
void ape_block_det( int NumStp );

/* exp_smearing.c */
void malloc_stout();
void stout_smear_main();
void free_stout();

#endif /* _GENERIC_H */
