#ifndef _GENERIC_WILSON_H
#define _GENERIC_WILSON_H
/************************ generic_wilson.h ******************************
 *									*
 *  Macros and declarations for generic_wilson routines                  *
 *  This header is for codes that call generic_wilson routines           *
 *  MIMD version 6 							*
 *									*
 */

#include "../include/su3.h"
#include "../include/macros.h"

/* matrix multiplications */
void multiply_fmat( wilson_vector * src, wilson_vector * dest, int isign );
void dslash( wilson_vector * src, wilson_vector * dest, int isign, int parity );

void latutil_cheb(double *tmp1, double *d, double *dd, double *qsrc, 
        double f1, double f2, double fch);


/* other */
void unit_wvec(wilson_vector *wvec, int parity);
void grand_wvec( wilson_vector * chi, int parity );
void clear_latwvec( wilson_vector * chi, int parity );
void funny_wvec( wilson_vector * chi );
int setup_KE(  );
int readin( int prompt );
void meas_perf();

/* single precision stuff */
void malloc_32bit();
void free_32bit();
void multiply_fmat_32( float * src, float * dest, int isign );
void dslash_32( float *src, float *dest, int isign, int parity );
void convert_gauge();
void convert_wvec(wilson_vector *src, float *dest_32);
void latutil_cheb_32(float *tmp1, float *d, float *dd, float *qsrc, 
        float f1, float f2, float fch);
void latutil_xpay_32(float * x, float a, float * y, int parity);
void latutil_axpy_32(float a, float * x, float * y, int parity);
void latutil_5xpay_32(float * x, float a, float * y, int parity);
double latutil_rdot_32(float * x, float * y, int parity);
complex latutil_dot_32(float * x, float * y, int parity);
double latutil_axpy_nrm2_32(float a, float * x, float * y, int parity);
double latutil_nrm2_32(float * x, int parity);

#endif /* _GENERIC_WILSON_H */
