/*************  mult_Hw.c *******************************/
/*
   mult_Hw(wilson_vector *src, wilson_vector *dest);
   dest(x)=g5*{src(x)-kappa*SUM_dirs( (1+g[dir])*U(x,dir)*src(x+dir) 
   +(1-g[dir])*U+(x-dir,dir)*src(x-dir))}       
 */

#include "./include/includes.h"

void dslash( wilson_vector * src, wilson_vector * dest, int isign, int parity );
void dslash_32( float * src, float * dest, int isign, int parity );

/* WILSON */

/* single precision matrix multiplication */
void multiply_fmat_32( float * src, float * dest, int isign )
{
    dslash_32( src, dest, isign, EVEN );
    dslash_32( src, dest, isign, ODD );
    latutil_xpay_32(src,-(float)kappa,dest,EVENANDODD);

}
void multiply_hfmat_32( float * src, float * dest )
{

    dslash_32( src, dest, 1, EVENANDODD );
    latutil_5xpay_32(src,-(float)kappa,dest,EVENANDODD);

}
void multiply_hfmat( wilson_vector * src, wilson_vector * dest )
{
    int i;
    site *s;
    dslash( src, dest, 1, EVENANDODD );
    FORALLSITES(i,s)
    {
        scalar_mult_add_wvec(&(src[i]),&(dest[i]),-kappa,&(dest[i]));
        g5_mult_wvec( &( dest[i] ), &( dest[i] ) );
    }
}
void multiply_fmat( wilson_vector * src, wilson_vector * dest, int isign) 
{
    int i;
    site *s;

    dslash( src, dest, isign, EVENANDODD );

    FORALLSITES(i,s)
        scalar_mult_add_wvec(&(src[i]),&(dest[i]),-kappa,&(dest[i]));
}
