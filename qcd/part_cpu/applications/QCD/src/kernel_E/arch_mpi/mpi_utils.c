#include "includes.h"

/* matrix x matrix */
void mult_su3_na_KE(  su3_matrix *a, su3_matrix *b, su3_matrix *c )
{
    register int i,j,k;
    register complex x,y;
    for(i=0;i<3;i++)for(j=0;j<3;j++)
    {
        x.real=x.imag=0.0;
        for(k=0;k<3;k++){
            CMUL_J( a->ROWCOL(i,k) , b->ROWCOL(j,k) , y );
            CSUM( x , y );
        }
        c->ROWCOL(i,j) = x;
    }
}
void mult_su3_nn_KE(  su3_matrix *a, su3_matrix *b, su3_matrix *c )
{
    register int i,j,k;
    register complex x,y;
    for(i=0;i<3;i++)for(j=0;j<3;j++){
        x.real=x.imag=0.0;
        for(k=0;k<3;k++){
            CMUL( a->ROWCOL(i,k) , b->ROWCOL(k,j) , y );
            CSUM( x , y );
        }
        c->ROWCOL(i,j).real = x.real;
        c->ROWCOL(i,j).imag = x.imag;
    }
}
void mult_su3_an_KE(  su3_matrix *a, su3_matrix *b, su3_matrix *c )
{
    register int i,j,k;
    register complex x,y;
    for(i=0;i<3;i++)for(j=0;j<3;j++){
        x.real=x.imag=0.0;
        for(k=0;k<3;k++){
            CMULJ_( a->ROWCOL(k,i) , b->ROWCOL(k,j), y );
            CSUM( x , y );
        }
        c->ROWCOL(i,j) = x;
    }
}
void mult_su3_aa_KE(  su3_matrix *a, su3_matrix *b, su3_matrix *c )
{
    register int i,j,k;
    register complex x,y;
    for(i=0;i<3;i++)for(j=0;j<3;j++){
        x.real=x.imag=0.0;
        for(k=0;k<3;k++){
            CMULJJ( a->ROWCOL(k,i) , b->ROWCOL(j,k), y );
            CSUM( x , y );
        }
        c->ROWCOL(i,j) = x;
    }
}


inline void mult_su3_32(float *A, float *x, float *y)
{
    register int c, d;
    register float re, im;
    for (c=0;c<3;c++)
    {
        re=im=0;
        for (d=0;d<3;d++)
        {
            re+=A[2*(3*c+d)+0]*x[2*d+0]-A[2*(3*c+d)+1]*x[2*d+1];            
            im+=A[2*(3*c+d)+1]*x[2*d+0]+A[2*(3*c+d)+0]*x[2*d+1];            
        }
        y[2*c+0]=re;
        y[2*c+1]=im;
    }
}

inline void mult_adj_su3_32(float *A, float *x, float *y)
{
    register int c, d;
    register float re, im;
    for (c=0;c<3;c++)
    {
        re=im=0;
        for (d=0;d<3;d++)
        {
            re+=A[2*(3*d+c)+0]*x[2*d+0]+A[2*(3*d+c)+1]*x[2*d+1];            
            im+=-A[2*(3*d+c)+1]*x[2*d+0]+A[2*(3*d+c)+0]*x[2*d+1];            
        }
        y[2*c+0]=re;
        y[2*c+1]=im;
    }
}

/* dslash */
void latutil_dslash0_32(float *src, 
        float *b1, float *b2, float *b3, float *b4, 
        int isign, int parity)
{
    int i, c;
    int lat_begin, lat_end;

    lat_begin = 0;
    if( parity == ODD )
	lat_begin = even_sites_on_node;
    lat_end = sites_on_node;
    if( parity == EVEN )
	lat_end = even_sites_on_node;
    
    if (isign==PLUS)
    {
        for (i=lat_begin;i<lat_end;i++)
        {
            for (c=0;c<3;c++)
            {
                /* X */
                b1[12*i+6*0+2*c+0] = src[24*i+8*c+2*0+0] - src[24*i+8*c+2*3+1];
                b1[12*i+6*0+2*c+1] = src[24*i+8*c+2*0+1] + src[24*i+8*c+2*3+0];
                b1[12*i+6*1+2*c+0] = src[24*i+8*c+2*1+0] - src[24*i+8*c+2*2+1];
                b1[12*i+6*1+2*c+1] = src[24*i+8*c+2*1+1] + src[24*i+8*c+2*2+0];

                /* Y */
                b2[12*i+6*0+2*c+0] = src[24*i+8*c+2*0+0] - src[24*i+8*c+2*3+0];
                b2[12*i+6*0+2*c+1] = src[24*i+8*c+2*0+1] - src[24*i+8*c+2*3+1];
                b2[12*i+6*1+2*c+0] = src[24*i+8*c+2*1+0] + src[24*i+8*c+2*2+0];
                b2[12*i+6*1+2*c+1] = src[24*i+8*c+2*1+1] + src[24*i+8*c+2*2+1];

                /* Z */
                b3[12*i+6*0+2*c+0] = src[24*i+8*c+2*0+0] - src[24*i+8*c+2*2+1];
                b3[12*i+6*0+2*c+1] = src[24*i+8*c+2*0+1] + src[24*i+8*c+2*2+0];
                b3[12*i+6*1+2*c+0] = src[24*i+8*c+2*1+0] + src[24*i+8*c+2*3+1];
                b3[12*i+6*1+2*c+1] = src[24*i+8*c+2*1+1] - src[24*i+8*c+2*3+0];

                /* T */
                b4[12*i+6*0+2*c+0] = src[24*i+8*c+2*0+0] + src[24*i+8*c+2*2+0];
                b4[12*i+6*0+2*c+1] = src[24*i+8*c+2*0+1] + src[24*i+8*c+2*2+1];
                b4[12*i+6*1+2*c+0] = src[24*i+8*c+2*1+0] + src[24*i+8*c+2*3+0];
                b4[12*i+6*1+2*c+1] = src[24*i+8*c+2*1+1] + src[24*i+8*c+2*3+1];
            }

        }

    }
    else
    {
        for (i=0;i<sites_on_node;i++)
        {
            for (c=0;c<3;c++)
            {
                /* XDOWN */
                b1[12*i+6*0+2*c+0] = src[24*i+8*c+2*0+0] + src[24*i+8*c+2*3+1];
                b1[12*i+6*0+2*c+1] = src[24*i+8*c+2*0+1] - src[24*i+8*c+2*3+0];
                b1[12*i+6*1+2*c+0] = src[24*i+8*c+2*1+0] + src[24*i+8*c+2*2+1];
                b1[12*i+6*1+2*c+1] = src[24*i+8*c+2*1+1] - src[24*i+8*c+2*2+0];

                /* YDOWN */
                b2[12*i+6*0+2*c+0] = src[24*i+8*c+2*0+0] + src[24*i+8*c+2*3+0];
                b2[12*i+6*0+2*c+1] = src[24*i+8*c+2*0+1] + src[24*i+8*c+2*3+1];
                b2[12*i+6*1+2*c+0] = src[24*i+8*c+2*1+0] - src[24*i+8*c+2*2+0];
                b2[12*i+6*1+2*c+1] = src[24*i+8*c+2*1+1] - src[24*i+8*c+2*2+1];

                /* ZDOWN */
                b3[12*i+6*0+2*c+0] = src[24*i+8*c+2*0+0] + src[24*i+8*c+2*2+1];
                b3[12*i+6*0+2*c+1] = src[24*i+8*c+2*0+1] - src[24*i+8*c+2*2+0];
                b3[12*i+6*1+2*c+0] = src[24*i+8*c+2*1+0] - src[24*i+8*c+2*3+1];
                b3[12*i+6*1+2*c+1] = src[24*i+8*c+2*1+1] + src[24*i+8*c+2*3+0];

                /* TDOWN */
                b4[12*i+6*0+2*c+0] = src[24*i+8*c+2*0+0] - src[24*i+8*c+2*2+0];
                b4[12*i+6*0+2*c+1] = src[24*i+8*c+2*0+1] - src[24*i+8*c+2*2+1];
                b4[12*i+6*1+2*c+0] = src[24*i+8*c+2*1+0] - src[24*i+8*c+2*3+0];
                b4[12*i+6*1+2*c+1] = src[24*i+8*c+2*1+1] - src[24*i+8*c+2*3+1];
            }

        }
    }

}

void latutil_dslash1_32(float *src, float *u,
        float *b1, float *b2, float *b3, float *b4, 
        int isign, int parity)
{
    int i, c;
    int lat_begin, lat_end;
    float b1temp[12], b2temp[12], b3temp[12], b4temp[12];
    
    lat_begin = 0;
    if( parity == ODD )
	lat_begin = even_sites_on_node;
    lat_end = sites_on_node;
    if( parity == EVEN )
	lat_end = even_sites_on_node;
    
    if (isign==PLUS)
    {
        for (i=lat_begin;i<lat_end;i++)
        {
            for (c=0;c<3;c++)
            {
                /* X */
                b1temp[6*0+2*c+0] = src[24*i+8*c+2*0+0] - src[24*i+8*c+2*3+1];
                b1temp[6*0+2*c+1] = src[24*i+8*c+2*0+1] + src[24*i+8*c+2*3+0];
                b1temp[6*1+2*c+0] = src[24*i+8*c+2*1+0] - src[24*i+8*c+2*2+1];
                b1temp[6*1+2*c+1] = src[24*i+8*c+2*1+1] + src[24*i+8*c+2*2+0];

                /* Y */
                b2temp[6*0+2*c+0] = src[24*i+8*c+2*0+0] - src[24*i+8*c+2*3+0];
                b2temp[6*0+2*c+1] = src[24*i+8*c+2*0+1] - src[24*i+8*c+2*3+1];
                b2temp[6*1+2*c+0] = src[24*i+8*c+2*1+0] + src[24*i+8*c+2*2+0];
                b2temp[6*1+2*c+1] = src[24*i+8*c+2*1+1] + src[24*i+8*c+2*2+1];

                /* Z */
                b3temp[6*0+2*c+0] = src[24*i+8*c+2*0+0] - src[24*i+8*c+2*2+1];
                b3temp[6*0+2*c+1] = src[24*i+8*c+2*0+1] + src[24*i+8*c+2*2+0];
                b3temp[6*1+2*c+0] = src[24*i+8*c+2*1+0] + src[24*i+8*c+2*3+1];
                b3temp[6*1+2*c+1] = src[24*i+8*c+2*1+1] - src[24*i+8*c+2*3+0];

                /* T */
                b4temp[6*0+2*c+0] = src[24*i+8*c+2*0+0] + src[24*i+8*c+2*2+0];
                b4temp[6*0+2*c+1] = src[24*i+8*c+2*0+1] + src[24*i+8*c+2*2+1];
                b4temp[6*1+2*c+0] = src[24*i+8*c+2*1+0] + src[24*i+8*c+2*3+0];
                b4temp[6*1+2*c+1] = src[24*i+8*c+2*1+1] + src[24*i+8*c+2*3+1];
            }

            /* multiply by adjoint matrix */
            mult_adj_su3_32(u+18*(4*i+XUP),b1temp+6*0,b1+12*i+6*0);
            mult_adj_su3_32(u+18*(4*i+XUP),b1temp+6*1,b1+12*i+6*1);
            mult_adj_su3_32(u+18*(4*i+YUP),b2temp+6*0,b2+12*i+6*0);
            mult_adj_su3_32(u+18*(4*i+YUP),b2temp+6*1,b2+12*i+6*1);
            mult_adj_su3_32(u+18*(4*i+ZUP),b3temp+6*0,b3+12*i+6*0);
            mult_adj_su3_32(u+18*(4*i+ZUP),b3temp+6*1,b3+12*i+6*1);
            mult_adj_su3_32(u+18*(4*i+TUP),b4temp+6*0,b4+12*i+6*0);
            mult_adj_su3_32(u+18*(4*i+TUP),b4temp+6*1,b4+12*i+6*1);

        }

    }
    else
    {
        for (i=0;i<sites_on_node;i++)
        {
            for (c=0;c<3;c++)
            {
                /* XDOWN */
                b1temp[6*0+2*c+0] = src[24*i+8*c+2*0+0] + src[24*i+8*c+2*3+1];
                b1temp[6*0+2*c+1] = src[24*i+8*c+2*0+1] - src[24*i+8*c+2*3+0];
                b1temp[6*1+2*c+0] = src[24*i+8*c+2*1+0] + src[24*i+8*c+2*2+1];
                b1temp[6*1+2*c+1] = src[24*i+8*c+2*1+1] - src[24*i+8*c+2*2+0];

                /* YDOWN */
                b2temp[6*0+2*c+0] = src[24*i+8*c+2*0+0] + src[24*i+8*c+2*3+0];
                b2temp[6*0+2*c+1] = src[24*i+8*c+2*0+1] + src[24*i+8*c+2*3+1];
                b2temp[6*1+2*c+0] = src[24*i+8*c+2*1+0] - src[24*i+8*c+2*2+0];
                b2temp[6*1+2*c+1] = src[24*i+8*c+2*1+1] - src[24*i+8*c+2*2+1];

                /* ZDOWN */
                b3temp[6*0+2*c+0] = src[24*i+8*c+2*0+0] + src[24*i+8*c+2*2+1];
                b3temp[6*0+2*c+1] = src[24*i+8*c+2*0+1] - src[24*i+8*c+2*2+0];
                b3temp[6*1+2*c+0] = src[24*i+8*c+2*1+0] - src[24*i+8*c+2*3+1];
                b3temp[6*1+2*c+1] = src[24*i+8*c+2*1+1] + src[24*i+8*c+2*3+0];

                /* TDOWN */
                b4temp[6*0+2*c+0] = src[24*i+8*c+2*0+0] - src[24*i+8*c+2*2+0];
                b4temp[6*0+2*c+1] = src[24*i+8*c+2*0+1] - src[24*i+8*c+2*2+1];
                b4temp[6*1+2*c+0] = src[24*i+8*c+2*1+0] - src[24*i+8*c+2*3+0];
                b4temp[6*1+2*c+1] = src[24*i+8*c+2*1+1] - src[24*i+8*c+2*3+1];
            }

            /* multiply by adjoint matrix */
            mult_adj_su3_32(u+18*(4*i+XUP),b1temp+6*0,b1+12*i+6*0);
            mult_adj_su3_32(u+18*(4*i+XUP),b1temp+6*1,b1+12*i+6*1);
            mult_adj_su3_32(u+18*(4*i+YUP),b2temp+6*0,b2+12*i+6*0);
            mult_adj_su3_32(u+18*(4*i+YUP),b2temp+6*1,b2+12*i+6*1);
            mult_adj_su3_32(u+18*(4*i+ZUP),b3temp+6*0,b3+12*i+6*0);
            mult_adj_su3_32(u+18*(4*i+ZUP),b3temp+6*1,b3+12*i+6*1);
            mult_adj_su3_32(u+18*(4*i+TUP),b4temp+6*0,b4+12*i+6*0);
            mult_adj_su3_32(u+18*(4*i+TUP),b4temp+6*1,b4+12*i+6*1);

        }
    }

}

void latutil_dslash2_32(float *dest, float *u, 
        char **pt1, char **pt2, char **pt3, char **pt4, 
        int isign, int parity)
{
    int i, c;
    int lat_begin, lat_end;
    float b1temp[12], b2temp[12], b3temp[12], b4temp[12];
    float *b1, *b2, *b3, *b4;
    
    lat_begin = 0;
    if( parity == ODD )
	lat_begin = even_sites_on_node;
    lat_end = sites_on_node;
    if( parity == EVEN )
	lat_end = even_sites_on_node;
   
    if (isign==PLUS)
    {
        for (i=lat_begin;i<lat_end;i++)
        {
            b1=(float *)(pt1[i]);
            b2=(float *)(pt2[i]);
            b3=(float *)(pt3[i]);
            b4=(float *)(pt4[i]);

            /* multiply by matrix */
            mult_su3_32(u+18*(4*i+XUP),b1+6*0,b1temp+6*0);
            mult_su3_32(u+18*(4*i+XUP),b1+6*1,b1temp+6*1);
            mult_su3_32(u+18*(4*i+YUP),b2+6*0,b2temp+6*0);
            mult_su3_32(u+18*(4*i+YUP),b2+6*1,b2temp+6*1);
            mult_su3_32(u+18*(4*i+ZUP),b3+6*0,b3temp+6*0);
            mult_su3_32(u+18*(4*i+ZUP),b3+6*1,b3temp+6*1);
            mult_su3_32(u+18*(4*i+TUP),b4+6*0,b4temp+6*0);
            mult_su3_32(u+18*(4*i+TUP),b4+6*1,b4temp+6*1);

            for (c=0;c<3;c++)
            {
                /* XUP - NOSUM */
                dest[24*i+8*c+2*0+0]=+b1temp[6*0+2*c+0];
                dest[24*i+8*c+2*0+1]=+b1temp[6*0+2*c+1];
                dest[24*i+8*c+2*1+0]=+b1temp[6*1+2*c+0];
                dest[24*i+8*c+2*1+1]=+b1temp[6*1+2*c+1];
                dest[24*i+8*c+2*2+0]=+b1temp[6*1+2*c+1];
                dest[24*i+8*c+2*2+1]=-b1temp[6*1+2*c+0];
                dest[24*i+8*c+2*3+0]=+b1temp[6*0+2*c+1];
                dest[24*i+8*c+2*3+1]=-b1temp[6*0+2*c+0];

                /* YUP */
                dest[24*i+8*c+2*0+0]+=b2temp[6*0+2*c+0];
                dest[24*i+8*c+2*0+1]+=b2temp[6*0+2*c+1];
                dest[24*i+8*c+2*1+0]+=b2temp[6*1+2*c+0];
                dest[24*i+8*c+2*1+1]+=b2temp[6*1+2*c+1];
                dest[24*i+8*c+2*2+0]+=b2temp[6*1+2*c+0];
                dest[24*i+8*c+2*2+1]+=b2temp[6*1+2*c+1];
                dest[24*i+8*c+2*3+0]-=b2temp[6*0+2*c+0];
                dest[24*i+8*c+2*3+1]-=b2temp[6*0+2*c+1];

                /* ZUP */
                dest[24*i+8*c+2*0+0]+=b3temp[6*0+2*c+0];
                dest[24*i+8*c+2*0+1]+=b3temp[6*0+2*c+1];
                dest[24*i+8*c+2*1+0]+=b3temp[6*1+2*c+0];
                dest[24*i+8*c+2*1+1]+=b3temp[6*1+2*c+1];
                dest[24*i+8*c+2*2+0]+=b3temp[6*0+2*c+1];
                dest[24*i+8*c+2*2+1]-=b3temp[6*0+2*c+0];
                dest[24*i+8*c+2*3+0]-=b3temp[6*1+2*c+1];
                dest[24*i+8*c+2*3+1]+=b3temp[6*1+2*c+0];

                /* TUP */
                dest[24*i+8*c+2*0+0]+=b4temp[6*0+2*c+0];
                dest[24*i+8*c+2*0+1]+=b4temp[6*0+2*c+1];
                dest[24*i+8*c+2*1+0]+=b4temp[6*1+2*c+0];
                dest[24*i+8*c+2*1+1]+=b4temp[6*1+2*c+1];
                dest[24*i+8*c+2*2+0]+=b4temp[6*0+2*c+0];
                dest[24*i+8*c+2*2+1]+=b4temp[6*0+2*c+1];
                dest[24*i+8*c+2*3+0]+=b4temp[6*1+2*c+0];
                dest[24*i+8*c+2*3+1]+=b4temp[6*1+2*c+1];
            }
        }

    }
    else
    {
        for (i=lat_begin;i<lat_end;i++)
        {
            b1=(float *)(pt1[i]);
            b2=(float *)(pt2[i]);
            b3=(float *)(pt3[i]);
            b4=(float *)(pt4[i]);

            /* multiply by matrix */
            mult_su3_32(u+18*(4*i+XUP),b1+6*0,b1temp+6*0);
            mult_su3_32(u+18*(4*i+XUP),b1+6*1,b1temp+6*1);
            mult_su3_32(u+18*(4*i+YUP),b2+6*0,b2temp+6*0);
            mult_su3_32(u+18*(4*i+YUP),b2+6*1,b2temp+6*1);
            mult_su3_32(u+18*(4*i+ZUP),b3+6*0,b3temp+6*0);
            mult_su3_32(u+18*(4*i+ZUP),b3+6*1,b3temp+6*1);
            mult_su3_32(u+18*(4*i+TUP),b4+6*0,b4temp+6*0);
            mult_su3_32(u+18*(4*i+TUP),b4+6*1,b4temp+6*1);

            for (c=0;c<3;c++)
            {
                /* case XDOWN: - NOSUM */
                dest[24*i+8*c+2*0+0]=+b1temp[6*0+2*c+0];
                dest[24*i+8*c+2*0+1]=+b1temp[6*0+2*c+1];
                dest[24*i+8*c+2*1+0]=+b1temp[6*1+2*c+0];
                dest[24*i+8*c+2*1+1]=+b1temp[6*1+2*c+1];
                dest[24*i+8*c+2*2+0]=-b1temp[6*1+2*c+1];
                dest[24*i+8*c+2*2+1]=+b1temp[6*1+2*c+0];
                dest[24*i+8*c+2*3+0]=-b1temp[6*0+2*c+1];
                dest[24*i+8*c+2*3+1]=+b1temp[6*0+2*c+0];

                /*  case YDOWN: */
                dest[24*i+8*c+2*0+0]+=b2temp[6*0+2*c+0];
                dest[24*i+8*c+2*0+1]+=b2temp[6*0+2*c+1];
                dest[24*i+8*c+2*1+0]+=b2temp[6*1+2*c+0];
                dest[24*i+8*c+2*1+1]+=b2temp[6*1+2*c+1];
                dest[24*i+8*c+2*2+0]-=b2temp[6*1+2*c+0];
                dest[24*i+8*c+2*2+1]-=b2temp[6*1+2*c+1];
                dest[24*i+8*c+2*3+0]+=b2temp[6*0+2*c+0];
                dest[24*i+8*c+2*3+1]+=b2temp[6*0+2*c+1];

                /*  case ZDOWN: */
                dest[24*i+8*c+2*0+0]+=b3temp[6*0+2*c+0];
                dest[24*i+8*c+2*0+1]+=b3temp[6*0+2*c+1];
                dest[24*i+8*c+2*1+0]+=b3temp[6*1+2*c+0];
                dest[24*i+8*c+2*1+1]+=b3temp[6*1+2*c+1];
                dest[24*i+8*c+2*2+0]-=b3temp[6*0+2*c+1];
                dest[24*i+8*c+2*2+1]+=b3temp[6*0+2*c+0];
                dest[24*i+8*c+2*3+0]+=b3temp[6*1+2*c+1];
                dest[24*i+8*c+2*3+1]-=b3temp[6*1+2*c+0];

                /*  case TDOWN: */
                dest[24*i+8*c+2*0+0]+=b4temp[6*0+2*c+0];
                dest[24*i+8*c+2*0+1]+=b4temp[6*0+2*c+1];
                dest[24*i+8*c+2*1+0]+=b4temp[6*1+2*c+0];
                dest[24*i+8*c+2*1+1]+=b4temp[6*1+2*c+1];
                dest[24*i+8*c+2*2+0]-=b4temp[6*0+2*c+0];
                dest[24*i+8*c+2*2+1]-=b4temp[6*0+2*c+1];
                dest[24*i+8*c+2*3+0]-=b4temp[6*1+2*c+0];
                dest[24*i+8*c+2*3+1]-=b4temp[6*1+2*c+1];

            }
        }

    }
    

}

void latutil_dslash3_32(float *dest, 
        char **pt1, char **pt2, char **pt3, char **pt4, 
        int isign, int parity)
{
    int i, c;
    int lat_begin, lat_end;
    float *b1, *b2, *b3, *b4;
    
    lat_begin = 0;
    if( parity == ODD )
	lat_begin = even_sites_on_node;
    lat_end = sites_on_node;
    if( parity == EVEN )
	lat_end = even_sites_on_node;
   
    if (isign==PLUS)
    {
        for (i=lat_begin;i<lat_end;i++)
        {
            b1=(float *)(pt1[i]);
            b2=(float *)(pt2[i]);
            b3=(float *)(pt3[i]);
            b4=(float *)(pt4[i]);
            for (c=0;c<3;c++)
            {
                /* XUP */
                dest[24*i+8*c+2*0+0]+=b1[6*0+2*c+0];
                dest[24*i+8*c+2*0+1]+=b1[6*0+2*c+1];
                dest[24*i+8*c+2*1+0]+=b1[6*1+2*c+0];
                dest[24*i+8*c+2*1+1]+=b1[6*1+2*c+1];
                dest[24*i+8*c+2*2+0]+=b1[6*1+2*c+1];
                dest[24*i+8*c+2*2+1]-=b1[6*1+2*c+0];
                dest[24*i+8*c+2*3+0]+=b1[6*0+2*c+1];
                dest[24*i+8*c+2*3+1]-=b1[6*0+2*c+0];

                /* YUP */
                dest[24*i+8*c+2*0+0]+=b2[6*0+2*c+0];
                dest[24*i+8*c+2*0+1]+=b2[6*0+2*c+1];
                dest[24*i+8*c+2*1+0]+=b2[6*1+2*c+0];
                dest[24*i+8*c+2*1+1]+=b2[6*1+2*c+1];
                dest[24*i+8*c+2*2+0]+=b2[6*1+2*c+0];
                dest[24*i+8*c+2*2+1]+=b2[6*1+2*c+1];
                dest[24*i+8*c+2*3+0]-=b2[6*0+2*c+0];
                dest[24*i+8*c+2*3+1]-=b2[6*0+2*c+1];

                /* ZUP */
                dest[24*i+8*c+2*0+0]+=b3[6*0+2*c+0];
                dest[24*i+8*c+2*0+1]+=b3[6*0+2*c+1];
                dest[24*i+8*c+2*1+0]+=b3[6*1+2*c+0];
                dest[24*i+8*c+2*1+1]+=b3[6*1+2*c+1];
                dest[24*i+8*c+2*2+0]+=b3[6*0+2*c+1];
                dest[24*i+8*c+2*2+1]-=b3[6*0+2*c+0];
                dest[24*i+8*c+2*3+0]-=b3[6*1+2*c+1];
                dest[24*i+8*c+2*3+1]+=b3[6*1+2*c+0];

                /* TUP */
                dest[24*i+8*c+2*0+0]+=b4[6*0+2*c+0];
                dest[24*i+8*c+2*0+1]+=b4[6*0+2*c+1];
                dest[24*i+8*c+2*1+0]+=b4[6*1+2*c+0];
                dest[24*i+8*c+2*1+1]+=b4[6*1+2*c+1];
                dest[24*i+8*c+2*2+0]+=b4[6*0+2*c+0];
                dest[24*i+8*c+2*2+1]+=b4[6*0+2*c+1];
                dest[24*i+8*c+2*3+0]+=b4[6*1+2*c+0];
                dest[24*i+8*c+2*3+1]+=b4[6*1+2*c+1];
            }
        }

    }
    else
    {
        for (i=lat_begin;i<lat_end;i++)
        {
            b1=(float *)(pt1[i]);
            b2=(float *)(pt2[i]);
            b3=(float *)(pt3[i]);
            b4=(float *)(pt4[i]);
            for (c=0;c<3;c++)
            {
                /* case XDOWN: */
                dest[24*i+8*c+2*0+0]+=b1[6*0+2*c+0];
                dest[24*i+8*c+2*0+1]+=b1[6*0+2*c+1];
                dest[24*i+8*c+2*1+0]+=b1[6*1+2*c+0];
                dest[24*i+8*c+2*1+1]+=b1[6*1+2*c+1];
                dest[24*i+8*c+2*2+0]-=b1[6*1+2*c+1];
                dest[24*i+8*c+2*2+1]+=b1[6*1+2*c+0];
                dest[24*i+8*c+2*3+0]-=b1[6*0+2*c+1];
                dest[24*i+8*c+2*3+1]+=b1[6*0+2*c+0];

                /*  case YDOWN: */
                dest[24*i+8*c+2*0+0]+=b2[6*0+2*c+0];
                dest[24*i+8*c+2*0+1]+=b2[6*0+2*c+1];
                dest[24*i+8*c+2*1+0]+=b2[6*1+2*c+0];
                dest[24*i+8*c+2*1+1]+=b2[6*1+2*c+1];
                dest[24*i+8*c+2*2+0]-=b2[6*1+2*c+0];
                dest[24*i+8*c+2*2+1]-=b2[6*1+2*c+1];
                dest[24*i+8*c+2*3+0]+=b2[6*0+2*c+0];
                dest[24*i+8*c+2*3+1]+=b2[6*0+2*c+1];

                /*  case ZDOWN: */
                dest[24*i+8*c+2*0+0]+=b3[6*0+2*c+0];
                dest[24*i+8*c+2*0+1]+=b3[6*0+2*c+1];
                dest[24*i+8*c+2*1+0]+=b3[6*1+2*c+0];
                dest[24*i+8*c+2*1+1]+=b3[6*1+2*c+1];
                dest[24*i+8*c+2*2+0]-=b3[6*0+2*c+1];
                dest[24*i+8*c+2*2+1]+=b3[6*0+2*c+0];
                dest[24*i+8*c+2*3+0]+=b3[6*1+2*c+1];
                dest[24*i+8*c+2*3+1]-=b3[6*1+2*c+0];

                /*  case TDOWN: */
                dest[24*i+8*c+2*0+0]+=b4[6*0+2*c+0];
                dest[24*i+8*c+2*0+1]+=b4[6*0+2*c+1];
                dest[24*i+8*c+2*1+0]+=b4[6*1+2*c+0];
                dest[24*i+8*c+2*1+1]+=b4[6*1+2*c+1];
                dest[24*i+8*c+2*2+0]-=b4[6*0+2*c+0];
                dest[24*i+8*c+2*2+1]-=b4[6*0+2*c+1];
                dest[24*i+8*c+2*3+0]-=b4[6*1+2*c+0];
                dest[24*i+8*c+2*3+1]-=b4[6*1+2*c+1];
            }
        }

    }
    

}

void dslash_32( float * src, float * dest, int isign, int parity )
{
    int dir;
    msg_tag *tag[8];

    latutil_dslash0_32(src,
            htmp_32[XUP],htmp_32[YUP],htmp_32[ZUP],htmp_32[TUP],
            isign,OPP_PAR(parity));
       
    for( dir=XUP; dir <= TUP; dir++) 
        tag[dir]=start_gather_from_temp(htmp_32[dir], 12*sizeof(float),
                12*sizeof(float),dir, parity, gen_pt[dir] );

    latutil_dslash1_32(src,gauge_32,
            htmp_32[XDOWN],htmp_32[YDOWN],htmp_32[ZDOWN],htmp_32[TDOWN],
            -isign,OPP_PAR(parity));


    for( dir=XUP; dir <= TUP; dir++) 
        tag[OPP_DIR(dir)]=start_gather_from_temp(htmp_32[OPP_DIR(dir)],
                12*sizeof(float), 12*sizeof(float),OPP_DIR(dir),
                parity, gen_pt[OPP_DIR(dir)] );

    for( dir=XUP; dir <= TUP; dir++) 
        wait_gather_KE(tag[dir]);

    latutil_dslash2_32(dest,gauge_32,
            gen_pt[XUP],gen_pt[YUP],gen_pt[ZUP],gen_pt[TUP],
            isign,parity);

    for( dir=XUP; dir <= TUP; dir++) 
        cleanup_gather(tag[dir]);

    for( dir=XUP; dir <= TUP; dir++) 
        wait_gather_KE(tag[OPP_DIR(dir)]);

    latutil_dslash3_32(dest,
            gen_pt[XDOWN],gen_pt[YDOWN],gen_pt[ZDOWN],gen_pt[TDOWN],  
            -isign,parity);

    for( dir=XUP; dir <= TUP; dir++) 
        cleanup_gather(tag[OPP_DIR(dir)]);

}

void dslash(wilson_vector *src, wilson_vector *dest, int isign, int parity)
{
    half_wilson_vector hwvx,hwvy,hwvz,hwvt;
    static int i;
    site *s;
    int dir;
    msg_tag *tag[8];

    FORSOMEPARITY(i,s,OPP_PAR(parity))
    {
        wp_shrink_4dir( &(src[i]), &(htmp[XUP][i]),
                &(htmp[YUP][i]), &(htmp[ZUP][i]), &(htmp[TUP][i]), isign);
    }
    for( dir=XUP; dir <= TUP; dir++) 
    {
        tag[dir]=start_gather_from_temp(htmp[dir], sizeof(half_wilson_vector),
                sizeof(half_wilson_vector),dir, parity, gen_pt[dir] );
    }

    FORSOMEPARITY(i,s,OPP_PAR(parity))
    {
        wp_shrink_4dir( &(src[i]),
                &hwvx, &hwvy, &hwvz, &hwvt, -isign);
        mult_adj_su3_mat_hwvec( &(gauge[4*i+XUP]), &hwvx, &(htmp[XDOWN][i]));
        mult_adj_su3_mat_hwvec( &(gauge[4*i+YUP]), &hwvy, &(htmp[YDOWN][i]));
        mult_adj_su3_mat_hwvec( &(gauge[4*i+ZUP]), &hwvz, &(htmp[ZDOWN][i]));
        mult_adj_su3_mat_hwvec( &(gauge[4*i+TUP]), &hwvt, &(htmp[TDOWN][i]));
    }

    for( dir=XUP; dir <= TUP; dir++) 
    {
        tag[OPP_DIR(dir)]=start_gather_from_temp(htmp[OPP_DIR(dir)],
                sizeof(half_wilson_vector), sizeof(half_wilson_vector),OPP_DIR(dir),
                parity, gen_pt[OPP_DIR(dir)] );
    }

    for( dir=XUP; dir <= TUP; dir++) 
    {
        wait_gather_KE(tag[dir]);
    }
    FORSOMEPARITY(i,s,parity)
    {
        mult_su3_mat_hwvec( &(gauge[4*i+XUP]),
                (half_wilson_vector * )(gen_pt[XUP][i]), &hwvx ); 
        mult_su3_mat_hwvec( &(gauge[4*i+YUP]),
                (half_wilson_vector * )(gen_pt[YUP][i]), &hwvy ); 
        mult_su3_mat_hwvec( &(gauge[4*i+ZUP]),
                (half_wilson_vector * )(gen_pt[ZUP][i]), &hwvz ); 
        mult_su3_mat_hwvec( &(gauge[4*i+TUP]),
                (half_wilson_vector * )(gen_pt[TUP][i]), &hwvt ); 
        grow_add_four_wvecs( &(dest[i]),
                &hwvx, &hwvy, &hwvz, &hwvt, isign, 0 ); /*  "0" is NOSUM */
    }

    for( dir=XUP; dir <= TUP; dir++) 
    {
        cleanup_gather(tag[dir]);
    }

    for( dir=XUP; dir <= TUP; dir++) 
    {
        wait_gather_KE(tag[OPP_DIR(dir)]);
    }

    FORSOMEPARITY(i,s,parity)
    {
        grow_add_four_wvecs( &(dest[i]),
                (half_wilson_vector *)(gen_pt[XDOWN][i]),
                (half_wilson_vector *)(gen_pt[YDOWN][i]),
                (half_wilson_vector *)(gen_pt[ZDOWN][i]),
                (half_wilson_vector *)(gen_pt[TDOWN][i]),
                -isign, 1 );	/*  "1" SUMs in current dest */
    }

    for( dir=XUP; dir <= TUP; dir++) 
    {
        cleanup_gather(tag[OPP_DIR(dir)]);
    }

} 

