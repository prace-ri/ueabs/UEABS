/********************** cexp.c (in complex.a) **********************/
/* MIMD version 6 */
/* Subroutines for operations on complex numbers */
/* complex exponential */
#include "../include/config.h"
#include <math.h>
#include "../include/complex.h"

complex cexp_milc( complex * a )
{
    complex c;
    double mag;
    mag = ( double ) exp( ( double ) ( *a ).real );
    c.real = mag * ( double ) cos( ( double ) ( *a ).imag );
    c.imag = mag * ( double ) sin( ( double ) ( *a ).imag );
    return ( c );
}
