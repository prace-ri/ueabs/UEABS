/*****************  wvec_rdot.c  (in su3.a) ******************************
*									*
* double wvec_rdot( wilson_vector *a, wilson_vector *b )			*
* return real part of dot product of two wilson_vectors			*
*/
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"

double wvec_rdot( wilson_vector * a, wilson_vector * b )
{
    register double ar, ai, br, bi, ss;

    ar = a->COLORSPINOR( 0, 0 ).real;
    ai = a->COLORSPINOR( 0, 0 ).imag;
    br = b->COLORSPINOR( 0, 0 ).real;
    bi = b->COLORSPINOR( 0, 0 ).imag;
    ss = ar * br + ai * bi;
    ar = a->COLORSPINOR( 1, 0 ).real;
    ai = a->COLORSPINOR( 1, 0 ).imag;
    br = b->COLORSPINOR( 1, 0 ).real;
    bi = b->COLORSPINOR( 1, 0 ).imag;
    ss += ar * br + ai * bi;
    ar = a->COLORSPINOR( 2, 0 ).real;
    ai = a->COLORSPINOR( 2, 0 ).imag;
    br = b->COLORSPINOR( 2, 0 ).real;
    bi = b->COLORSPINOR( 2, 0 ).imag;
    ss += ar * br + ai * bi;

    ar = a->COLORSPINOR( 0, 1 ).real;
    ai = a->COLORSPINOR( 0, 1 ).imag;
    br = b->COLORSPINOR( 0, 1 ).real;
    bi = b->COLORSPINOR( 0, 1 ).imag;
    ss += ar * br + ai * bi;
    ar = a->COLORSPINOR( 1, 1 ).real;
    ai = a->COLORSPINOR( 1, 1 ).imag;
    br = b->COLORSPINOR( 1, 1 ).real;
    bi = b->COLORSPINOR( 1, 1 ).imag;
    ss += ar * br + ai * bi;
    ar = a->COLORSPINOR( 2, 1 ).real;
    ai = a->COLORSPINOR( 2, 1 ).imag;
    br = b->COLORSPINOR( 2, 1 ).real;
    bi = b->COLORSPINOR( 2, 1 ).imag;
    ss += ar * br + ai * bi;

    ar = a->COLORSPINOR( 0, 2 ).real;
    ai = a->COLORSPINOR( 0, 2 ).imag;
    br = b->COLORSPINOR( 0, 2 ).real;
    bi = b->COLORSPINOR( 0, 2 ).imag;
    ss += ar * br + ai * bi;
    ar = a->COLORSPINOR( 1, 2 ).real;
    ai = a->COLORSPINOR( 1, 2 ).imag;
    br = b->COLORSPINOR( 1, 2 ).real;
    bi = b->COLORSPINOR( 1, 2 ).imag;
    ss += ar * br + ai * bi;
    ar = a->COLORSPINOR( 2, 2 ).real;
    ai = a->COLORSPINOR( 2, 2 ).imag;
    br = b->COLORSPINOR( 2, 2 ).real;
    bi = b->COLORSPINOR( 2, 2 ).imag;
    ss += ar * br + ai * bi;

    ar = a->COLORSPINOR( 0, 3 ).real;
    ai = a->COLORSPINOR( 0, 3 ).imag;
    br = b->COLORSPINOR( 0, 3 ).real;
    bi = b->COLORSPINOR( 0, 3 ).imag;
    ss += ar * br + ai * bi;
    ar = a->COLORSPINOR( 1, 3 ).real;
    ai = a->COLORSPINOR( 1, 3 ).imag;
    br = b->COLORSPINOR( 1, 3 ).real;
    bi = b->COLORSPINOR( 1, 3 ).imag;
    ss += ar * br + ai * bi;
    ar = a->COLORSPINOR( 2, 3 ).real;
    ai = a->COLORSPINOR( 2, 3 ).imag;
    br = b->COLORSPINOR( 2, 3 ).real;
    bi = b->COLORSPINOR( 2, 3 ).imag;
    ss += ar * br + ai * bi;

    return ( ss );
}
