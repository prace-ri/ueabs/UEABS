/******************  complextr.c  (in su3.a) ****************************
*									*
* complex complextrace_su3_KE( su3_matrix *a,*b)				*
* return Tr( A_adjoint*B )   						*
*/
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"

complex complextrace_su3_KE( su3_matrix * a, su3_matrix * b )
{
    register int i, j;
    register double sumr, sumi;
    complex sum;
    for ( sumr = 0.0, sumi = 0.0, i = 0; i < 3; i++ )
	for ( j = 0; j < 3; j++ )
	{
	    sumr += a->ROWCOL( i, j ).real * b->ROWCOL( i, j ).real + a->ROWCOL( i, j ).imag * b->ROWCOL( i, j ).imag;
	    sumi += a->ROWCOL( i, j ).real * b->ROWCOL( i, j ).imag - a->ROWCOL( i, j ).imag * b->ROWCOL( i, j ).real;
	}
    sum.real = sumr;
    sum.imag = sumi;
    return ( sum );
}
