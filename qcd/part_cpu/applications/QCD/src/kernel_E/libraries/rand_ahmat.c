/******************  rand_ahmat.c  (in su3.a) ***************************
*									*
* void random_anti_hermitian_KE( anti_hermitmat *mat_antihermit, passthru *prn_pt)*
* Creates gaussian random anti-hermitian matrices			*
* Normalization is < |m01|^2 > = 1, or < m01.real*m01.real > = 1/2	*
* The argument "prn_pt" is a pointer to be passed to gaussian_rand_no_KE() *
* RS6000 may choke on void *						*
*/
#include "../include/config.h"
#include <math.h>
#include "../include/complex.h"
#include "../include/su3.h"

void random_anti_hermitian_KE( anti_hermitmat * mat_antihermit  )
{
    double r3, r8;
    double sqrt_third;

    sqrt_third = sqrt( 1.0 / 3.0 );
    r3 = gaussian_rand_no_KE(  );
    r8 = gaussian_rand_no_KE(  );
    mat_antihermit->m00im = r3 + sqrt_third * r8;
    mat_antihermit->m11im = -r3 + sqrt_third * r8;
    mat_antihermit->m22im = -2.0 * sqrt_third * r8;
    mat_antihermit->m01.real = gaussian_rand_no_KE(  );
    mat_antihermit->m02.real = gaussian_rand_no_KE(  );
    mat_antihermit->m12.real = gaussian_rand_no_KE(  );
    mat_antihermit->m01.imag = gaussian_rand_no_KE(  );
    mat_antihermit->m02.imag = gaussian_rand_no_KE(  );
    mat_antihermit->m12.imag = gaussian_rand_no_KE(  );

}				/*random_anti_hermitian_ */

void funny_anti_hermitian( int ix, int iy, int iz, int it, int idir, anti_hermitmat * mat_antihermit )
{
    double r3, r8;
    double sqrt_third;

    sqrt_third = sqrt( ( double ) ( 1.0 / 3.0 ) );
    r3 = ( double ) ix / 137.0 * ( double ) idir;
    r8 = ( double ) ( iy + iz ) / 42.0;
    mat_antihermit->m00im = r3 + sqrt_third * r8;
    mat_antihermit->m11im = -r3 + sqrt_third * r8;
    mat_antihermit->m22im = -2.0 * sqrt_third * r8;
    mat_antihermit->m01.real = -( double ) ix / 25. * 10;
    mat_antihermit->m02.real = -( double ) iy / 20. * 10;
    mat_antihermit->m12.real = ( double ) iz / 19. * ( double ) idir *10;
    mat_antihermit->m01.imag = -( double ) it / 80.;
    mat_antihermit->m02.imag = ( double ) it / 5.;
    mat_antihermit->m12.imag = ( double ) ix / 19.;
}
