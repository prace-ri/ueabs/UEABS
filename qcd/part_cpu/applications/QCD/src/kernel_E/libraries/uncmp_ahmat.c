/************  uncmp_ahmat.c  (in su3.a) ********************************
*									*
* void uncompress_anti_hermitian_KE( anti_hermitmat *mat_antihermit,	*
*	su3_matrix *mat_su3 )						*
* uncompresses an anti_hermitian matrix to make a 3x3 complex matrix	*
*/
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"

void uncompress_anti_hermitian_KE( anti_hermitmat * mat_antihermit, su3_matrix * mat_su3 )
{
    /* uncompresses an anti_hermitian su3 matrix */
    double temp1;
    mat_su3->ROWCOL( 0, 0 ).imag = mat_antihermit->m00im;
    mat_su3->ROWCOL( 0, 0 ).real = 0.;
    mat_su3->ROWCOL( 1, 1 ).imag = mat_antihermit->m11im;
    mat_su3->ROWCOL( 1, 1 ).real = 0.;
    mat_su3->ROWCOL( 2, 2 ).imag = mat_antihermit->m22im;
    mat_su3->ROWCOL( 2, 2 ).real = 0.;
    mat_su3->ROWCOL( 0, 1 ).imag = mat_antihermit->m01.imag;
    temp1 = mat_antihermit->m01.real;
    mat_su3->ROWCOL( 0, 1 ).real = temp1;
    mat_su3->ROWCOL( 1, 0 ).real = -temp1;
    mat_su3->ROWCOL( 1, 0 ).imag = mat_antihermit->m01.imag;
    mat_su3->ROWCOL( 0, 2 ).imag = mat_antihermit->m02.imag;
    temp1 = mat_antihermit->m02.real;
    mat_su3->ROWCOL( 0, 2 ).real = temp1;
    mat_su3->ROWCOL( 2, 0 ).real = -temp1;
    mat_su3->ROWCOL( 2, 0 ).imag = mat_antihermit->m02.imag;
    mat_su3->ROWCOL( 1, 2 ).imag = mat_antihermit->m12.imag;
    temp1 = mat_antihermit->m12.real;
    mat_su3->ROWCOL( 1, 2 ).real = temp1;
    mat_su3->ROWCOL( 2, 1 ).real = -temp1;
    mat_su3->ROWCOL( 2, 1 ).imag = mat_antihermit->m12.imag;
}				/*uncompress_anti_hermitian */
