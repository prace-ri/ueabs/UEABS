/********************  s_g5_m_a_wvec.c  (in su3.a) ********************
*
*void scalar_g5_mult_add_wvec(wilson_vector *src1, wilson_vector *src2,
	double s, wilson_vector *dest)
* Multiply a Wilson vector by a scalar and gamma5 and add to another vector 
* dest  <-  src1 + gamma5*s*src2
*/
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"

void scalar_g5_mult_add_wvec( wilson_vector * src1, wilson_vector * src2, double s, wilson_vector * dest )
{
    register int i;
    for ( i = 0; i < 3; i++ )
    {

	dest->COLORSPINOR( i, 0 ).real = src1->COLORSPINOR( i, 0 ).real + s * ( src2->COLORSPINOR( i, 0 ).real );
	dest->COLORSPINOR( i, 1 ).real = src1->COLORSPINOR( i, 1 ).real + s * ( src2->COLORSPINOR( i, 1 ).real );
	dest->COLORSPINOR( i, 2 ).real = src1->COLORSPINOR( i, 2 ).real - s * ( src2->COLORSPINOR( i, 2 ).real );
	dest->COLORSPINOR( i, 3 ).real = src1->COLORSPINOR( i, 3 ).real - s * ( src2->COLORSPINOR( i, 3 ).real );

	dest->COLORSPINOR( i, 0 ).imag = src1->COLORSPINOR( i, 0 ).imag + s * ( src2->COLORSPINOR( i, 0 ).imag );
	dest->COLORSPINOR( i, 1 ).imag = src1->COLORSPINOR( i, 1 ).imag + s * ( src2->COLORSPINOR( i, 1 ).imag );
	dest->COLORSPINOR( i, 2 ).imag = src1->COLORSPINOR( i, 2 ).imag - s * ( src2->COLORSPINOR( i, 2 ).imag );
	dest->COLORSPINOR( i, 3 ).imag = src1->COLORSPINOR( i, 3 ).imag - s * ( src2->COLORSPINOR( i, 3 ).imag );
    }
}
