/********************  s_m_wvec.c  (in su3.a) ********************
*
*void scalar_mult_wvec(wilson_vector *src, double s, wilson_vector *dest)
*  Multiply a Wilson vector by a scalar
* dest  <-  s*src
*/
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"

void scalar_mult_wvec( wilson_vector * src, double s, wilson_vector * dest )
{
    register double ss;
    ss = s;

    dest->COLORSPINOR( 0, 0 ).real = ss * src->COLORSPINOR( 0, 0 ).real;
    dest->COLORSPINOR( 0, 0 ).imag = ss * src->COLORSPINOR( 0, 0 ).imag;
    dest->COLORSPINOR( 1, 0 ).real = ss * src->COLORSPINOR( 1, 0 ).real;
    dest->COLORSPINOR( 1, 0 ).imag = ss * src->COLORSPINOR( 1, 0 ).imag;
    dest->COLORSPINOR( 2, 0 ).real = ss * src->COLORSPINOR( 2, 0 ).real;
    dest->COLORSPINOR( 2, 0 ).imag = ss * src->COLORSPINOR( 2, 0 ).imag;

    dest->COLORSPINOR( 0, 1 ).real = ss * src->COLORSPINOR( 0, 1 ).real;
    dest->COLORSPINOR( 0, 1 ).imag = ss * src->COLORSPINOR( 0, 1 ).imag;
    dest->COLORSPINOR( 1, 1 ).real = ss * src->COLORSPINOR( 1, 1 ).real;
    dest->COLORSPINOR( 1, 1 ).imag = ss * src->COLORSPINOR( 1, 1 ).imag;
    dest->COLORSPINOR( 2, 1 ).real = ss * src->COLORSPINOR( 2, 1 ).real;
    dest->COLORSPINOR( 2, 1 ).imag = ss * src->COLORSPINOR( 2, 1 ).imag;

    dest->COLORSPINOR( 0, 2 ).real = ss * src->COLORSPINOR( 0, 2 ).real;
    dest->COLORSPINOR( 0, 2 ).imag = ss * src->COLORSPINOR( 0, 2 ).imag;
    dest->COLORSPINOR( 1, 2 ).real = ss * src->COLORSPINOR( 1, 2 ).real;
    dest->COLORSPINOR( 1, 2 ).imag = ss * src->COLORSPINOR( 1, 2 ).imag;
    dest->COLORSPINOR( 2, 2 ).real = ss * src->COLORSPINOR( 2, 2 ).real;
    dest->COLORSPINOR( 2, 2 ).imag = ss * src->COLORSPINOR( 2, 2 ).imag;

    dest->COLORSPINOR( 0, 3 ).real = ss * src->COLORSPINOR( 0, 3 ).real;
    dest->COLORSPINOR( 0, 3 ).imag = ss * src->COLORSPINOR( 0, 3 ).imag;
    dest->COLORSPINOR( 1, 3 ).real = ss * src->COLORSPINOR( 1, 3 ).real;
    dest->COLORSPINOR( 1, 3 ).imag = ss * src->COLORSPINOR( 1, 3 ).imag;
    dest->COLORSPINOR( 2, 3 ).real = ss * src->COLORSPINOR( 2, 3 ).real;
    dest->COLORSPINOR( 2, 3 ).imag = ss * src->COLORSPINOR( 2, 3 ).imag;

}
