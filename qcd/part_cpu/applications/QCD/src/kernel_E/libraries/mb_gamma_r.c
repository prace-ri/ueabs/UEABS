/************* mb_gamma_r.c  (in su3.a) **************************/
/* 
   Multiply a Wilson matrix by a gamma matrix acting on the column index
   (This is the second index, or equivalently, multiplication on the right)
   usage:   mult_by_gamma_right wilson_matrix *src,  wilson_matrix *dest,
   int dir )
   dir = XUP, YUP, ZUP, TUP or GAMMAFIVE

   gamma(XUP) 
   0  0  0  i
   0  0  i  0
   0 -i  0  0
   -i  0  0  0

   gamma(YUP)
   0  0  0 -1
   0  0  1  0
   0  1  0  0
   -1  0  0  0

   gamma(ZUP)
   0  0  i  0
   0  0  0 -i
   -i  0  0  0
   0  i  0  0

   gamma(TUP)
   0  0  1  0
   0  0  0  1
   1  0  0  0
   0  1  0  0

   gamma(FIVE) 
   1  0  0  0
   0  1  0  0
   0  0 -1  0
   0  0  0 -1
 */
#include <stdio.h>
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"
#include "../include/dirs.h"

void mult_by_gamma_right( wilson_matrix * src, wilson_matrix * dest, int dir )
{
    register int i;		/*color */
    register int c1, s1;	/* row indices, color and spin */

    switch ( dir )
    {
	case XUP:
	    for ( i = 0; i < 3; i++ )
		for ( s1 = 0; s1 < 4; s1++ )
		    for ( c1 = 0; c1 < 3; c1++ )
		    {
			TIMESMINUSI( src->d[s1].c[c1].COLORSPINOR( i, 3 ), dest->d[s1].c[c1].COLORSPINOR( i, 0 ) );
			TIMESMINUSI( src->d[s1].c[c1].COLORSPINOR( i, 2 ), dest->d[s1].c[c1].COLORSPINOR( i, 1 ) );
			TIMESPLUSI( src->d[s1].c[c1].COLORSPINOR( i, 1 ), dest->d[s1].c[c1].COLORSPINOR( i, 2 ) );
			TIMESPLUSI( src->d[s1].c[c1].COLORSPINOR( i, 0 ), dest->d[s1].c[c1].COLORSPINOR( i, 3 ) );
		    }
	    break;
	case YUP:
	    for ( i = 0; i < 3; i++ )
		for ( s1 = 0; s1 < 4; s1++ )
		    for ( c1 = 0; c1 < 3; c1++ )
		    {
			TIMESMINUSONE( src->d[s1].c[c1].COLORSPINOR( i, 3 ), dest->d[s1].c[c1].COLORSPINOR( i, 0 ) );
			TIMESPLUSONE( src->d[s1].c[c1].COLORSPINOR( i, 2 ), dest->d[s1].c[c1].COLORSPINOR( i, 1 ) );
			TIMESPLUSONE( src->d[s1].c[c1].COLORSPINOR( i, 1 ), dest->d[s1].c[c1].COLORSPINOR( i, 2 ) );
			TIMESMINUSONE( src->d[s1].c[c1].COLORSPINOR( i, 0 ), dest->d[s1].c[c1].COLORSPINOR( i, 3 ) );
		    }
	    break;
	case ZUP:
	    for ( i = 0; i < 3; i++ )
		for ( s1 = 0; s1 < 4; s1++ )
		    for ( c1 = 0; c1 < 3; c1++ )
		    {
			TIMESMINUSI( src->d[s1].c[c1].COLORSPINOR( i, 2 ), dest->d[s1].c[c1].COLORSPINOR( i, 0 ) );
			TIMESPLUSI( src->d[s1].c[c1].COLORSPINOR( i, 3 ), dest->d[s1].c[c1].COLORSPINOR( i, 1 ) );
			TIMESPLUSI( src->d[s1].c[c1].COLORSPINOR( i, 0 ), dest->d[s1].c[c1].COLORSPINOR( i, 2 ) );
			TIMESMINUSI( src->d[s1].c[c1].COLORSPINOR( i, 1 ), dest->d[s1].c[c1].COLORSPINOR( i, 3 ) );
		    }
	    break;
	case TUP:
	    for ( i = 0; i < 3; i++ )
		for ( s1 = 0; s1 < 4; s1++ )
		    for ( c1 = 0; c1 < 3; c1++ )
		    {
			TIMESPLUSONE( src->d[s1].c[c1].COLORSPINOR( i, 2 ), dest->d[s1].c[c1].COLORSPINOR( i, 0 ) );
			TIMESPLUSONE( src->d[s1].c[c1].COLORSPINOR( i, 3 ), dest->d[s1].c[c1].COLORSPINOR( i, 1 ) );
			TIMESPLUSONE( src->d[s1].c[c1].COLORSPINOR( i, 0 ), dest->d[s1].c[c1].COLORSPINOR( i, 2 ) );
			TIMESPLUSONE( src->d[s1].c[c1].COLORSPINOR( i, 1 ), dest->d[s1].c[c1].COLORSPINOR( i, 3 ) );
		    }
	    break;
	case GAMMAFIVE:
	    for ( i = 0; i < 3; i++ )
		for ( s1 = 0; s1 < 4; s1++ )
		    for ( c1 = 0; c1 < 3; c1++ )
		    {
			TIMESPLUSONE( src->d[s1].c[c1].COLORSPINOR( i, 0 ), dest->d[s1].c[c1].COLORSPINOR( i, 0 ) );
			TIMESPLUSONE( src->d[s1].c[c1].COLORSPINOR( i, 1 ), dest->d[s1].c[c1].COLORSPINOR( i, 1 ) );
			TIMESMINUSONE( src->d[s1].c[c1].COLORSPINOR( i, 2 ), dest->d[s1].c[c1].COLORSPINOR( i, 2 ) );
			TIMESMINUSONE( src->d[s1].c[c1].COLORSPINOR( i, 3 ), dest->d[s1].c[c1].COLORSPINOR( i, 3 ) );
		    }
	    break;
	default:
	    printf( "BAD CALL TO MULT_BY_GAMMA_RIGHT()\n" );
    }
}

void mult_by_gamma_r( spin_wilson_vector *src, spin_wilson_vector *dest, 
		     int dir)
{
register int i; /*color*/
register int s1;	/* row  spin indices*/

  switch(dir){
    case XUP:
	for(i=0;i<3;i++)for(s1=0;s1<4;s1++){
	    TIMESMINUSI( src->d[s1].COLORSPINOR(i,3),
		dest->d[s1].COLORSPINOR(i,0) );
	    TIMESMINUSI( src->d[s1].COLORSPINOR(i,2),
		dest->d[s1].COLORSPINOR(i,1) );
	    TIMESPLUSI(  src->d[s1].COLORSPINOR(i,1),
		dest->d[s1].COLORSPINOR(i,2) );
	    TIMESPLUSI(  src->d[s1].COLORSPINOR(i,0),
		dest->d[s1].COLORSPINOR(i,3) );
	}
	break;
    case YUP:
	for(i=0;i<3;i++)for(s1=0;s1<4;s1++){
	    TIMESMINUSONE( src->d[s1].COLORSPINOR(i,3),
		dest->d[s1].COLORSPINOR(i,0) );
	    TIMESPLUSONE(  src->d[s1].COLORSPINOR(i,2),
		dest->d[s1].COLORSPINOR(i,1) );
	    TIMESPLUSONE(  src->d[s1].COLORSPINOR(i,1),
		dest->d[s1].COLORSPINOR(i,2) );
	    TIMESMINUSONE( src->d[s1].COLORSPINOR(i,0),
		dest->d[s1].COLORSPINOR(i,3) );
	}
	break;
    case ZUP:
	for(i=0;i<3;i++)for(s1=0;s1<4;s1++){
	    TIMESMINUSI( src->d[s1].COLORSPINOR(i,2),
		dest->d[s1].COLORSPINOR(i,0) );
	    TIMESPLUSI(  src->d[s1].COLORSPINOR(i,3),
		dest->d[s1].COLORSPINOR(i,1) );
	    TIMESPLUSI(  src->d[s1].COLORSPINOR(i,0),
		dest->d[s1].COLORSPINOR(i,2) );
	    TIMESMINUSI( src->d[s1].COLORSPINOR(i,1),
		dest->d[s1].COLORSPINOR(i,3) );
	}
	break;
    case TUP:
	for(i=0;i<3;i++)for(s1=0;s1<4;s1++){
	    TIMESPLUSONE( src->d[s1].COLORSPINOR(i,2),
		dest->d[s1].COLORSPINOR(i,0) );
	    TIMESPLUSONE( src->d[s1].COLORSPINOR(i,3),
		dest->d[s1].COLORSPINOR(i,1) );
	    TIMESPLUSONE( src->d[s1].COLORSPINOR(i,0),
		dest->d[s1].COLORSPINOR(i,2) );
	    TIMESPLUSONE( src->d[s1].COLORSPINOR(i,1),
		dest->d[s1].COLORSPINOR(i,3) );
	}
	break;
    case GAMMAFIVE:
	for(i=0;i<3;i++)for(s1=0;s1<4;s1++){
	    TIMESPLUSONE(  src->d[s1].COLORSPINOR(i,0),
		dest->d[s1].COLORSPINOR(i,0) );
	    TIMESPLUSONE(  src->d[s1].COLORSPINOR(i,1),
		dest->d[s1].COLORSPINOR(i,1) );
	    TIMESMINUSONE( src->d[s1].COLORSPINOR(i,2),
		dest->d[s1].COLORSPINOR(i,2) );
	    TIMESMINUSONE( src->d[s1].COLORSPINOR(i,3),
		dest->d[s1].COLORSPINOR(i,3) );
	}
	break;
    default:
	printf("BAD CALL TO MULT_BY_GAMMA_RIGHT()\n");
  }
}
