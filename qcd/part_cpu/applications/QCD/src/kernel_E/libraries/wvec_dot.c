/******************  wvec_dot.c  (in su3.a) ****************************/
/* MIMD version 6 */
/*                                                                      *
 * complex wvec_dot(a,b) wilson_vector *a,*b;                           *
 * return dot product of two wilson_vectors                                     *
 */
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"

complex wvec_dot( wilson_vector * a, wilson_vector * b )
{
    complex temp1, temp2;
    register int i;
    temp1.real = temp1.imag = 0.0;
    for ( i = 0; i < 4; i++ )
    {
	CMULJ_( a->COLORSPINOR( 0, i ), b->COLORSPINOR( 0, i ), temp2 );
	CSUM( temp1, temp2 );
	CMULJ_( a->COLORSPINOR( 1, i ), b->COLORSPINOR( 1, i ), temp2 );
	CSUM( temp1, temp2 );
	CMULJ_( a->COLORSPINOR( 2, i ), b->COLORSPINOR( 2, i ), temp2 );
	CSUM( temp1, temp2 );
    }
    return ( temp1 );

}

complex hwvec_dot( half_wilson_vector * a, half_wilson_vector * b )
{
    complex temp1, temp2;
    register int i;
    temp1.real = temp1.imag = 0.0;
    for ( i = 0; i < 2; i++ )
    {
	CMULJ_( a->h[i].c[0], b->h[i].c[0], temp2 );
	CSUM( temp1, temp2 );
	CMULJ_( a->h[i].c[1], b->h[i].c[1], temp2 );
	CSUM( temp1, temp2 );
	CMULJ_( a->h[i].c[2], b->h[i].c[2], temp2 );
	CSUM( temp1, temp2 );
    }
    return ( temp1 );

}
