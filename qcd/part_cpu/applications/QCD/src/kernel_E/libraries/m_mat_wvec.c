/******************  m_mat_wvec.c  (in su3.a) ********************
 *									*
 *void mult_mat_wilson_vec(su3_matrix *mat, wilson_vector *src,*dest)	*
 *  multiply a Wilson vector by a matrix					*
 * dest  <-  mat*src							*
 */
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"

void mult_mat_wilson_vec( su3_matrix * mat, wilson_vector * src, wilson_vector * dest )
{
    register int i, k;
    register double t, ar, ai, br, bi, cr, ci;
    for ( k = 0; k < 4; k++ )
    {
	for ( i = 0; i < 3; i++ )
	{

	    ar = mat->ROWCOL( i, 0 ).real;
	    ai = mat->ROWCOL( i, 0 ).imag;
	    br = src->COLORSPINOR( 0, k ).real;
	    bi = src->COLORSPINOR( 0, k ).imag;
	    cr = ar * br;
	    t = ai * bi;
	    cr -= t;
	    ci = ar * bi;
	    t = ai * br;
	    ci += t;

	    ar = mat->ROWCOL( i, 1 ).real;
	    ai = mat->ROWCOL( i, 1 ).imag;
	    br = src->COLORSPINOR( 1, k ).real;
	    bi = src->COLORSPINOR( 1, k ).imag;
	    t = ar * br;
	    cr += t;
	    t = ai * bi;
	    cr -= t;
	    t = ar * bi;
	    ci += t;
	    t = ai * br;
	    ci += t;

	    ar = mat->ROWCOL( i, 2 ).real;
	    ai = mat->ROWCOL( i, 2 ).imag;
	    br = src->COLORSPINOR( 2, k ).real;
	    bi = src->COLORSPINOR( 2, k ).imag;
	    t = ar * br;
	    cr += t;
	    t = ai * bi;
	    cr -= t;
	    t = ar * bi;
	    ci += t;
	    t = ai * br;
	    ci += t;

	    dest->COLORSPINOR( i, k ).real = cr;
	    dest->COLORSPINOR( i, k ).imag = ci;
	}
    }
}
