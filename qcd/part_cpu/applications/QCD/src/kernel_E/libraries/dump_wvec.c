/****************  dump_wvec.c  (in su3.a) ***********************
*									*
*  void dump_wvec( wilson_vector *v )					*
*  Print out a Wilson vector 						*
*/
#include "../include/config.h"
#include <stdio.h>
#include "../include/complex.h"
#include "../include/su3.h"


void dump_wvec( wilson_vector * v )
{
    register int i, j;
    for ( i = 0; i < 4; i++ )
    {
	for ( j = 0; j < 3; j++ )
	    printf( "(%.8e,%.2e)\t", v->COLORSPINOR( j, i ).real, v->COLORSPINOR( j, i ).imag );
	printf( "\n" );
    }
    printf( "\n" );
}

void dump_half_wvec( half_wilson_vector * v )
{
    register int i, j;
    for ( i = 0; i < 2; i++ )
    {
	for ( j = 0; j < 3; j++ )
	    printf( "(%.8e,%.2e)\t", v->h[i].c[j].real, v->h[i].c[j].imag );
	printf( "\n" );
    }
    printf( "\n" );
}

void dump_wvec_32( float * v )
{
    register int i, j;
    for ( i = 0; i < 4; i++ )
    {
	for ( j = 0; j < 3; j++ )
#ifndef SPINORFIRST
            printf( "(%.8e,%.2e)\t", *(v+2*(4*j+i)), *(v+2*(4*j+i)+1) );
#endif
	printf( "\n" );
    }
    printf( "\n" );
}

void dump_half_wvec_32( float * v )
{
    register int i, j;
    for ( i = 0; i < 2; i++ )
    {
	for ( j = 0; j < 3; j++ )
            printf( "(%.8e,%.2e)\t", *(v+2*(3*i+j)), *(v+2*(3*i+j)+1) );
	printf( "\n" );
    }
    printf( "\n" );
}
