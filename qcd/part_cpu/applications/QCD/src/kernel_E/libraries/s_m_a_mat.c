/****************  s_m_a_mat.c  (in su3.a) ******************************
*									*
* void scalar_mult_add_su3_matrix_KE( su3_matrix *a, su3_matrix *b,	*
*	double s, su3_matrix *c)						*
* C <- A + s*B								*
*/
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"

/* c <- a + s*b, matrices */
void scalar_mult_add_su3_matrix_KE( su3_matrix * a, su3_matrix * b, double s, su3_matrix * c )
{
    register int i, j;
    for ( i = 0; i < 3; i++ )
	for ( j = 0; j < 3; j++ )
	{
	    c->ROWCOL( i, j ).real = a->ROWCOL( i, j ).real + s * b->ROWCOL( i, j ).real;
	    c->ROWCOL( i, j ).imag = a->ROWCOL( i, j ).imag + s * b->ROWCOL( i, j ).imag;
	}
}
