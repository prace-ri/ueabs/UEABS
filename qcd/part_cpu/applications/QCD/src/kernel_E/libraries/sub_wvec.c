/********************  sub_wvec.c  (in su3.a) ********************
*
*void sub_wilson_vector(wilson_vector *src1,*src2,*dest)
*  sub two Wilson vectors
* dest  <-  src1 + src2
*/
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"


void sub_wilson_vector( wilson_vector * src1, wilson_vector * src2, wilson_vector * dest )
{
    dest->COLORSPINOR( 0, 0 ).real = src1->COLORSPINOR( 0, 0 ).real - src2->COLORSPINOR( 0, 0 ).real;
    dest->COLORSPINOR( 0, 0 ).imag = src1->COLORSPINOR( 0, 0 ).imag - src2->COLORSPINOR( 0, 0 ).imag;
    dest->COLORSPINOR( 1, 0 ).real = src1->COLORSPINOR( 1, 0 ).real - src2->COLORSPINOR( 1, 0 ).real;
    dest->COLORSPINOR( 1, 0 ).imag = src1->COLORSPINOR( 1, 0 ).imag - src2->COLORSPINOR( 1, 0 ).imag;
    dest->COLORSPINOR( 2, 0 ).real = src1->COLORSPINOR( 2, 0 ).real - src2->COLORSPINOR( 2, 0 ).real;
    dest->COLORSPINOR( 2, 0 ).imag = src1->COLORSPINOR( 2, 0 ).imag - src2->COLORSPINOR( 2, 0 ).imag;

    dest->COLORSPINOR( 0, 1 ).real = src1->COLORSPINOR( 0, 1 ).real - src2->COLORSPINOR( 0, 1 ).real;
    dest->COLORSPINOR( 0, 1 ).imag = src1->COLORSPINOR( 0, 1 ).imag - src2->COLORSPINOR( 0, 1 ).imag;
    dest->COLORSPINOR( 1, 1 ).real = src1->COLORSPINOR( 1, 1 ).real - src2->COLORSPINOR( 1, 1 ).real;
    dest->COLORSPINOR( 1, 1 ).imag = src1->COLORSPINOR( 1, 1 ).imag - src2->COLORSPINOR( 1, 1 ).imag;
    dest->COLORSPINOR( 2, 1 ).real = src1->COLORSPINOR( 2, 1 ).real - src2->COLORSPINOR( 2, 1 ).real;
    dest->COLORSPINOR( 2, 1 ).imag = src1->COLORSPINOR( 2, 1 ).imag - src2->COLORSPINOR( 2, 1 ).imag;

    dest->COLORSPINOR( 0, 2 ).real = src1->COLORSPINOR( 0, 2 ).real - src2->COLORSPINOR( 0, 2 ).real;
    dest->COLORSPINOR( 0, 2 ).imag = src1->COLORSPINOR( 0, 2 ).imag - src2->COLORSPINOR( 0, 2 ).imag;
    dest->COLORSPINOR( 1, 2 ).real = src1->COLORSPINOR( 1, 2 ).real - src2->COLORSPINOR( 1, 2 ).real;
    dest->COLORSPINOR( 1, 2 ).imag = src1->COLORSPINOR( 1, 2 ).imag - src2->COLORSPINOR( 1, 2 ).imag;
    dest->COLORSPINOR( 2, 2 ).real = src1->COLORSPINOR( 2, 2 ).real - src2->COLORSPINOR( 2, 2 ).real;
    dest->COLORSPINOR( 2, 2 ).imag = src1->COLORSPINOR( 2, 2 ).imag - src2->COLORSPINOR( 2, 2 ).imag;

    dest->COLORSPINOR( 0, 3 ).real = src1->COLORSPINOR( 0, 3 ).real - src2->COLORSPINOR( 0, 3 ).real;
    dest->COLORSPINOR( 0, 3 ).imag = src1->COLORSPINOR( 0, 3 ).imag - src2->COLORSPINOR( 0, 3 ).imag;
    dest->COLORSPINOR( 1, 3 ).real = src1->COLORSPINOR( 1, 3 ).real - src2->COLORSPINOR( 1, 3 ).real;
    dest->COLORSPINOR( 1, 3 ).imag = src1->COLORSPINOR( 1, 3 ).imag - src2->COLORSPINOR( 1, 3 ).imag;
    dest->COLORSPINOR( 2, 3 ).real = src1->COLORSPINOR( 2, 3 ).real - src2->COLORSPINOR( 2, 3 ).real;
    dest->COLORSPINOR( 2, 3 ).imag = src1->COLORSPINOR( 2, 3 ).imag - src2->COLORSPINOR( 2, 3 ).imag;
}
