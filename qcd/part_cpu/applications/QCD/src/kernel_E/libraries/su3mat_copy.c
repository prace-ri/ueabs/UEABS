/*****************  su3mat_copy.c  (in su3.a) ***************************
*									*
* void su3mat_copy_KE( su3_matrix *a, su3_matrix *b )			*
* Copy an su3 matrix:  B <- A   						*
*/
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"

/* Copy a su3 matrix:  b <- a   */
void su3mat_copy_KE( su3_matrix * a, su3_matrix * b )
{
    register int i, j;
    for ( i = 0; i < 3; i++ )
	for ( j = 0; j < 3; j++ )
	{
	    b->ROWCOL( i, j ).real = a->ROWCOL( i, j ).real;
	    b->ROWCOL( i, j ).imag = a->ROWCOL( i, j ).imag;
	}
}
