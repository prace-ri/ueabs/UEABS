/*****************  grow4wvecs.c  (in su3.a) ****************************
*									*
*  If sum=0,								*
*  Grow and add four wilson_vectors 					*
*  If sum=1,								*
*  Grow and sum four wilson_vectors to another wilson_vector		*
* void grow_four_wvecs(a,b1,b2,b3,b4,sign,sum)				*
* wilson_vector *a; half_wilson_vector *b1,*b2,*b3,*b4;			*
* int sign,sum;								*
* A  <-  B1 + B2 + B3 + B4   or						*
* A  <-  A + B1 + B2 + B3 + B4						*
* B1 is expanded using gamma_x, B2 using gamma_y, etc. 			*
*/
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"
#include "../include/dirs.h"
void grow_add_four_wvecs( wilson_vector * a, half_wilson_vector * b1,
			  half_wilson_vector * b2, half_wilson_vector * b3, half_wilson_vector * b4, int sign, int sum )
{
    int i;
    if( sum == 0 )
    {
	/* wp_grow( b1,a,XUP,sign); */

	/* case XUP: */
	if( sign == PLUS )
	{
	    for ( i = 0; i < 3; i++ )
	    {
		a->COLORSPINOR( i, 0 ) = b1->h[0].c[i];
		a->COLORSPINOR( i, 1 ) = b1->h[1].c[i];
		TIMESMINUSI( b1->h[0].c[i], a->COLORSPINOR( i, 3 ) );
		TIMESMINUSI( b1->h[1].c[i], a->COLORSPINOR( i, 2 ) );
	    }
	}
	else
	{
	    /* case XDOWN: */
	    for ( i = 0; i < 3; i++ )
	    {
		a->COLORSPINOR( i, 0 ) = b1->h[0].c[i];
		a->COLORSPINOR( i, 1 ) = b1->h[1].c[i];
		TIMESPLUSI( b1->h[0].c[i], a->COLORSPINOR( i, 3 ) );
		TIMESPLUSI( b1->h[1].c[i], a->COLORSPINOR( i, 2 ) );
	    }
	}
    }
    else
    {
	/* wp_grow_add( b1,a,XUP,sign); */

	/* case XUP: */
	if( sign == PLUS )
	{
	    for ( i = 0; i < 3; i++ )
	    {
		CSUM( a->COLORSPINOR( i, 0 ), b1->h[0].c[i] );
		CSUM( a->COLORSPINOR( i, 1 ), b1->h[1].c[i] );
		CSUM_TMI( a->COLORSPINOR( i, 2 ), b1->h[1].c[i] );
		CSUM_TMI( a->COLORSPINOR( i, 3 ), b1->h[0].c[i] );
	    }
	}
	else
	{
	    /* case XDOWN: */
	    for ( i = 0; i < 3; i++ )
	    {
		CSUM( a->COLORSPINOR( i, 0 ), b1->h[0].c[i] );
		CSUM( a->COLORSPINOR( i, 1 ), b1->h[1].c[i] );
		CSUM_TPI( a->COLORSPINOR( i, 2 ), b1->h[1].c[i] );
		CSUM_TPI( a->COLORSPINOR( i, 3 ), b1->h[0].c[i] );
	    }
	}
    }

    /* wp_grow_add( b2,a,YUP,sign); */

    if( sign == PLUS )
    {
	/*  case YUP: */
	for ( i = 0; i < 3; i++ )
	{
	    CSUM( a->COLORSPINOR( i, 0 ), b2->h[0].c[i] );
	    CSUM( a->COLORSPINOR( i, 1 ), b2->h[1].c[i] );
	    CSUM( a->COLORSPINOR( i, 2 ), b2->h[1].c[i] );
	    CSUB( a->COLORSPINOR( i, 3 ), b2->h[0].c[i], a->COLORSPINOR( i, 3 ) );
	}
    }
    else
    {
	/*  case YDOWN: */
	for ( i = 0; i < 3; i++ )
	{
	    CSUM( a->COLORSPINOR( i, 0 ), b2->h[0].c[i] );
	    CSUM( a->COLORSPINOR( i, 1 ), b2->h[1].c[i] );
	    CSUB( a->COLORSPINOR( i, 2 ), b2->h[1].c[i], a->COLORSPINOR( i, 2 ) );
	    CSUM( a->COLORSPINOR( i, 3 ), b2->h[0].c[i] );
	}
    }

    /*  wp_grow_add( b3,a,ZUP,sign); */

    if( sign == PLUS )
    {
	/*  case ZUP: */
	for ( i = 0; i < 3; i++ )
	{
	    CSUM( a->COLORSPINOR( i, 0 ), b3->h[0].c[i] );
	    CSUM( a->COLORSPINOR( i, 1 ), b3->h[1].c[i] );
	    CSUM_TMI( a->COLORSPINOR( i, 2 ), b3->h[0].c[i] );
	    CSUM_TPI( a->COLORSPINOR( i, 3 ), b3->h[1].c[i] );
	}
    }
    else
    {
	/*  case ZDOWN: */
	for ( i = 0; i < 3; i++ )
	{
	    CSUM( a->COLORSPINOR( i, 0 ), b3->h[0].c[i] );
	    CSUM( a->COLORSPINOR( i, 1 ), b3->h[1].c[i] );
	    CSUM_TPI( a->COLORSPINOR( i, 2 ), b3->h[0].c[i] );
	    CSUM_TMI( a->COLORSPINOR( i, 3 ), b3->h[1].c[i] );
	}
    }

    /*  wp_grow_add( b4,a,TUP,sign); */

    if( sign == PLUS )
    {
	/*  case TUP: */
	for ( i = 0; i < 3; i++ )
	{
	    CSUM( a->COLORSPINOR( i, 0 ), b4->h[0].c[i] );
	    CSUM( a->COLORSPINOR( i, 1 ), b4->h[1].c[i] );
	    CSUM( a->COLORSPINOR( i, 2 ), b4->h[0].c[i] );
	    CSUM( a->COLORSPINOR( i, 3 ), b4->h[1].c[i] );
	}
    }
    else
    {
	/*  case TDOWN: */
	for ( i = 0; i < 3; i++ )
	{
	    CSUM( a->COLORSPINOR( i, 0 ), b4->h[0].c[i] );
	    CSUM( a->COLORSPINOR( i, 1 ), b4->h[1].c[i] );
	    CSUB( a->COLORSPINOR( i, 2 ), b4->h[0].c[i], a->COLORSPINOR( i, 2 ) );
	    CSUB( a->COLORSPINOR( i, 3 ), b4->h[1].c[i], a->COLORSPINOR( i, 3 ) );
	}
    }


}

void grow_add_four_wvecs_hch( wilson_vector * a, half_wilson_vector * b1,
			      half_wilson_vector * b2, half_wilson_vector * b3, half_wilson_vector * b4, int sign, int sum )
{

    if( sum == 0 )
    {
	wp_grow_hch( b1, a, XUP, sign );

    }
    else
    {
	wp_grow_add_hch( b1, a, XUP, sign );

    }

    wp_grow_add_hch( b2, a, YUP, sign );

    wp_grow_add_hch( b3, a, ZUP, sign );

    wp_grow_add_hch( b4, a, TUP, sign );


}
