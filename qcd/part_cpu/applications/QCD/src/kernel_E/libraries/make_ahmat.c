/*****************  make_ahmat.c  (in su3.a) ****************************
*									*
* void make_anti_hermitian_KE( su3_matrix *m3, anti_hermitmat *ah3)	*
* take the traceless and anti_hermitian part of an su3 matrix 		*
* and compress it 							*
*/
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"

#ifndef FAST
void make_anti_hermitian_KE( su3_matrix * m3, anti_hermitmat * ah3 )
{
    double temp;

    temp = ( m3->ROWCOL( 0, 0 ).imag + m3->ROWCOL( 1, 1 ).imag + m3->ROWCOL( 2, 2 ).imag ) / 3.;
    ah3->m00im = m3->ROWCOL( 0, 0 ).imag - temp;
    ah3->m11im = m3->ROWCOL( 1, 1 ).imag - temp;
    ah3->m22im = m3->ROWCOL( 2, 2 ).imag - temp;
    ah3->m01.real = ( m3->ROWCOL( 0, 1 ).real - m3->ROWCOL( 1, 0 ).real ) * 0.5;
    ah3->m02.real = ( m3->ROWCOL( 0, 2 ).real - m3->ROWCOL( 2, 0 ).real ) * 0.5;
    ah3->m12.real = ( m3->ROWCOL( 1, 2 ).real - m3->ROWCOL( 2, 1 ).real ) * 0.5;
    ah3->m01.imag = ( m3->ROWCOL( 0, 1 ).imag + m3->ROWCOL( 1, 0 ).imag ) * 0.5;
    ah3->m02.imag = ( m3->ROWCOL( 0, 2 ).imag + m3->ROWCOL( 2, 0 ).imag ) * 0.5;
    ah3->m12.imag = ( m3->ROWCOL( 1, 2 ).imag + m3->ROWCOL( 2, 1 ).imag ) * 0.5;

}				/* make_anti_hermitian */

#else
void make_anti_hermitian_KE( su3_matrix * m3, anti_hermitmat * ah3 )
{
    double temp, temp2;

    temp = ( m3->ROWCOL( 0, 0 ).imag + m3->ROWCOL( 1, 1 ).imag );
    temp2 = temp + m3->ROWCOL( 2, 2 ).imag;
    temp = temp2 / 3.;
    ah3->m00im = m3->ROWCOL( 0, 0 ).imag - temp;
    ah3->m11im = m3->ROWCOL( 1, 1 ).imag - temp;
    ah3->m22im = m3->ROWCOL( 2, 2 ).imag - temp;
    temp = m3->ROWCOL( 0, 1 ).real - m3->ROWCOL( 1, 0 ).real;
    ah3->m01.real = temp * 0.5;
    temp = m3->ROWCOL( 0, 2 ).real - m3->ROWCOL( 2, 0 ).real;
    ah3->m02.real = temp * 0.5;
    temp = m3->ROWCOL( 1, 2 ).real - m3->ROWCOL( 2, 1 ).real;
    ah3->m12.real = temp * 0.5;
    temp = m3->ROWCOL( 0, 1 ).imag + m3->ROWCOL( 1, 0 ).imag;
    ah3->m01.imag = temp * 0.5;
    temp = m3->ROWCOL( 0, 2 ).imag + m3->ROWCOL( 2, 0 ).imag;
    ah3->m02.imag = temp * 0.5;
    temp = m3->ROWCOL( 1, 2 ).imag + m3->ROWCOL( 2, 1 ).imag;
    ah3->m12.imag = temp * 0.5;

}				/* make_anti_hermitian */
#endif /*end ifdef FAST */

void make_traceless( su3_matrix * m3, su3_matrix * m4 )
{
    double retr, imtr;

    retr = m3->ROWCOL( 0, 0 ).real + m3->ROWCOL( 1, 1 ).real + m3->ROWCOL( 2, 2 ).real;
    imtr = m3->ROWCOL( 0, 0 ).imag + m3->ROWCOL( 1, 1 ).imag + m3->ROWCOL( 2, 2 ).imag;

    m4->ROWCOL( 0, 1 ) = m3->ROWCOL( 0, 1 );
    m4->ROWCOL( 0, 2 ) = m3->ROWCOL( 0, 2 );
    m4->ROWCOL( 1, 0 ) = m3->ROWCOL( 1, 0 );
    m4->ROWCOL( 1, 2 ) = m3->ROWCOL( 1, 2 );
    m4->ROWCOL( 2, 0 ) = m3->ROWCOL( 2, 0 );
    m4->ROWCOL( 2, 1 ) = m3->ROWCOL( 2, 1 );

    m4->ROWCOL( 0, 0 ).real = m3->ROWCOL( 0, 0 ).real - retr / 3.;
    m4->ROWCOL( 0, 0 ).imag = m3->ROWCOL( 0, 0 ).imag - imtr / 3.;
    m4->ROWCOL( 1, 1 ).real = m3->ROWCOL( 1, 1 ).real - retr / 3.;
    m4->ROWCOL( 1, 1 ).imag = m3->ROWCOL( 1, 1 ).imag - imtr / 3.;
    m4->ROWCOL( 2, 2 ).real = m3->ROWCOL( 2, 2 ).real - retr / 3.;
    m4->ROWCOL( 2, 2 ).imag = m3->ROWCOL( 2, 2 ).imag - imtr / 3.;
}

void scalar_mult_ahm( anti_hermitmat * a, double s, anti_hermitmat * b )
{
    b->m00im = s * ( a->m00im );
    b->m11im = s * ( a->m11im );
    b->m22im = s * ( a->m22im );
    b->m01.real = s * ( a->m01.real );
    b->m02.real = s * ( a->m02.real );
    b->m12.real = s * ( a->m12.real );
    b->m01.imag = s * ( a->m01.imag );
    b->m02.imag = s * ( a->m02.imag );
    b->m12.imag = s * ( a->m12.imag );
}

void clear_ahm( anti_hermitmat * b )
{
    b->m00im = 0.0;
    b->m11im = 0.0;
    b->m22im = 0.0;
    b->m01.real = 0.0;
    b->m02.real = 0.0;
    b->m12.real = 0.0;
    b->m01.imag = 0.0;
    b->m02.imag = 0.0;
    b->m12.imag = 0.0;
}
