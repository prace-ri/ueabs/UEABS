 /********************  msq_wvec.c  (in su3.a) ********************
*
*double msq_wvec(wilson_vector *vec)
*  squared magnitude of a Wilson vector
* 
*/
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"

double magsq_wvec( wilson_vector * vec )
{
    register double ar, ai, sum;

    ar = vec->COLORSPINOR( 0, 0 ).real;
    ai = vec->COLORSPINOR( 0, 0 ).imag;
    sum = ar * ar + ai * ai;
    ar = vec->COLORSPINOR( 1, 0 ).real;
    ai = vec->COLORSPINOR( 1, 0 ).imag;
    sum += ar * ar + ai * ai;
    ar = vec->COLORSPINOR( 2, 0 ).real;
    ai = vec->COLORSPINOR( 2, 0 ).imag;
    sum += ar * ar + ai * ai;

    ar = vec->COLORSPINOR( 0, 1 ).real;
    ai = vec->COLORSPINOR( 0, 1 ).imag;
    sum += ar * ar + ai * ai;
    ar = vec->COLORSPINOR( 1, 1 ).real;
    ai = vec->COLORSPINOR( 1, 1 ).imag;
    sum += ar * ar + ai * ai;
    ar = vec->COLORSPINOR( 2, 1 ).real;
    ai = vec->COLORSPINOR( 2, 1 ).imag;
    sum += ar * ar + ai * ai;

    ar = vec->COLORSPINOR( 0, 2 ).real;
    ai = vec->COLORSPINOR( 0, 2 ).imag;
    sum += ar * ar + ai * ai;
    ar = vec->COLORSPINOR( 1, 2 ).real;
    ai = vec->COLORSPINOR( 1, 2 ).imag;
    sum += ar * ar + ai * ai;
    ar = vec->COLORSPINOR( 2, 2 ).real;
    ai = vec->COLORSPINOR( 2, 2 ).imag;
    sum += ar * ar + ai * ai;

    ar = vec->COLORSPINOR( 0, 3 ).real;
    ai = vec->COLORSPINOR( 0, 3 ).imag;
    sum += ar * ar + ai * ai;
    ar = vec->COLORSPINOR( 1, 3 ).real;
    ai = vec->COLORSPINOR( 1, 3 ).imag;
    sum += ar * ar + ai * ai;
    ar = vec->COLORSPINOR( 2, 3 ).real;
    ai = vec->COLORSPINOR( 2, 3 ).imag;
    sum += ar * ar + ai * ai;

    return ( ( double ) sum );
}

double magsq_hwvec( half_wilson_vector * vec )
{

#ifdef NATIVEDOUBLE
    register double ar, ai, sum;
#else
    register double ar, ai, sum;
#endif

    ar = vec->h[0].c[0].real;
    ai = vec->h[0].c[0].imag;
    sum = ar * ar + ai * ai;
    ar = vec->h[0].c[1].real;
    ai = vec->h[0].c[1].imag;
    sum += ar * ar + ai * ai;
    ar = vec->h[0].c[2].real;
    ai = vec->h[0].c[2].imag;
    sum += ar * ar + ai * ai;

    ar = vec->h[1].c[0].real;
    ai = vec->h[1].c[0].imag;
    sum += ar * ar + ai * ai;
    ar = vec->h[1].c[1].real;
    ai = vec->h[1].c[1].imag;
    sum += ar * ar + ai * ai;
    ar = vec->h[1].c[2].real;
    ai = vec->h[1].c[2].imag;
    sum += ar * ar + ai * ai;

    return ( ( double ) sum );
}
