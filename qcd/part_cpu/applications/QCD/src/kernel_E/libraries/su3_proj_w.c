/***************** su3_proj_w.c  (in su3.a) ****************************/
/* MIMD version 6 */
/*                                                                      *
 * void su3_projector_w( wilson_vector *a, wilson_vector *b, su3_matrix *c )
 * C  <- sum over spins of outer product of A.d[i] and B.d[i]           *
 *  C_ij = sum( A_i * B_adjoint_j )                                     *
 */
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"

void su3_projector_w( wilson_vector * a, wilson_vector * b, su3_matrix * c )
{
    register int i, j, k;
    register complex cc;
    for ( i = 0; i < 3; i++ )
	for ( j = 0; j < 3; j++ )
	{
	    c->ROWCOL( i, j ) = cmplx_KE( 0.0, 0.0 );
	    for ( k = 0; k < 4; k++ )
	    {
		CMUL_J( a->COLORSPINOR( i, k ), b->COLORSPINOR( j, k ), cc );
		CSUM( c->ROWCOL( i, j ), cc );
	    }
	}
}
