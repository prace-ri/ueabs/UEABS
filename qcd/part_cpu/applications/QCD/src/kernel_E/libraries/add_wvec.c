/********************  add_wvec.c  (in su3.a) ********************
*
*void add_wilson_vector(wilson_vector *src1,*src2,*dest)
*  add two Wilson vectors
* dest  <-  src1 + src2
*/
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"

void add_wilson_vector( wilson_vector * src1, wilson_vector * src2, wilson_vector * dest )
{
    register int i;
    for ( i = 0; i < 3; i++ )
    {
	dest->COLORSPINOR( i, 0 ).real = src1->COLORSPINOR( i, 0 ).real + ( src2->COLORSPINOR( i, 0 ).real );
	dest->COLORSPINOR( i, 1 ).real = src1->COLORSPINOR( i, 1 ).real + ( src2->COLORSPINOR( i, 1 ).real );
	dest->COLORSPINOR( i, 2 ).real = src1->COLORSPINOR( i, 2 ).real + ( src2->COLORSPINOR( i, 2 ).real );
	dest->COLORSPINOR( i, 3 ).real = src1->COLORSPINOR( i, 3 ).real + ( src2->COLORSPINOR( i, 3 ).real );

	dest->COLORSPINOR( i, 0 ).imag = src1->COLORSPINOR( i, 0 ).imag + ( src2->COLORSPINOR( i, 0 ).imag );
	dest->COLORSPINOR( i, 1 ).imag = src1->COLORSPINOR( i, 1 ).imag + ( src2->COLORSPINOR( i, 1 ).imag );
	dest->COLORSPINOR( i, 2 ).imag = src1->COLORSPINOR( i, 2 ).imag + ( src2->COLORSPINOR( i, 2 ).imag );
	dest->COLORSPINOR( i, 3 ).imag = src1->COLORSPINOR( i, 3 ).imag + ( src2->COLORSPINOR( i, 3 ).imag );
    }
}
