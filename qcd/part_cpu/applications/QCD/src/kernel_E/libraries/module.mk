MYFILES:=       \
	cadd.c cdiv.c ce_itheta.c cexp.c clog.c cmplx.c cmul.c \
	conjg.c csqrt.c csub.c \
	addmat.c addvec.c cmp_ahmat.c cs_m_a_vec.c cs_m_a_mat.c cs_m_s_vec.c \
	cs_m_vec.c det_su3.c clear_mat.c dumpmat.c dumpvec.c clearvec.c \
	m_amatvec_s.c m_amatvec.c m_amatvec_ns.c \
	m_matvec.c m_matvec_ns.c m_matvec_s.c \
	make_ahmat.c rand_ahmat.c realtr.c complextr.c \
	s_m_a_mat.c s_m_a_vec.c s_m_s_mat.c s_m_s_vec.c s_m_sum_vec.c \
	s_m_vec.c s_m_mat.c cs_m_mat.c cs_m_s_mat.c \
	su3_adjoint.c su3_dot.c su3_rdot.c su3_proj.c su3mat_copy.c \
	su3vec_copy.c \
	submat.c subvec.c trace_su3.c uncmp_ahmat.c \
	msq_su3vec.c sub4vecs.c m_amv_4dir.c m_amv_4vec.c \
	m_mv_s_4dir.c flush_to_zero.c \
	mb_gamma_l.c mb_gamma_r.c \
	gaussrand.c byterevn.c \
	m_su2_mat_vec_a.c m_su2_mat_vec_n.c r_su2_hit_a.c l_su2_hit_n.c \
	wp_shrink.c wp_grow.c wp_grow_a.c dump_wvec.c clear_wvec.c \
	su3_proj_w.c copy_wvec.c add_wvec.c sub_wvec.c s_m_wvec.c \
	s_m_hwvec.c msq_wvec.c wvec_dot.c wvec2_dot.c wvec_rdot.c \
	s_m_a_wvec.c s_m_a_g5_wvec.c \
	cs_m_wvec.c cs_m_a_wvec.c \
	m_mat_wvec.c m_amat_wvec.c \
	m_mat_hwvec.c m_amat_hwvec.c \
	grow4wvecs.c wp_shrink4.c s_g5_m_a_wvec.c g5_m_wvec.c

SOURCE+= $(patsubst %,libraries/%,$(MYFILES))

