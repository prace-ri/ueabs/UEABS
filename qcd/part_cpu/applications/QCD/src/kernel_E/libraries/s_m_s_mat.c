/****************  s_m_s_mat.c  (in su3.a) ******************************
*									*
* void scalar_mult_sub_su3_matrix_KE( su3_matrix *a, su3_matrix *b,	*
*	double s, su3_matrix *c)						*
* C <- A - s*B,   A,B and C matrices 					*
*/
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"

/* c <- a - s*b, matrices */
void scalar_mult_sub_su3_matrix_KE( su3_matrix * a, su3_matrix * b, double s, su3_matrix * c )
{

#ifndef NATIVEDOUBLE
    register int i, j;
    for ( i = 0; i < 3; i++ )
	for ( j = 0; j < 3; j++ )
	{
	    c->ROWCOL( i, j ).real = a->ROWCOL( i, j ).real - s * b->ROWCOL( i, j ).real;
	    c->ROWCOL( i, j ).imag = a->ROWCOL( i, j ).imag - s * b->ROWCOL( i, j ).imag;
	}

#else /* RS6000 version */

    register double ss;

    ss = s;

    c->ROWCOL( 0, 0 ).real = a->ROWCOL( 0, 0 ).real - ss * b->ROWCOL( 0, 0 ).real;
    c->ROWCOL( 0, 0 ).imag = a->ROWCOL( 0, 0 ).imag - ss * b->ROWCOL( 0, 0 ).imag;
    c->ROWCOL( 0, 1 ).real = a->ROWCOL( 0, 1 ).real - ss * b->ROWCOL( 0, 1 ).real;
    c->ROWCOL( 0, 1 ).imag = a->ROWCOL( 0, 1 ).imag - ss * b->ROWCOL( 0, 1 ).imag;
    c->ROWCOL( 0, 2 ).real = a->ROWCOL( 0, 2 ).real - ss * b->ROWCOL( 0, 2 ).real;
    c->ROWCOL( 0, 2 ).imag = a->ROWCOL( 0, 2 ).imag - ss * b->ROWCOL( 0, 2 ).imag;

    c->ROWCOL( 1, 0 ).real = a->ROWCOL( 1, 0 ).real - ss * b->ROWCOL( 1, 0 ).real;
    c->ROWCOL( 1, 0 ).imag = a->ROWCOL( 1, 0 ).imag - ss * b->ROWCOL( 1, 0 ).imag;
    c->ROWCOL( 1, 1 ).real = a->ROWCOL( 1, 1 ).real - ss * b->ROWCOL( 1, 1 ).real;
    c->ROWCOL( 1, 1 ).imag = a->ROWCOL( 1, 1 ).imag - ss * b->ROWCOL( 1, 1 ).imag;
    c->ROWCOL( 1, 2 ).real = a->ROWCOL( 1, 2 ).real - ss * b->ROWCOL( 1, 2 ).real;
    c->ROWCOL( 1, 2 ).imag = a->ROWCOL( 1, 2 ).imag - ss * b->ROWCOL( 1, 2 ).imag;

    c->ROWCOL( 2, 0 ).real = a->ROWCOL( 2, 0 ).real - ss * b->ROWCOL( 2, 0 ).real;
    c->ROWCOL( 2, 0 ).imag = a->ROWCOL( 2, 0 ).imag - ss * b->ROWCOL( 2, 0 ).imag;
    c->ROWCOL( 2, 1 ).real = a->ROWCOL( 2, 1 ).real - ss * b->ROWCOL( 2, 1 ).real;
    c->ROWCOL( 2, 1 ).imag = a->ROWCOL( 2, 1 ).imag - ss * b->ROWCOL( 2, 1 ).imag;
    c->ROWCOL( 2, 2 ).real = a->ROWCOL( 2, 2 ).real - ss * b->ROWCOL( 2, 2 ).real;
    c->ROWCOL( 2, 2 ).imag = a->ROWCOL( 2, 2 ).imag - ss * b->ROWCOL( 2, 2 ).imag;

#endif
}
