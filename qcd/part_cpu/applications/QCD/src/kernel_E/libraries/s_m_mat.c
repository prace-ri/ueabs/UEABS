/******************  s_m_mat.c  (in su3.a) ******************************
*									*
* void scalar_mult_su3_matrix_KE( su3_matrix *a, double s, su3_matrix *b)	*
* B <- s*A								*
*/
#include "../include/config.h"
#include "../include/complex.h"
#include "../include/su3.h"

/* b <- s*a, matrices */
void scalar_mult_su3_matrix_KE( su3_matrix * a, double s, su3_matrix * b )
{
    register int i, j;
    for ( i = 0; i < 3; i++ )
	for ( j = 0; j < 3; j++ )
	{
	    b->ROWCOL( i, j ).real = s * a->ROWCOL( i, j ).real;
	    b->ROWCOL( i, j ).imag = s * a->ROWCOL( i, j ).imag;
	}
}
