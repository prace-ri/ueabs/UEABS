/************************ control.c ******************************/
/* MIMD version 6 */
/* Main procedure for SU3 with dynamical Wilson fermions */

#define CONTROL

#include "./include/includes.h"

/* JuBE */
void kernel_e( )
{
  
/*   JuBE */
  int jube_kernel_number = 4;
  jube_kernel_init(&jube_kernel_number);

    debug();
/*     JuBE */
/*     MPI_Init(&argc, &argv); */
    debug();
/*  JuBE: no args needed */
    initialize_machine_KE();
    debug();
    g_sync_KE(  );

/*  JuBE: std para file kernel_E.input */
    setup_KE( );

    /* Measure performance */
    {
	wilson_vector *latwvec_a, *latwvec_b;
	int i,j; site *s;
	float *vec_a;
	int iters;

	MEMALIGN(latwvec_a,wilson_vector, sites_on_node);
	MEMALIGN(latwvec_b,wilson_vector, sites_on_node);
	MEMALIGN(vec_a,float,24*sites_on_node);

	/* 64 bit vectors*/
	unit_wvec(latwvec_a,EVENANDODD);
	clear_latwvec(latwvec_b,EVENANDODD);
	
	/* 32 bit vectors*/
	FORALLSITES( i, s)
	{
	    for (j=0;j<24;j++)
	    {
		if (j%2){
		    vec_a[24*i+j]=0.0;
		}else{
		    vec_a[24*i+j]=1.0;
		}
	    }
	}
	
	int congrad_32(float *wvec, double myrsqmin, int maxniter, double shift);
	int congrad_64( wilson_vector *src, wilson_vector *dest, int maxniter, double myrsqmin, double shift);
	
/*     JuBE: */
	jube_kernel_run();

	iters = congrad_32(vec_a, 1e-8, max_cg_iters, 0.0);
	iters = congrad_64(latwvec_a, latwvec_b, max_cg_iters, 1e-16, 0.0);
	
/*     JuBE: */
	jube_kernel_finalize();

	FREE(latwvec_a,wilson_vector, sites_on_node);
	FREE(latwvec_b,wilson_vector, sites_on_node);
	FREE(vec_a,float,24*even_sites_on_node);
    }

/*     JuBE: */
	jube_kernel_end();


/*     return 0; */
}
