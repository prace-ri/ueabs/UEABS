replaceInFile ()
{
    sed "s/${1}(/${2}(/g" $3 > tmp.dat; cp tmp.dat $3
}

case $1 in
    "doublicates") echo "generate doublicate list";
		   echo "parse files:\n`ls *.a`";
		   nm -X64 *.a | grep ' T ' | cut -f 1 -d ' ' | sort > all.sort.dat 
		   nm -X64 *.a | grep ' T ' | cut -f 1 -d ' ' | sort | sort -u > all.sort.unique.dat  
		   diff all.sort.dat all.sort.unique.dat | grep '<' | cut -f 2- -d '.'> doublicates.dat
		   echo "found `wc -l doublicates.dat` doublicates" ;;
		   
    "find") echo "looking for $2";
            grep -r $2 `find . -name '*.[c|h|f|f90|F90]'` | cut -f 2- -d '<' > find.dat;
	    cat find.dat;;
    
    "replace") echo "replace $2 by $3 in $4";
               replaceInFile $2 $3 $4;;
    
    "replaceAllFiles") echo "replace all $2 by $3";
                       grep -r $2 `find . -name '*.[c|h|f|f90|F90]'` | cut -f 2- -d '<'  | cut -f 1 -d ':' | sort -u > find.dat;
		       for i in `cat find.dat`
		       do 
			 echo "replacing $2 by $3 in $i";
			 replaceInFile $2 $3 $i
		       done
		       ;;

    "replaceAll") 
	echo "replace all doublicates in this directory, using doublicate list $2 and postfix $3";
	for dn in `cat $2`
	do
	  echo "replace $dn"
	  grep -r $dn `find . -name '*.[c|h|f|f90|F90]'` | cut -f 2- -d '<'  | cut -f 1 -d ':' | sort -u > find.dat;
	  for fn in `cat find.dat`
	    do 
	    echo "replacing $dn by ${dn}_$3 in $fn";
	    replaceInFile ${dn} ${dn}_${3} ${fn}
	  done
	done
	;;
    
esac