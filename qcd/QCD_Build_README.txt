Description and Building of the QCD Benchmark
=============================================

Description
===========
The QCD benchmark is, unlike the other benchmarks in the PRACE
application benchmark suite, not a full application but a set of 7
kernels which are representative of some of the most compute-intensive
parts of QCD calculations for different architectures.

The QCD benchmark suite consists of three different main application,
part_cpu is based on 5 kernels of major QCD application and different
major codes. The applications contains in part_1 and part_2 are 
suitable to run benchmarks on HPC machines equiped with accelerators
like Nvidia GPUs or Intel Xeon Phi processors.

In the following building instruction of the part_cpu part
are descriped, see for describtion of the other parts in the different
subdirectories.

============================================

PART_CPU:

Test Cases
----------
Each of the 5 kernels has one test case to be used for Tier-0 and
Tier-1: 

Kernel A is derived from BQCD (Berlin Quantum ChromoDynamics program),
a hybrid Monte-Carlo code that simulates Quantum Chromodynamics with
dynamical standard Wilson fermions. The computations take place on a
four-dimensional regular grid with periodic boundary conditions. The
kernel is a standard conjugate gradient solver with even/odd
pre-conditioning. Lattice size is 32^2 x 64^2. 

Kernel B is derived from SU3_AHiggs, a lattice quantum chromodynamics
(QCD) code intended for computing the conditions of the Early
Universe. Instead of "full QCD", the code applies an effective field
theory, which is valid at high temperatures. In the effective theory,
the lattice is 3D. Lattice size is 256^3. 

Kernel C Lattice size is 84. Note that Kernel C can only be run in a
weak scaling mode, where each CPU stores the same local lattice size,
regardless of the number of CPUs. Ideal scaling for this kernel
therefore corresponds to constant execution time, and performance per
peak TFlop/s is simply the reciprocal of the execution time.  

Kernel D consists of the core matrix-vector multiplication routine for
standard Wilson fermions. The lattice size is 64^4 . 

Kernel E consists of a full conjugate gradient solution using Wilson
fermions. Lattice size is 64^4. 

Building the QCD Benchmark in the JuBE Framework
================================================

The QCD benchmark is integrated in the JuBE Benchmarking Environment
(www.fz-juelich.de/jsc/jube). 
JuBE also includes all steps to build the application. 

Unpack the QCD_Source_TestCaseA.tar.gz into a directory of your
choice. 

After unpacking the Benchmark the following directory structure is available:
     PABS/
     applications/
     bench/
     doc/
     platform/
     skel/
     LICENCE

The applications/ subdirectory contains the QCD benchmark
applications. 
The bench/ subdirectory contains the benchmark environment scripts. 
The doc/ subdirectory contains the overall documentation of the
framework and a tutorial. 
The platform/ subdirectory holds the platform definitions as well as
job submission script templates for each defined platform. 
The skel/ subdirectory contains templates for analysis patterns for
text output of different measurement tools.  

Configuration
-------------

Definition files are already prepared for many platforms. If you are
running on a defined platform just skip this part and go forward to
QCD_Run_README.txt ("Execution"). 

The platform
------------

A platform is defined through a set of variables in the platform.xml
file, which can be found in the platform/ directory. To create a new
platform entry, copy an existing platform description and modify it to
fit your local setup. The variables defined here will be used by the
individual applications in the later process. Best practice for the
platform nomenclature would be: <vendor>-<system type>-<system
name|site>. Additionally, you have to create a template batch
submission script, which should be placed in a subdirectory of the
platform/ directory of the same name as the platform itself. Although
this nomenclature is not required by the benchmarking environment, it
helps keeping track of you templates, and minimises the amount of
adaptation necessary for the individual application configurations.

The applications
----------------

Once a platform is defined, each individual application that should be
used in the benchmark (in this case the QCD application) needs to be
configured for this platform. In order to configure an individual
application, copy an existing top-level configuration file
(e.g. prace-scaling-juqueen.xml) to the file prace-<yourplatform>.xml. 
Then open an editor of your choice, to adapt the file to your
needs. Change the settings of the platform parameter to the name of
your defined platform. The platform name can then be referenced
throughout the benchmarking environment by the $platform variable.  
Do the same for compile.xml, execute.xml, analyse.xml.
You can find a step by step tutorial also in doc/JuBETutorial.pdf.

The compilation is part of the run of the application. Please continue
with the QCD_Run_README.txt to finalize the build and to run the
benchmark.  
