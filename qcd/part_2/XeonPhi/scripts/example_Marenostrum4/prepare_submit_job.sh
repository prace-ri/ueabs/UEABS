##
##
##
##
##

time=$1
Node=$2
n=$3
g=$4
openmp=$5
cpuptask=$6
perm=$7
scr=$8

sed 's/#NODES#/'${Node}'/g' submit_job.sh.template > test
mv test submit_job.temp
sed 's/#NTASK#/'${n}'/g' submit_job.temp > test
mv test submit_job.temp
sed 's/#NTASKPERNODE#/'${g}'/g' submit_job.temp > test
mv test submit_job.temp
sed 's/#OPENMP#/'${openmp}'/g' submit_job.temp > test
mv test submit_job.temp
sed 's/#CPUSPERTASK#/'${cpuptask}'/g' submit_job.temp > test
mv test submit_job.temp
wrc=$(pwd)
echo $wrc
sed 's #WRC# '${wrc}' g' submit_job.temp > test
mv test submit_job.temp
sed 's/#TIME#/'${time}'/g' submit_job.temp > test
mv test $scr

if [ $perm -eq 1 ];then
	chmod +x $scr
fi
rm submit_job.temp
