##
##
##
##
##

time=$1
Node=$2
n=$3
g=$4
openmp=$5
cpuptask=$6
perm=$7
scr=$8
pt=${9}
pz=${10}
py=${11}
px=${12}
exe=${13}
name=${14}
lx=${15}
lz=${16}
ly=${17}
lt=${18}
prec=${19}


sed 's/#NODES#/'${Node}'/g' submit_job.sh.template > test
mv test submit_job.temp
sed 's/#NTASK#/'${n}'/g' submit_job.temp > test
mv test submit_job.temp
sed 's/#NTASKPERNODE#/'${g}'/g' submit_job.temp > test
mv test submit_job.temp
sed 's/#OPENMP#/'${openmp}'/g' submit_job.temp > test
mv test submit_job.temp
sed 's/#CPUSPERTASK#/'${cpuptask}'/g' submit_job.temp > test
mv test submit_job.temp
wrc=$(pwd)
echo $wrc
sed 's #WRC# '${wrc}' g' submit_job.temp > test
mv test submit_job.temp
sed 's/#PT#/'${pt}'/g' submit_job.temp > test
mv test submit_job.temp
sed 's/#PZ#/'${pz}'/g' submit_job.temp > test
mv test submit_job.temp
sed 's/#PY#/'${py}'/g' submit_job.temp > test
mv test submit_job.temp
sed 's/#PX#/'${px}'/g' submit_job.temp > test
mv test submit_job.temp
sed 's #EXE# '${exe}' g' submit_job.temp > test
mv test submit_job.temp
sed 's/#NAME#/'${name}'/g' submit_job.temp > test
mv test submit_job.temp
sed 's/#LT#/'${lt}'/g' submit_job.temp > test
mv test submit_job.temp
sed 's/#LZ#/'${lz}'/g' submit_job.temp > test
mv test submit_job.temp
sed 's/#LY#/'${ly}'/g' submit_job.temp > test
mv test submit_job.temp
sed 's/#LX#/'${lx}'/g' submit_job.temp > test
mv test submit_job.temp
sed 's/#PREC#/'${prec}'/g' submit_job.temp > test
mv test submit_job.temp

sed 's/#TIME#/'${time}'/g' submit_job.temp > test
mv test $scr

if [ $perm -eq 1 ];then
	chmod +x $scr
fi
rm submit_job.temp
