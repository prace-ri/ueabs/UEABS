#!/bin/bash
##
##	SUBMIT SCRIPT RUNING
##
##      edit: "srun" to the system
##
# SUBMITION HEADER
#

n=$1
m=$2
g=$3
EXE=$4
name=$5
lx=$6
ly=$7
lz=$8
lt=$9
recon=${10}
prec=${11}
prec_sloppy=${12}


echo $n
echo $m
echo $g
echo $EXE
echo $lx
echo $ly
echo $lz
echo $lt


echo "srun -N ${n} --tasks-per-node=$g $EXE --xgridsize 1 --ygridsize $g --zgridsize $m --tgridsize $n --xdim $lx --ydim $ly --zdim $lz --tdim $lt --recon $recon --recon_sloppy $recon --prec $prec --prec_sloppy $prec_sloppy > /dev/null"
srun -N ${n} --tasks-per-node=$g $EXE --xgridsize 1 --ygridsize $g --zgridsize $m --tgridsize $n --xdim $lx --ydim $ly --zdim $lz --tdim $lt --recon $recon --recon_sloppy $recon --prec $prec --prec_sloppy $prec_sloppy > /dev/null
if [ $? -eq 0 ] ; then
  echo "srun -N ${n} --tasks-per-node=$g $EXE --xgridsize 1 --ygridsize $g --zgridsize $m --tgridsize $n --xdim $lx --ydim $ly --zdim $lz --tdim $lt --recon $recon --recon_sloppy $recon --prec $prec --prec_sloppy $prec_sloppy > $name.log"
  srun -N ${n} --tasks-per-node=$g $EXE --xgridsize 1 --ygridsize $g --zgridsize $m --tgridsize $n --xdim $lx --ydim $ly --zdim $lz --tdim $lt --recon $recon --recon_sloppy $recon --prec $prec --prec_sloppy $prec_sloppy > $name.log
fi
