Run instructions for NAMD.
ntell@grnet.gr

After build of NAMD you have an executable called namd2.

The best performance and scaling of namd is achieved using
hybrid MPI/MT version. On a system with nodes of NC cores per node
use 1 MPI task per node and NC threads per task,
for example on a 20 cores/node system use 1 MPI process,
set OMP_NUM_THREADS or any batch system related variable to 20. 

Set a  variable, for example MYPPN to NC-1, 
for example to 19 for a 20 cores/node system.

You can also try other combinations of TASKSPERNODE/THREADSPERTASK to check.

The control file is stmv.8M.memopt.namd for tier-1 and stmv.28M.memopt.namd
for tier-0 systems.

The general way to run is :
WRAPPER WRAPPER_OPTIONS PATH_TO_namd2 +ppn $MYPPN stmv.8M.memopt.namd > logfile

WRAPPER and WRAPPER_OPTIONS  depend on system, batch system etc.
Few common pairs are :

CRAY     : aprun -n TASKS -N NODES -d THREADSPERTASK
Curie    : ccc_mrun with no options - obtained from batch system
Juqueen  : runjob --np TASKS --ranks-per-node TASKSPERNOD --exp-env OMP_NUM_THREADS
Slurm    : srun  with no options, obtained from slurm if the variables below are set.
#SBATCH --nodes=NODES
#SBATCH --ntasks-per-node=TASKSPERNODE
#SBATCH --cpus-per-task=THREADSPERTASK


The run walltime is reported at the end of logfile : grep WallClock: logfile  | awk -F ' ' '{print $2}'

