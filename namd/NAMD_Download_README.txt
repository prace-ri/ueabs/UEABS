The official site to download namd is :
http://www.ks.uiuc.edu/Research/namd/

You need to register for free here to get a namd copy from here :
http://www.ks.uiuc.edu/Development/Download/download.cgi?PackageName=NAMD

In order to get a specific CVS snapshot, you need first to ask for
username/password :  http://www.ks.uiuc.edu/Research/namd/cvsrequest.html
When your cvs access application is approved, you can use your username/password
to download a specific cvs snapshot :
cvs  -d :pserver:username@cvs.ks.uiuc.edu:/namd/cvsroot co -D "2013-02-06 23:59:00 GMT" namd2

In this case, the charm++ is not included. 
You have to download separately and put it in the namd2 source tree :
http://charm.cs.illinois.edu/distrib/charm-6.5.0.tar.gz


