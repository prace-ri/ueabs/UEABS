Build instructions for namd.
In order to run benchmarks the memopt build with SMP support is mandatory.
 
NAMD may be compiled in an experimental memory-optimized mode that utilizes a compressed version of the molecular structure and also supports parallel I/O. 
In addition to reducing per-node memory requirements, the compressed structure greatly reduces startup times compared to reading a psf file. 

In order to build this version, your MPI need to have level of thread support: MPI_THREAD_FUNNELED
You need a NAMD 2.11 version or newer.

1. 	Uncompress/tar the source. 
2. 	cd NAMD_Source_BASE (the directory name depends on how the source obtained,
	typically : namd2 or NAMD_2.11_Source )
3.	untar the charm-VERSION.tar that exists. If you obtained the namd source via
	cvs, you need to download separately charm.
4.      cd to charm-VERSION directory
5.      configure and compile charm :
        This step is system dependent. Some examples are :

CRAY XE6  : ./build charm++ mpi-crayxe        smp                         --with-production -O -DCMK_OPTIMIZE
CURIE     : ./build charm++ mpi-linux-x86_64  smp  mpicxx  ifort          --with-production -O -DCMK_OPTIMIZE
JUQUEEN   : ./build charm++ mpi-bluegeneq     smp  xlc                    --with-production -O -DCMK_OPTIMIZE
Help      : ./build --help to see all available options.
For special notes on various systems, you should look in http://www.ks.uiuc.edu/Research/namd/2.11/notes.html.

The syntax is : ./build charm++ ARCHITECTURE  smp (compilers, optional)   --with-production -O -DCMK_OPTIMIZE
You can find a list of supported architectures/compilers in charm-VERSION/src/arch
The smp option is mandatory to build the Hybrid version of namd.
This builds charm++.

6.	cd ..
7.	Configure NAMD.
        This step is system dependent. Some examples are :
CRAY-XE6    	./config CRAY-XT-g++ --charm-base ./charm-6.7.0 --charm-arch mpi-crayxe-smp --with-fftw3 --fftw-prefix $CRAY_FFTW_DIR --without-tcl --with-memopt --charm-opts -verbose

CURIE		./config Linux-x86_64-icc --charm-base ./charm-6.7.0  --charm-arch mpi-linux-x86_64-ifort-smp-mpicxx --with-fftw3 --fftw-prefix PATH_TO_FFTW3_INSTALLATION --without-tcl --with-memopt --charm-opts -verbose --cxx-opts "-O3 -xAVX "  --cc-opts "-O3 -xAVX" --cxx icpc --cc icc --cxx-noalias-opts "-fno-alias -ip -fno-rtti -no-vec  "

Juqueen: 	./config BlueGeneQ-MPI-xlC --charm-base ./charm-6.7.0 --charm-arch mpi-bluegeneq-smp-xlc --with-fftw3 --with-fftw-prefix PATH_TO_FFTW3_INSTALLATION  --without-tcl --charm-opts -verbose --with-memopt
Help      : ./config --help to see all available options.
See in http://www.ks.uiuc.edu/Research/namd/2.11/notes.html for special notes on various systems.

What is absolutely necessary is the option : --with-memopt and an SMP enabled charm++ build.
It is suggested to disable tcl support as it is indicated by the --without-tcl flags, since tcl is not necessary
to run the benchmarks.

	You need to specify the fftw3 installation directory. On systems that
	use environment modules you need to load the existing fftw3 module
	and probably use the provided environment variables - like in CRAY-XE6
	example above. 
	If fftw3 libraries are not installed on your system,
	download and install fftw-3.3.5.tar.gz from http://www.fftw.org/.

	You may adjust the compilers and compiler flags as the CURIE example. 
        A typical use of compilers/flags adjustement is for example
        to add -xAVX in the CURIE case and keep all the other compiler flags of the architecture the same.
        Take care or even just avoid using the --cxx option for NAMD config with no reason, 
        as this will override the compilation flags from the arch file.

	When config ends prompts to change to a directory and run make. 

8.	cd to the reported directory and run make 
	If everything is ok you'll find the executable with name namd2 in this
	directory.





